package com.vbrain.crypto7;

import com.vbrain.crypto.Xor;
import com.vbrain.crypto2.PKCS7Padder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.BeforeClass;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.security.Security;
import java.util.Arrays;
import java.util.Random;

import static junit.framework.Assert.*;

public class Test_49_CBC_MAC_Message_Forgery {

	public static final int BLOCK_SIZE = 16;

	@BeforeClass
	public static void beforeClass() {
		Security.addProvider(new BouncyCastleProvider());
	}

	@Test
	public void testAttack_ControlledIV() throws Exception {
		byte[] key = "1234567812345678".getBytes();

		byte[] iv = new byte[BLOCK_SIZE];
		new Random().nextBytes(iv); // Fill with random bytes

		Client_49 client = new Client_49(key);
		Server_49 server = new Server_49(key);

		byte[] plaintext = "from=123&to=456&amount=1000000".getBytes();

		byte[] signedMessage = client.sign(plaintext, iv);

		assertTrue(server.verify(signedMessage));

		// Changing "from" to 124
		int offset = 7;
		signedMessage[offset] = '4';

		// Adjusting IV for change above
		byte mask = '4' ^ '3';
		signedMessage[(signedMessage.length - BLOCK_SIZE * 2 + offset)] ^= mask;

		assertTrue(new String(signedMessage).startsWith("from=124&to=456"));
		assertTrue(server.verify(signedMessage));
	}

	@Test
	public void testAttack_LengthExtension() throws Exception {
		byte[] originalKey = "1234567812345678".getBytes();

		Client_49_2 originalClient = new Client_49_2(originalKey);
		Server_49_2 server = new Server_49_2(originalKey);

		// Ask target user to pay you 30k in 3 transactions (need clear ;777:10000; string in forged blocks)
		byte[] originalMessage = PKCS7Padder.pad("from=123&tx_list=777:10000;777:10000;777:10000".getBytes(), BLOCK_SIZE);
		byte[] signedOriginalMessage = originalClient.sign(originalMessage);
		assertTrue(server.verify(signedOriginalMessage));

		byte[] originalMac = Arrays.copyOfRange(signedOriginalMessage, signedOriginalMessage.length - BLOCK_SIZE, signedOriginalMessage.length);

		// Extension
		byte[] glueBlock0 = Arrays.copyOfRange(originalMessage, 0, BLOCK_SIZE);
		glueBlock0 = Xor.encode(glueBlock0, originalMac);

		int i = 100; // 100 x 10,000 = 1,000,000 spacebucks
		byte[] forgedBlock = Arrays.copyOfRange(originalMessage, BLOCK_SIZE, originalMessage.length);

		ByteBuffer bb = ByteBuffer.allocate(originalMessage.length + (glueBlock0.length + forgedBlock.length) * (i - 1) + originalMac.length);
		bb.put(originalMessage);
		for (int j = 0; j < i - 1; j++) {
			bb.put(glueBlock0);
			bb.put(forgedBlock);
		}
		bb.put(originalMac);
		byte[] forgedMessage = bb.array();

		// Verifying forged message
		assertTrue(server.verify(forgedMessage));

		// Verification: Counting valid transactions in forged message
		int count = 0;
		int index = 0;
		String msgStr = new String(forgedMessage);
		while (index >= 0) {
			index = msgStr.indexOf(";777:10000;", index + 11);
			if (index > 0) {
				count++;
			}
		}
		assertEquals(100, count);
	}

	@Test
	public void testSignVerify() throws Exception {
		byte[] key = "1234567812345678".getBytes();

		byte[] iv = new byte[BLOCK_SIZE];
		new Random().nextBytes(iv); // Fill with random bytes

		Client_49 client = new Client_49(key);
		Server_49 server = new Server_49(key);

		byte[] plaintext = "from=123&to=456&amount=1000000".getBytes();

		byte[] signedMessage = client.sign(plaintext, iv);
		assertTrue(server.verify(signedMessage));
	}

	@Test
	public void testSignVerifyTampered() throws Exception {
		byte[] key = "1234567812345678".getBytes();

		byte[] iv = new byte[BLOCK_SIZE];
		new Random().nextBytes(iv); // Fill with random bytes

		Client_49 client = new Client_49(key);
		Server_49 server = new Server_49(key);

		byte[] plaintext = "from=123&to=456&amount=1000000".getBytes();

		byte[] signedMessage = client.sign(plaintext, iv);
		signedMessage[0] = 'x';
		assertFalse(server.verify(signedMessage));
	}

	@Test
	public void testSignVerify2() throws Exception {
		byte[] key = "1234567812345678".getBytes();

		Client_49_2 client = new Client_49_2(key);
		Server_49_2 server = new Server_49_2(key);

		byte[] plaintext = "from=123&to=456&amount=1000000".getBytes();

		byte[] signedMessage = client.sign(plaintext);
		assertTrue(server.verify(signedMessage));
	}

	@Test
	public void testSignVerifyTampered2() throws Exception {
		byte[] key = "1234567812345678".getBytes();

		Client_49_2 client = new Client_49_2(key);
		Server_49_2 server = new Server_49_2(key);

		byte[] plaintext = "from=123&to=456&amount=1000000".getBytes();

		byte[] signedMessage = client.sign(plaintext);
		signedMessage[0] = 'x';
		assertFalse(server.verify(signedMessage));
	}

}
