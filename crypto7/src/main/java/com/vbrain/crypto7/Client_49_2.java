package com.vbrain.crypto7;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.ByteBuffer;

public class Client_49_2 {

	public static final String AES_CBC_PKCS7_PADDING = "AES/CBC/PKCS7Padding";
	public static final String AES_CBC_NO_PADDING = "AES/CBC/NoPadding";
	public static final int AES_BLOCK_SIZE = 16;

	private byte[] key;
	private final byte[] iv = new byte[AES_BLOCK_SIZE];

	public Client_49_2(byte[] key) {
		this.key = key;
	}

	public byte[] sign(byte[] plaintext) throws Exception {
		byte[] encrypted = encrypt(plaintext);
		ByteBuffer buff = ByteBuffer.allocate(plaintext.length + AES_BLOCK_SIZE);
		buff.put(plaintext).put(encrypted, encrypted.length - AES_BLOCK_SIZE, AES_BLOCK_SIZE);
		return buff.array();
	}

	private byte[] encrypt(byte[] plaintext) throws Exception {
		SecretKeySpec keySpec = new SecretKeySpec(key, "AES");
		IvParameterSpec ivSpec = new IvParameterSpec(iv);

		Cipher cipher = Cipher.getInstance(AES_CBC_NO_PADDING, "BC");
		cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);

		return cipher.doFinal(plaintext);
	}

}
