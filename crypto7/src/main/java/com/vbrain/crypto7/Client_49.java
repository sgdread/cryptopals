package com.vbrain.crypto7;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class Client_49 {

	public static final String AES_CBC_PKCS7_PADDING = "AES/CBC/PKCS7Padding";
	public static final int AES_BLOCK_SIZE = 16;

	private byte[] key;

	public Client_49(byte[] key) {
		this.key = key;
	}

	public byte[] sign(byte[] plaintext, byte[] iv) throws Exception {
		byte[] encrypted = encrypt(plaintext, iv);
		ByteBuffer buff = ByteBuffer.allocate(plaintext.length + iv.length + AES_BLOCK_SIZE);
		buff.put(plaintext).put(iv).put(encrypted, encrypted.length - AES_BLOCK_SIZE, AES_BLOCK_SIZE);
		return buff.array();
	}

	private byte[] encrypt(byte[] plaintext, byte[] iv) throws Exception {
		SecretKeySpec keySpec = new SecretKeySpec(key, "AES");
		IvParameterSpec ivSpec = new IvParameterSpec(iv);

		Cipher cipher = Cipher.getInstance(AES_CBC_PKCS7_PADDING, "BC");
		cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);

		return cipher.doFinal(plaintext);
	}

}
