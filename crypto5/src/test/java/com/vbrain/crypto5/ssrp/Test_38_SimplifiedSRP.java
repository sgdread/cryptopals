package com.vbrain.crypto5.ssrp;

import static com.vbrain.crypto5.srp.Tools.HMAC_SHA256;
import static com.vbrain.crypto5.srp.Tools.sha256;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.Security;
import java.util.Random;

import org.apache.commons.codec.binary.Hex;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.BeforeClass;
import org.junit.Test;

public class Test_38_SimplifiedSRP {

	private static final BigInteger NIST_PRIME = new BigInteger(
			"ffffffffffffffffc90fdaa22168c234c4c6628b80dc1cd129024e088a67cc74020bbea63b139b22514a08798e3404ddef9519b3cd3a431b302b0a6df25f14374fe1356d6d51c245e485b576625e7ec6f44c42e9a637ed6b0bff5cb6f406b7edee386bfb5a899fa5ae9f24117c4b1fe649286651ece45b3dc2007cb8a163bf0598da48361c55d39a69163fa8fd24cf5f83655d23dca3ad961c62f356208552bb9ed529077096966d670c354e4abc9804f1746c08ca237327ffffffffffffffff",
			16);

	@BeforeClass
	public static void before() {
		Security.addProvider(new BouncyCastleProvider());
	}

	@Test
	public void testMessageSending() throws Exception {
		BigInteger n = NIST_PRIME;
		BigInteger g = BigInteger.valueOf(2L);
		String email = "john.malkovich@teddy.kgb";
		String password = "password"; // << very secure one

		Server server = new Server(n, g, email, password);
		Client client = new Client(n, g, email, password);

		new DirectChannelImpl(server, client);

		server.init();
		client.start();

		assertTrue(client.isOkReceived());
	}

	@Test
	public void testMessageSendingMITM() throws Exception {
		BigInteger n = NIST_PRIME;
		BigInteger g = BigInteger.valueOf(2L);
		String email = "john.malkovich@teddy.kgb";
		String password = "password"; // << very secure one

		Server server = new Server(n, g, email, password);
		Client client = new Client(n, g, email, password);

		MITMChannelImpl mitm = new MITMChannelImpl(server, client, n, g);

		server.init();
		client.start();

		assertEquals(password, mitm.getRecoveredPassword());
	}

	@Test
	public void testSSRP() throws Exception {
		String I = "person";
		String pc = "hello"; // Client password
		String ps = "hello"; // Server password
		long s = new Random().nextLong();
		BigInteger g = BigInteger.valueOf(2);
		BigInteger N = BigInteger.valueOf(13);

		String xHs = Hex.encodeHexString(sha256(ByteBuffer.allocate(8 + ps.length()).putLong(s).put(ps.getBytes()).array()));
		BigInteger xCs = new BigInteger(xHs, 16);
		BigInteger v = g.modPow(xCs, N);

		System.out.println("1. client sends username I and public ephemeral value A to the server");
		BigInteger a = BigInteger.valueOf(new Random().nextLong());
		BigInteger A = g.modPow(a, N);
		System.out.println("    client->server (I=" + I + ", A=" + A + ")");

		BigInteger b = BigInteger.valueOf(new Random().nextLong());
		BigInteger B = g.modPow(b, N);
		BigInteger u = BigInteger.valueOf(new Random().nextLong());
		System.out.println("    server->client (salt=" + s + ", B=" + B + ", u=" + u + ")");

		System.out.println("2. client and server calculate session keys");
		String xHc = Hex.encodeHexString(sha256(ByteBuffer.allocate(8 + pc.length()).putLong(s).put(pc.getBytes()).array()));
		BigInteger xCc = new BigInteger(xHc, 16);
		BigInteger sc2 = a.add(u.multiply(xCc));
		BigInteger Sc = B.modPow(sc2, N);
		String Kc = Hex.encodeHexString(sha256(Sc.toByteArray()));
		System.out.println("    client: S=" + Sc + "; K=" + Kc);

		BigInteger ss1 = A.multiply(v.modPow(u, N));
		BigInteger Ss = ss1.modPow(b, N);
		String Ks = Hex.encodeHexString(sha256(Ss.toByteArray()));
		System.out.println("    server: S=" + Ss + "; K=" + Ks);

		System.out.println("3. client sends proof of session key to server");
		String Mc = Hex.encodeHexString(HMAC_SHA256(Kc.getBytes(), ByteBuffer.allocate(8).putLong(s).array()));
		System.out.println("    client->server (" + Mc + ")");

		System.out.println("4. server sends proof of session key to client");
		String Ms = Hex.encodeHexString(HMAC_SHA256(Ks.getBytes(), ByteBuffer.allocate(8).putLong(s).array()));
		System.out.println("    Mc==Ms: " + Mc.equals(Ms));
		assertEquals(Mc, Ms);
	}

}
