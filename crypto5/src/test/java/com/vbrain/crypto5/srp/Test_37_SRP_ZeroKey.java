package com.vbrain.crypto5.srp;

import static org.junit.Assert.assertTrue;

import java.math.BigInteger;
import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Before;
import org.junit.Test;

public class Test_37_SRP_ZeroKey {

	private static final BigInteger NIST_PRIME = new BigInteger(
			"ffffffffffffffffc90fdaa22168c234c4c6628b80dc1cd129024e088a67cc74020bbea63b139b22514a08798e3404ddef9519b3cd3a431b302b0a6df25f14374fe1356d6d51c245e485b576625e7ec6f44c42e9a637ed6b0bff5cb6f406b7edee386bfb5a899fa5ae9f24117c4b1fe649286651ece45b3dc2007cb8a163bf0598da48361c55d39a69163fa8fd24cf5f83655d23dca3ad961c62f356208552bb9ed529077096966d670c354e4abc9804f1746c08ca237327ffffffffffffffff",
			16);

	@Before
	public void before() {
		Security.addProvider(new BouncyCastleProvider());
	}

	@Test
	public void testZeroKey() throws Exception {
		BigInteger n = NIST_PRIME;
		BigInteger g = BigInteger.valueOf(2L);
		BigInteger k = BigInteger.valueOf(3L);
		String email = "john.malkovich@teddy.kgb";
		String password = "foreverYoung123!@#";

		Server server = new Server(n, g, k, email, password);
		ZeroKeyClient client = new ZeroKeyClient(email);

		new DirectChannelImpl(server, client);

		server.init();
		client.start();

		assertTrue(client.isOkReceived());
	}

	@Test
	public void testZeroKey1() throws Exception {
		BigInteger n = NIST_PRIME;
		BigInteger g = BigInteger.valueOf(2L);
		BigInteger k = BigInteger.valueOf(3L);
		String email = "john.malkovich@teddy.kgb";
		String password = "foreverYoung123!@#";
		
		Server server = new Server(n, g, k, email, password);
		ZeroKeyClient2 client = new ZeroKeyClient2(n, email);
		
		new DirectChannelImpl(server, client);
		
		server.init();
		client.start();
		
		assertTrue(client.isOkReceived());
	}
	
}
