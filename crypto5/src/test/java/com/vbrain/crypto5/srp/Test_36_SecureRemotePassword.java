package com.vbrain.crypto5.srp;

import static com.vbrain.crypto5.srp.Tools.HMAC_SHA256;
import static com.vbrain.crypto5.srp.Tools.sha256;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.Security;
import java.util.Random;

import org.apache.commons.codec.binary.Hex;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.BeforeClass;
import org.junit.Test;

public class Test_36_SecureRemotePassword {

	private static final BigInteger NIST_PRIME = new BigInteger(
			"ffffffffffffffffc90fdaa22168c234c4c6628b80dc1cd129024e088a67cc74020bbea63b139b22514a08798e3404ddef9519b3cd3a431b302b0a6df25f14374fe1356d6d51c245e485b576625e7ec6f44c42e9a637ed6b0bff5cb6f406b7edee386bfb5a899fa5ae9f24117c4b1fe649286651ece45b3dc2007cb8a163bf0598da48361c55d39a69163fa8fd24cf5f83655d23dca3ad961c62f356208552bb9ed529077096966d670c354e4abc9804f1746c08ca237327ffffffffffffffff",
			16);

	@BeforeClass
	public static void before() {
		Security.addProvider(new BouncyCastleProvider());
	}

	@Test
	public void testMessageSending() throws Exception {
		BigInteger n = NIST_PRIME;
		BigInteger g = BigInteger.valueOf(2L);
		BigInteger k = BigInteger.valueOf(3L);
		String email = "john.malkovich@teddy.kgb";
		String password = "foreverYoung123!@#";

		Server server = new Server(n, g, k, email, password);
		Client client = new Client(n, g, k, email, password);

		new DirectChannelImpl(server, client);

		server.init();
		client.start();

		assertTrue(client.isOkReceived());
	}

	@Test
	public void testSRPWiki() throws Exception {
		String I = "person";
		String p = "";
		BigInteger g = BigInteger.valueOf(2);
		BigInteger k = BigInteger.valueOf(3);
		BigInteger N = BigInteger.valueOf(13);

		long s = new Random().nextLong();
		System.out.println("Salt:" + s);
		String xH = Hex.encodeHexString(sha256(ByteBuffer.allocate(8 + p.length()).putLong(s).put(p.getBytes()).array()));
		BigInteger x = new BigInteger(xH, 16);
		BigInteger v = g.modPow(x, N);

		System.out.println("1. client sends username I and public ephemeral value A to the server");
		BigInteger a = BigInteger.valueOf(new Random().nextLong());
		BigInteger A = g.modPow(a, N);
		
		System.out.println("client->server (" + I + "," + A + ")");

		System.out.println("2. server sends user's salt s and public ephemeral value B to client");
		BigInteger b = BigInteger.valueOf(new Random().nextLong());
		// B = (k * v + pow(g, b, N)) % N
		BigInteger bb1 = k.multiply(v);
		BigInteger bb2 = g.modPow(b, N);
		BigInteger B = bb1.add(bb2).mod(N);
		System.out.println("server->client (" + s + "," + B + ")");

		System.out.println("3. client and server calculate the random scrambling parameter");
		String uH = Hex.encodeHexString(sha256(ByteBuffer.allocate(A.toByteArray().length + B.toByteArray().length).put(A.toByteArray()).put(B.toByteArray())
				.array()));
		BigInteger u = new BigInteger(uH, 16);
		System.out.println(u);

		System.out.println("4. client computes session key");
		xH = Hex.encodeHexString(sha256(ByteBuffer.allocate(8 + p.length()).putLong(s).put(p.getBytes()).array()));
		x = new BigInteger(xH, 16);
		// S_c = pow(B - k * pow(g, x, N), a + u * x, N)
		BigInteger sc1 = B.subtract(k.multiply(g.modPow(x, N)));
		BigInteger sc2 = a.add(u.multiply(x));
		BigInteger Sc = sc1.modPow(sc2, N);
		System.out.println("Sc:" + Sc);
		String Kc = Hex.encodeHexString(sha256(Sc.toByteArray()));
		System.out.println(Kc);

		System.out.println("5. server computes session key");
		// S_s = pow(A * pow(v, u, N), b, N)
		BigInteger ss1 = A.multiply(v.modPow(u, N));
		BigInteger Ss = ss1.modPow(b, N);
		System.out.println("Ss:" + Ss);
		String Ks = Hex.encodeHexString(sha256(Ss.toByteArray()));
		System.out.println(Ks);

		System.out.println("6. client sends proof of session key to server");
		// M_c = H(H(N) ^ H(g), H(I), s, A, B, K_c)
		String Mc = Hex.encodeHexString(HMAC_SHA256(Kc.getBytes(), ByteBuffer.allocate(8).putLong(s).array()));
		System.out.println("client->server (" + Mc + ")");

		System.out.println("7. server sends proof of session key to client");
		String Ms = Hex.encodeHexString(HMAC_SHA256(Ks.getBytes(), ByteBuffer.allocate(8).putLong(s).array()));
		System.out.println("Mc==Ms: " + Mc.equals(Ms));
		assertEquals(Mc, Ms);
	}
}
