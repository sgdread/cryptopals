package com.vbrain.crypto5.mitm1;

import static org.junit.Assert.assertEquals;

import java.math.BigInteger;

import org.junit.Test;

import com.vbrain.crypto5.mitm1.EchoNode;
import com.vbrain.crypto5.mitm1.MITMNode;
import com.vbrain.crypto5.mitm1.MessageReceiver;
import com.vbrain.crypto5.mitm1.Node;

public class Test_34_DiffieHellman_MITM_KeyFixing {

	@Test
	public void testMessageSendingEcho_NoMITM() throws Exception {
		BigInteger p = BigInteger.valueOf(37);
		BigInteger g = BigInteger.valueOf(2);
		
		Node alice = new Node(p, g);
		Node bob = new EchoNode();
		
		alice.connect(bob);
		bob.connect(alice);
		
		alice.sendPG();
		alice.sendPublicKey();
		
		bob.sendPublicKey();
		
		TestMessageReceiver messageReceiver = new TestMessageReceiver();
		alice.setMessageReceiver(messageReceiver);
		
		String msg = "test message";
		alice.sendMessage(msg.getBytes());
		
		assertEquals(msg, messageReceiver.message);
	}
	
	@Test
	public void testMessageSending() throws Exception {
		BigInteger p = BigInteger.valueOf(37);
		BigInteger g = BigInteger.valueOf(2);

		Node alice = new Node(p, g);
		Node bob = new EchoNode();

		
		Node mallory = new MITMNode(alice, bob);
		alice.connect(mallory);
		bob.connect(mallory);

		alice.sendPG();
		alice.sendPublicKey(); // A->M : p,g,A; M->B: p,g,p

		bob.sendPublicKey(); // B->M : B; M->A: p

		TestMessageReceiver messageReceiver = new TestMessageReceiver();
		alice.setMessageReceiver(messageReceiver);

		String msg = "test message";
		alice.sendMessage(msg.getBytes());

		assertEquals(msg + "ABBA", messageReceiver.message);
	}

	private class TestMessageReceiver implements MessageReceiver {

		private String message;

		public void receiveMessage(byte[] msg, Node from) {
			message = new String(msg);
		}

	}

}
