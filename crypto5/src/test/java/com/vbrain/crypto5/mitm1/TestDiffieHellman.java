package com.vbrain.crypto5.mitm1;

import static org.junit.Assert.assertEquals;

import java.math.BigInteger;

import org.junit.Test;

import com.vbrain.crypto5.mitm1.MessageReceiver;
import com.vbrain.crypto5.mitm1.Node;

public class TestDiffieHellman {

	@Test
	public void testExchange() throws Exception {
		BigInteger p = BigInteger.valueOf(37);
		BigInteger g = BigInteger.valueOf(5);

		Node alice = new Node(p, g);
		Node bob = new Node();
		
		alice.connect(bob);
		bob.connect(alice);
		
		alice.sendPG();
		alice.sendPublicKey();
		bob.sendPublicKey();

		assertEquals(alice.getSessionKey(), bob.getSessionKey());
	}

	@Test
	public void testExchangeNIST() throws Exception {
		String pVal = "ffffffffffffffffc90fdaa22168c234c4c6628b80dc1cd129024e088a67cc74020bbea63b139b22514a08798e3404ddef9519b3cd3a431b302b0a6df25f14374fe1356d6d51c245e485b576625e7ec6f44c42e9a637ed6b0bff5cb6f406b7edee386bfb5a899fa5ae9f24117c4b1fe649286651ece45b3dc2007cb8a163bf0598da48361c55d39a69163fa8fd24cf5f83655d23dca3ad961c62f356208552bb9ed529077096966d670c354e4abc9804f1746c08ca237327ffffffffffffffff";
		BigInteger p = new BigInteger(pVal, 16);
		BigInteger g = BigInteger.valueOf(2);

		Node alice = new Node(p, g);
		Node bob = new Node();

		alice.connect(bob);
		bob.connect(alice);

		alice.sendPG();
		alice.sendPublicKey();
		bob.sendPublicKey();

		assertEquals(alice.getSessionKey(), bob.getSessionKey());
	}
	
	@Test
	public void testMessageSending() throws Exception {
		BigInteger p = BigInteger.valueOf(37);
		BigInteger g = BigInteger.valueOf(2);
		
		Node alice = new Node(p, g);
		Node bob = new Node();
		
		alice.connect(bob);
		bob.connect(alice);

		alice.sendPG();
		alice.sendPublicKey();
		bob.sendPublicKey();
		
		TestMessageReceiver messageReceiver = new TestMessageReceiver();
		bob.setMessageReceiver(messageReceiver);

		String msg = "test message";
		alice.sendMessage(msg.getBytes());
		
		assertEquals(msg, messageReceiver.message);
	}
	
	private class TestMessageReceiver implements MessageReceiver {

		private String message; 
		
		public void receiveMessage(byte[] msg, Node from) {
			message = new String(msg);
		}
		
	}

}
