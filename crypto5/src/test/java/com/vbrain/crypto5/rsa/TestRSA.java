package com.vbrain.crypto5.rsa;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;



public class TestRSA {

	@Test
	public void testEncryptDecrypt() throws Exception {
		RSA rsa = new RSA();
		String plaintext = "testText";
		String ciphertext = rsa.encrypt(plaintext);
		System.out.println(ciphertext);
		String decrypted = rsa.decrypt(ciphertext);
		assertNotEquals(plaintext, ciphertext);
		assertEquals(plaintext, decrypted);
	}
	
}
