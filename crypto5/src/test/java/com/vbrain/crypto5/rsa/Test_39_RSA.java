package com.vbrain.crypto5.rsa;

import static org.junit.Assert.assertEquals;

import java.math.BigInteger;
import java.security.SecureRandom;

import org.javatuples.Pair;
import org.junit.Test;

public class Test_39_RSA {

	@Test
	public void testRun1() throws Exception {
		System.out.println(BigInteger.valueOf(17).modInverse(BigInteger.valueOf(3120)));
	}

	@Test
	public void testRun() throws Exception {
		BigInteger e = BigInteger.valueOf(3);
		BigInteger d = null;
		BigInteger n = null;
		
		while (d == null) {
			try {
				BigInteger p = BigInteger.probablePrime(512, new SecureRandom());
				BigInteger q = BigInteger.probablePrime(512, new SecureRandom());
				n = p.multiply(q);
				// totient
				BigInteger et = (p.subtract(BigInteger.ONE).multiply(q.subtract(BigInteger.ONE)));
				d = e.modInverse(et);
			} catch (ArithmeticException ex) {
				// probable primes generated not-invertible e and et
			}
		}

		Pair<BigInteger, BigInteger> pubKey = Pair.with(e, n);
		Pair<BigInteger, BigInteger> privKey = Pair.with(d, n);

		BigInteger m = BigInteger.valueOf(42);

		// Encryption
		BigInteger c = m.modPow(pubKey.getValue0(), pubKey.getValue1());
		
		// Decryption
		BigInteger md = c.modPow(privKey.getValue0(), privKey.getValue1());

		assertEquals(m, md);
	}

}
