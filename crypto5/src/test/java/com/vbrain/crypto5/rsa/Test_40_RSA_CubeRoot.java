package com.vbrain.crypto5.rsa;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.apache.commons.codec.binary.Hex;
import org.junit.Test;
import org.nevec.rjm.BigDecimalMath;

public class Test_40_RSA_CubeRoot {

	@Test
	public void testEncryptDecrypt2() throws Exception {
		String plaintext = "very secure text";

		RSA rsa1 = new RSA();
		String ciphertext1 = rsa1.encrypt(plaintext);
		BigInteger c1 = new BigInteger(Hex.decodeHex(ciphertext1.toCharArray()));

		RSA rsa2 = new RSA();
		String ciphertext2 = rsa2.encrypt(plaintext);
		BigInteger c2 = new BigInteger(Hex.decodeHex(ciphertext2.toCharArray()));

		RSA rsa3 = new RSA();
		String ciphertext3 = rsa3.encrypt(plaintext);
		BigInteger c3 = new BigInteger(Hex.decodeHex(ciphertext3.toCharArray()));

		BigInteger cbrt = BigDecimalMath.cbrt(new BigDecimal(c1)).toBigInteger();
		String recoveredOriginal = new String(Hex.decodeHex(cbrt.toString(16).toCharArray()));

		// Ensuring message is not padded :-P
		assertEquals(c1, c2);
		assertEquals(c1, c3);

		assertEquals(plaintext, recoveredOriginal);
	}

}
