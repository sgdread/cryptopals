package com.vbrain.crypto5.mitm1;

import java.math.BigInteger;
import java.util.Random;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

import com.vbrain.crypto2.CBC;
import com.vbrain.crypto2.PKCS7Padder;
import com.vbrain.crypto5.DiffieHellmanTools;

public class Node {

	protected BigInteger privateKey;
	protected BigInteger publicKey;
	protected BigInteger p;
	protected BigInteger g;
	protected MessageReceiver messageReceiver;
	private Node connectedNode;

	public Node() {
	}

	public Node(BigInteger p, BigInteger g) {
		this.p = p;
		this.g = g;
		this.privateKey = BigInteger.valueOf(new Random().nextInt(Integer.MAX_VALUE)).mod(p);
	}

	public void sendPG() {
		connectedNode.receivePG(p, g, this);
	}

	protected void receivePG(BigInteger p, BigInteger g, Node from) {
		this.p = p;
		this.g = g;
		this.privateKey = BigInteger.valueOf(new Random().nextInt(Integer.MAX_VALUE)).mod(p);
	}

	public void sendPublicKey() {
		connectedNode.receivePublicKey(g.modPow(privateKey, p), this);
	}

	protected void receivePublicKey(BigInteger publicKey, Node from) {
		this.publicKey = publicKey;
	}

	protected BigInteger getSessionKey() {
		return publicKey.modPow(privateKey, p);
	}

	public void sendMessage(byte[] message) {
		byte[] key = new byte[16];
		System.arraycopy(DiffieHellmanTools.generateSha1Key(getSessionKey()), 0, key, 0, key.length);

		byte[] iv = new byte[16];
		new Random().nextBytes(iv);

		byte[] ciphertext;
		try {
			byte[] padded = PKCS7Padder.pad(message, 16);
			ciphertext = CBC.encrypt(padded, key, iv);
		} catch (Exception ex) {
			throw new IllegalStateException(ex);
		}

		String msg = Hex.encodeHexString(ciphertext) + "|" + Hex.encodeHexString(iv);
		connectedNode.receiveMessage(msg, this);
	}

	public void receiveMessage(String msg, Node from) {
		String[] splits = msg.split("\\|");

		byte[] key = new byte[16];
		System.arraycopy(DiffieHellmanTools.generateSha1Key(getSessionKey()), 0, key, 0, key.length);

		byte[] ciphertext;
		byte[] iv;
		try {
			ciphertext = Hex.decodeHex(splits[0].toCharArray());
			iv = Hex.decodeHex(splits[1].toCharArray());
		} catch (DecoderException ex) {
			throw new IllegalStateException(ex);
		}

		byte[] plaintext;
		try {
			byte[] padded = CBC.decrypt(ciphertext, key, iv);
			plaintext = PKCS7Padder.unpad(padded, 16);
		} catch (Exception ex) {
			throw new IllegalStateException(ex);
		}

		if (messageReceiver != null) {
			messageReceiver.receiveMessage(plaintext, from);
		}
	}

	public void setMessageReceiver(MessageReceiver messageReceiver) {
		this.messageReceiver = messageReceiver;
	}

	public void connect(Node connectedNode) {
		this.connectedNode = connectedNode;
	}

}
