package com.vbrain.crypto5.mitm1;

import java.math.BigInteger;

public class MITMNode extends Node implements MessageReceiver {

	private Node alice;
	private Node bob;

	public MITMNode(Node alice, Node bob) {
		this.messageReceiver = this;
		this.alice = alice;
		this.bob = bob;
	}

	@Override
	protected void receivePG(BigInteger p, BigInteger g, Node from) {
		this.p = p;
		this.g = g;
		if (from == alice) {
			bob.receivePG(p, g, from);
		}
	}

	@Override
	protected void receivePublicKey(BigInteger publicKey, Node from) {
		if (from == alice) {
			bob.receivePublicKey(p, from);
		} else {
			alice.receivePublicKey(p, from);
		}
	}

	@Override
	public void receiveMessage(String msg, Node from) {
		super.receiveMessage(msg, from);
	}

	@Override
	protected BigInteger getSessionKey() {
		return BigInteger.ZERO;
	}

	@Override
	public void receiveMessage(byte[] msg, Node from) {
		String message = new String(msg);
		String tamperedMessage = message + (from == alice ? "AB" : "BA");
		System.out.println("MITM: " + (from == alice ? "Alice->Bob" : "Bob->Alice") + ": [" + message + "] -> [" + tamperedMessage + "]");
		connect(from == alice ? bob : alice);
		sendMessage(tamperedMessage.getBytes());
	}

}
