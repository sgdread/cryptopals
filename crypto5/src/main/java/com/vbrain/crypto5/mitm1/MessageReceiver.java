package com.vbrain.crypto5.mitm1;

public interface MessageReceiver {

	public void receiveMessage(byte[] msg, Node from);

}
