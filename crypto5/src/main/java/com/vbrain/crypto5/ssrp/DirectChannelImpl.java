package com.vbrain.crypto5.ssrp;

import java.math.BigInteger;

public class DirectChannelImpl implements Channel {

	public ServerEndpoint server;
	public ClientEndpoint client;

	public DirectChannelImpl(ServerEndpoint server, ClientEndpoint client) {
		server.setChannel(this);
		client.setChannel(this);
		this.server = server;
		this.client = client;
	}

	@Override
	public void sendEmailAndA(String email, BigInteger A) {
		server.receiveEmailAndA(email, A);
	}

	@Override
	public void sendSaltAndBAndU(long salt, BigInteger B, BigInteger u) {
		client.receiveSaltAndBAndU(salt, B, u);
	}

	@Override
	public void sendHmac(String hmac) {
		server.receiveHmac(hmac);
	}

	@Override
	public void sendOk() {
		client.receiveOk();

	}

}
