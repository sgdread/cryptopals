package com.vbrain.crypto5.ssrp;

import java.math.BigInteger;

public interface ServerEndpoint {

	public void receiveEmailAndA(String email, BigInteger A);

	public void receiveHmac(String hmac);

	public abstract void setChannel(Channel channel);

}
