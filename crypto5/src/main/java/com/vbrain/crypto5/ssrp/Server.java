package com.vbrain.crypto5.ssrp;

import static com.vbrain.crypto5.srp.Tools.HMAC_SHA256;
import static com.vbrain.crypto5.srp.Tools.sha256;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.Random;

import org.apache.commons.codec.binary.Hex;

public class Server implements ServerEndpoint {

	private BigInteger N;
	private BigInteger g;

	private String email; // I
	private String p; // P
	private long s;
	private BigInteger v;
	private Channel channel;
	private BigInteger b;
	private String K;

	public Server(BigInteger n, BigInteger g, String email, String password) {
		this.N = n;
		this.g = g;
		this.email = email;
		this.p = password;
		b = BigInteger.valueOf(new Random().nextLong());
	}

	protected void init() {
		s = new Random().nextLong();
		String xH = Hex.encodeHexString(sha256(ByteBuffer.allocate(8 + p.length()).putLong(s).put(p.getBytes()).array()));
		BigInteger x = new BigInteger(xH, 16);
		v = g.modPow(x, N);
	}

	@Override
	public void receiveEmailAndA(String email, BigInteger A) {
		BigInteger b = BigInteger.valueOf(new Random().nextLong());
		BigInteger B = g.modPow(b, N);
		BigInteger u = BigInteger.valueOf(new Random().nextLong());

		BigInteger ss1 = A.multiply(v.modPow(u, N));
		BigInteger Ss = ss1.modPow(b, N);
		K = Hex.encodeHexString(sha256(Ss.toByteArray()));

		channel.sendSaltAndBAndU(s, B, u);
	}

	@Override
	public void receiveHmac(String Mc) {
		String Ms = Hex.encodeHexString(HMAC_SHA256(K.getBytes(), ByteBuffer.allocate(8).putLong(s).array()));
		if (Mc.equals(Ms)) {
			channel.sendOk();
		}
	}

	@Override
	public void setChannel(Channel channel) {
		this.channel = channel;
	}

}
