package com.vbrain.crypto5.ssrp;

import static com.vbrain.crypto5.srp.Tools.HMAC_SHA256;
import static com.vbrain.crypto5.srp.Tools.sha256;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.List;

import org.apache.commons.codec.binary.Hex;

import tools.FileLoader;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

public class MITMChannelImpl implements Channel {

	public ServerEndpoint server;
	public ClientEndpoint client;

	private BigInteger N;
	private BigInteger g;
	private BigInteger u;
	private long s;
	private BigInteger A;
	private BigInteger b;
	private List<String> dict;
	private String recoveredPassword;

	public MITMChannelImpl(ServerEndpoint server, ClientEndpoint client, BigInteger n, BigInteger g) {
		server.setChannel(this);
		client.setChannel(this);
		this.server = server;
		this.client = client;
		this.N = n;
		this.g = g;
		
		// John the Ripper password dictionary
		// http://www.skullsecurity.org/wiki/index.php/Passwords#Password_dictionaries
		String data = FileLoader.loadDataFromFile("src/main/resources/john.txt");
		dict = Lists.newArrayList(Splitter.on("\n").split(data));
		System.out.println("MITM: Loaded password dictionary: " + dict.size() + " entries");
	}

	@Override
	public void sendEmailAndA(String email, BigInteger A) {
		b = BigInteger.valueOf(1);
		BigInteger B = g.modPow(b, N);
		u = BigInteger.valueOf(1);
		s = 0;
		this.A = A;
		
		client.receiveSaltAndBAndU(s, B, u);
	}

	@Override
	public void sendSaltAndBAndU(long salt, BigInteger B, BigInteger u) {
		//client.receiveSaltAndBAndU(salt, B, u);
	}

	@Override
	public void sendHmac(String hmac) {
		offlineDictionaryCrack(hmac);

		//server.receiveHmac(hmac);
	}

	private void offlineDictionaryCrack(String Mc) {
		for (String passwordCandidate : dict) {
			String Ms = generateServerHMAC(passwordCandidate);
			if (Mc.equals(Ms)) {
				System.out.println("MITM: password found [" + passwordCandidate + "]");
				this.recoveredPassword = passwordCandidate;
				break;
			}
		}
	}

	private String generateServerHMAC(String p) {
		String xHs = Hex.encodeHexString(sha256(ByteBuffer.allocate(8 + p.length()).putLong(s).put(p.getBytes()).array()));
		BigInteger xCs = new BigInteger(xHs, 16);
		BigInteger v = g.modPow(xCs, N);

		BigInteger ss1 = A.multiply(v.modPow(u, N));
		BigInteger Ss = ss1.modPow(b, N);
		String K = Hex.encodeHexString(sha256(Ss.toByteArray()));

		String Ms = Hex.encodeHexString(HMAC_SHA256(K.getBytes(), ByteBuffer.allocate(8).putLong(s).array()));
		return Ms;
	}

	@Override
	public void sendOk() {
		//client.receiveOk();
	}

	public Object getRecoveredPassword() {
		return recoveredPassword;
	}

}
