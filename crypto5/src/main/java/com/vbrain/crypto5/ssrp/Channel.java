package com.vbrain.crypto5.ssrp;

import java.math.BigInteger;

public interface Channel {

	public void sendEmailAndA(String email, BigInteger a);

	public void sendSaltAndBAndU(long salt, BigInteger b, BigInteger u);

	public void sendHmac(String hmac);

	public void sendOk();

}
