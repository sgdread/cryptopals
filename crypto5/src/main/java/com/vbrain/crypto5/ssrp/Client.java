package com.vbrain.crypto5.ssrp;

import static com.vbrain.crypto5.srp.Tools.HMAC_SHA256;
import static com.vbrain.crypto5.srp.Tools.sha256;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.Random;

import org.apache.commons.codec.binary.Hex;

public class Client implements ClientEndpoint {

	private BigInteger N;
	private BigInteger g;

	private String email; // I
	private String p; // P

	private Channel channel;
	private BigInteger a;
	private BigInteger A;

	private boolean isOkReceived = false;

	public Client(BigInteger n, BigInteger g, String email, String password) {
		this.N = n;
		this.g = g;
		this.email = email;
		this.p = password;
		a = BigInteger.valueOf(new Random().nextLong());
	}

	public void start() {
		A = g.modPow(a, N);
		channel.sendEmailAndA(email, A);
	}

	@Override
	public void receiveSaltAndBAndU(long s, BigInteger B, BigInteger u) {
		String xH = Hex.encodeHexString(sha256(ByteBuffer.allocate(8 + p.length()).putLong(s).put(p.getBytes()).array()));
		BigInteger xC = new BigInteger(xH, 16);
		BigInteger sc2 = a.add(u.multiply(xC));
		BigInteger Sc = B.modPow(sc2, N);
		String Kc = Hex.encodeHexString(sha256(Sc.toByteArray()));

		String Mc = Hex.encodeHexString(HMAC_SHA256(Kc.getBytes(), ByteBuffer.allocate(8).putLong(s).array()));

		channel.sendHmac(Mc);
	}

	@Override
	public void receiveOk() {
		isOkReceived = true;
		System.out.println("OK");
	}

	public boolean isOkReceived() {
		return isOkReceived;
	}

	@Override
	public void setChannel(Channel channel) {
		this.channel = channel;
	}

}
