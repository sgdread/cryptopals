package com.vbrain.crypto5.ssrp;

import java.math.BigInteger;

public interface ClientEndpoint {

	public void receiveSaltAndBAndU(long salt, BigInteger b, BigInteger u);

	public void receiveOk();

	public abstract void setChannel(Channel channel);
	
}
