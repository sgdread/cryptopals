package com.vbrain.crypto5.srp;

import static com.vbrain.crypto5.srp.Tools.HMAC_SHA256;
import static com.vbrain.crypto5.srp.Tools.sha256;

import java.math.BigInteger;
import java.nio.ByteBuffer;

import org.apache.commons.codec.binary.Hex;

public class ZeroKeyClient2 implements ClientEndpoint {

	private String email; // I

	private Channel channel;
	private BigInteger A;
	private BigInteger N;

	private boolean isOkReceived = false;

	public ZeroKeyClient2(BigInteger N, String email) {
		this.N = N;
		this.email = email;
	}

	public void start() {
		A = N;
		channel.sendEmailAndA(email, A);
	}

	@Override
	public void receiveSaltAndB(long s, BigInteger B) {
		BigInteger Sc = BigInteger.ZERO;
		String Kc = Hex.encodeHexString(sha256(Sc.toByteArray()));
		String Mc = Hex.encodeHexString(HMAC_SHA256(Kc.getBytes(), ByteBuffer.allocate(8).putLong(s).array()));
		channel.sendHmac(Mc);
	}

	@Override
	public void receiveOk() {
		isOkReceived = true;
		System.out.println("OK");
	}

	public boolean isOkReceived() {
		return isOkReceived;
	}

	@Override
	public void setChannel(Channel channel) {
		this.channel = channel;
	}

}
