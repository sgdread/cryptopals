package com.vbrain.crypto5.srp;

import static com.vbrain.crypto5.srp.Tools.HMAC_SHA256;
import static com.vbrain.crypto5.srp.Tools.sha256;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.Random;

import org.apache.commons.codec.binary.Hex;

public class Server implements ServerEndpoint {

	private BigInteger N;
	private BigInteger g;
	private BigInteger k;

	private String email; // I
	private String p; // P
	private long s;
	private BigInteger v;
	private Channel channel;
	private BigInteger b;
	private String K;

	public Server(BigInteger n, BigInteger g, BigInteger k, String email, String password) {
		this.N = n;
		this.g = g;
		this.k = k;
		this.email = email;
		this.p = password;
		b = BigInteger.valueOf(new Random().nextLong());
	}

	protected void init() {
		s = new Random().nextLong();
		String xH = Hex.encodeHexString(sha256(ByteBuffer.allocate(8 + p.length()).putLong(s).put(p.getBytes()).array()));
		BigInteger x = new BigInteger(xH, 16);
		v = g.modPow(x, N);
	}

	@Override
	public void receiveEmailAndA(String email, BigInteger A) {
		BigInteger b = BigInteger.valueOf(new Random().nextLong());
		// B = (k * v + pow(g, b, N)) % N
		BigInteger bb1 = k.multiply(v);
		BigInteger bb2 = g.modPow(b, N);
		BigInteger B = bb1.add(bb2).mod(N);

		String uH = Hex.encodeHexString(sha256(ByteBuffer.allocate(A.toByteArray().length + B.toByteArray().length).put(A.toByteArray()).put(B.toByteArray())
				.array()));
		BigInteger u = new BigInteger(uH, 16);

		BigInteger ss1 = A.multiply(v.modPow(u, N));
		BigInteger Ss = ss1.modPow(b, N);
		K = Hex.encodeHexString(sha256(Ss.toByteArray()));

		channel.sendSaltAndB(s, B);
	}

	@Override
	public void receiveHmac(String Mc) {
		String Ms = Hex.encodeHexString(HMAC_SHA256(K.getBytes(), ByteBuffer.allocate(8).putLong(s).array()));
		if (Mc.equals(Ms)) {
			channel.sendOk();
		}
	}

	@Override
	public void setChannel(Channel channel) {
		this.channel = channel;
	}

}
