package com.vbrain.crypto5.srp;

import java.math.BigInteger;

public interface ClientEndpoint {

	public void receiveSaltAndB(long salt, BigInteger b);

	public void receiveOk();

	public abstract void setChannel(Channel channel);
	
}
