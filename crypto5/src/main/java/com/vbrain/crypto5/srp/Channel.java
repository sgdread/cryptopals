package com.vbrain.crypto5.srp;

import java.math.BigInteger;

public interface Channel {

	public void sendEmailAndA(String email, BigInteger a);

	public void sendSaltAndB(long salt, BigInteger b);

	public void sendHmac(String hmac);

	public void sendOk();

}
