package com.vbrain.crypto5.srp;

import java.security.MessageDigest;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class Tools {

	public static byte[] sha256(byte[] data) {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA256", "BC");
			byte[] digest = md.digest(data);
			return digest;
		} catch (Exception ex) {
			throw new IllegalStateException(ex);
		}
	}
	
	public static byte[] HMAC_SHA256(byte[] key, byte[] salt) {
		try {
			SecretKeySpec secretKey = new SecretKeySpec(key, "HmacSHA256");
			Mac mac = Mac.getInstance("HMAC-SHA256", "BC");
			mac.init(secretKey);
			mac.update(salt);
			return mac.doFinal();
		} catch (Exception ex) {
			throw new IllegalStateException(ex);
		}
	}
	
}
