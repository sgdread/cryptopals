package com.vbrain.crypto5.rsa;

import java.math.BigInteger;
import java.security.SecureRandom;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.javatuples.Pair;

public class RSA {

	private Pair<BigInteger, BigInteger> pubKey;
	private Pair<BigInteger, BigInteger> privKey;
	private BigInteger e;

	public RSA() {
		init();
	}
	
	private void init() {
		e = BigInteger.valueOf(3);
		BigInteger d = null;
		BigInteger n = null;
		
		while (d == null) {
			try {
				BigInteger p = BigInteger.probablePrime(512, new SecureRandom());
				BigInteger q = BigInteger.probablePrime(512, new SecureRandom());
				n = p.multiply(q);
				// totient
				BigInteger et = (p.subtract(BigInteger.ONE).multiply(q.subtract(BigInteger.ONE)));
				d = e.modInverse(et);
			} catch (ArithmeticException ex) {
				// probable primes generated not-invertible e and et
			}
		}

		pubKey = Pair.with(e, n);
		privKey = Pair.with(d, n);
	}
	
	public String encrypt(String data) {
		BigInteger m = new BigInteger(Hex.encodeHexString(data.getBytes()), 16);
		BigInteger c = m.modPow(pubKey.getValue0(), pubKey.getValue1());
		return Hex.encodeHexString(c.toByteArray());
	}
	
	public String decrypt(String ciphertext) {
		try {
			BigInteger c = new BigInteger(Hex.decodeHex(ciphertext.toCharArray()));
			BigInteger m = c.modPow(privKey.getValue0(), privKey.getValue1());
			return new String(Hex.decodeHex(m.toString(16).toCharArray()));

		} catch (DecoderException ex) {
			throw new IllegalArgumentException(ex);
		}
	}
	
}
