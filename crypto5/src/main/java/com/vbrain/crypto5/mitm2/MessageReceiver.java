package com.vbrain.crypto5.mitm2;

public interface MessageReceiver {

	public void receiveMessage(byte[] msg, Node from);

}
