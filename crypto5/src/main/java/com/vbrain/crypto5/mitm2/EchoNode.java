package com.vbrain.crypto5.mitm2;

import java.math.BigInteger;

public class EchoNode extends Node implements MessageReceiver {

	public EchoNode() {
		super();
		this.messageReceiver = this;
	}

	public EchoNode(BigInteger p, BigInteger g) {
		super(p, g);
		this.messageReceiver = this;
	}

	public void receiveMessage(byte[] msg, Node from) {
		sendMessage(msg);
	}

}
