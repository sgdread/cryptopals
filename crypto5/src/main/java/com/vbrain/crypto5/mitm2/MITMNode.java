package com.vbrain.crypto5.mitm2;

import java.math.BigInteger;
import java.util.Random;

public class MITMNode extends Node implements MessageReceiver {

	private Node alice;
	private Node bob;
	protected BigInteger bobG;
	protected BigInteger alicePubKey;
	protected BigInteger bobPubKey;
	protected BigInteger myPubKeyForAlice; // The one sent to Alice
	protected BigInteger myPubKeyForBob; // The one sent to Bob
	private AttackType attackType;
	
	public static enum AttackType {
		ONE, P, P_1;
	}

	public MITMNode(Node alice, Node bob, AttackType attackType) {
		this.messageReceiver = this;
		this.alice = alice;
		this.bob = bob;
		this.attackType = attackType;
	}

	@Override
	protected void receivePG(BigInteger p, BigInteger g, Node from) {
		if (from == alice) {
			this.p = p;
			this.g = g;
			this.bobG = attackType == AttackType.ONE ? BigInteger.ONE :
				attackType == AttackType.P ? p : p.subtract(BigInteger.ONE);
			this.privateKey = BigInteger.valueOf(new Random().nextInt(Integer.MAX_VALUE)).mod(p);
			this.myPubKeyForAlice = g.modPow(privateKey, p);
			this.myPubKeyForBob = bobG.modPow(privateKey, p);
			bob.receivePG(p, bobG, from);
		}
	}

	@Override
	protected void receivePublicKey(BigInteger publicKey, Node from) {
		if (from == alice) {
			alicePubKey = publicKey;
			bob.receivePublicKey(myPubKeyForBob, from);
		} else {
			bobPubKey = publicKey;
			alice.receivePublicKey(myPubKeyForAlice, from);
		}
	}

	@Override
	public void receiveMessage(String msg, Node from) {
		connectedNode = from;
		super.receiveMessage(msg, from);
	}

	@Override
	protected BigInteger getSessionKey() {
		return connectedNode == alice ? alicePubKey.modPow(privateKey, p) : bobPubKey.modPow(privateKey, p);
	}

	@Override
	public void receiveMessage(byte[] msg, Node from) {
		String message = new String(msg);
		String tamperedMessage = message + (from == alice ? "AB" : "BA");
		System.out.println("MITM: " + (from == alice ? "Alice->Bob" : "Bob->Alice") + ": [" + message + "] -> [" + tamperedMessage + "]");
		connect(from == alice ? bob : alice);
		sendMessage(tamperedMessage.getBytes());
	}

}
