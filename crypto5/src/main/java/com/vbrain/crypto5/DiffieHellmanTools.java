package com.vbrain.crypto5;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class DiffieHellmanTools {

	static {
		Security.addProvider(new BouncyCastleProvider());
	}

	public static byte[] generateKey(BigInteger key) {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA256", "BC");
		} catch (Exception ex) {
			throw new IllegalStateException();
		}
		byte[] digest = md.digest(key.toByteArray());
		return digest;
	}

	public static byte[] generateSha1Key(BigInteger key) {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA-1", "BC");
		} catch (Exception ex) {
			throw new IllegalStateException();
		}
		byte[] digest = md.digest(key.toByteArray());
		return digest;
	}

}
