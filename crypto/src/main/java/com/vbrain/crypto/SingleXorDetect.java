package com.vbrain.crypto;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

import com.vbrain.crypto.SingleXorCracker.SingleXorCrackResult;

public class SingleXorDetect {


	public static boolean isSingleXor(String line, SingleXorCracker cracker) throws DecoderException {
		byte[] data = Hex.decodeHex(line.toCharArray());
		boolean result = false;
		SingleXorCrackResult crackResult = cracker.crack(data);
		char xorChar = crackResult.getXorChar();
		boolean isValidChar = xorChar >= ' ' && xorChar <= '~';
		if (isValidChar && crackResult.getResultQuality() > 10 && crackResult.getOverallCharFrequencyDeltaQuality() < 0.25) {
			System.out.println("Detected single-char XOR for line [" + line + "]");
			System.out.println(crackResult);
		}
		return result;
	}

}
