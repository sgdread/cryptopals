package com.vbrain.crypto;

public class HammingDistance {
	
	public static int calc(byte[] a, byte[] b) {
		int distance = 0;
		for (int i = 0; i < a.length; i++) {
			distance += countNumberOfDifferentBits(a[i] ^ b[i]);
		}
		return distance;
	}
	
	
	private static int countNumberOfDifferentBits(int x) {
		int c = 0;
		for (int i = 0; i < 8; i++) {
			if(((x >> i) & 1) == 1) {
				c++;
			}
		}
		return c;
	}

}
