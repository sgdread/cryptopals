package com.vbrain.crypto;

import java.util.Arrays;

import com.vbrain.crypto.SingleXorCracker.SingleXorCrackResult;

public class RepeatingXorCracker {

	public static RepeatingXorCrackResult crack(byte[] data, int maxKeyLen, int lenFactor) {
		int len = getKeyLength(data, maxKeyLen, lenFactor);
		return crackUsingFixedLen(data, len);
	}

	private static int getKeyLength(byte[] data, int maxKeyLen, int lenFactor) {
		double smallest = Integer.MAX_VALUE;
		int len = 0;
		for (int i = 2; i < maxKeyLen; i++) {
			double dist = 0;
			for (int j = 0; j < lenFactor; j += 2) {
				dist += HammingDistance
						.calc(Arrays.copyOfRange(data, (j * 2) * i, (j * 2 + 1) * i), Arrays.copyOfRange(data, (j * 2 + 1) * i, (j * 2 + 2) * i));
			}
			double normDist = dist / (lenFactor * i);
			if (normDist < smallest) {
				smallest = normDist;
				len = i;
			}
		}
		return len;
	}

	public static RepeatingXorCrackResult crackUsingFixedLen(byte[] data, int len) {
		byte[][] blocks = BlockUtils.splitIntoBlocks(data, len);
		byte[] key = new byte[len];
		SingleXorCracker singleXorCracker = new SingleXorCracker("src/main/resources/english_sample.txt");
		for (int i = 0; i < blocks.length; i++) {
			SingleXorCrackResult result = singleXorCracker.crack(blocks[i]);
			blocks[i] = result.getResult();
			key[i] = (byte) result.getXorChar();
		}
		byte[] result = BlockUtils.joinBlocks(blocks, data.length);
		return new RepeatingXorCrackResult(result, key);
	}

	public static class RepeatingXorCrackResult {
		private byte[] result;
		private byte[] key;

		public RepeatingXorCrackResult(byte[] result, byte[] key) {
			this.result = result;
			this.key = key;
		}

		public byte[] getResult() {
			return result;
		}

		public byte[] getKey() {
			return key;
		}

		@Override
		public String toString() {
			return "RepeatingXorCrackResult [" + new String(result) + "], key=[" + new String(key) + "]";
		}

	}

}
