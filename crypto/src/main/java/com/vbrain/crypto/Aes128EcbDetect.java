package com.vbrain.crypto;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Set;

import com.google.common.collect.Sets;

public class Aes128EcbDetect {

	public static void main(String[] args) throws Exception {
		String lines = loadTestData();

		for (String line : lines.split("\n")) {
			Set<String> bag = Sets.newHashSet();
			for (int i = 0; i < line.length() ; i += 32) {
				String block = line.substring(i, i + 32);
				bag.add(block);
			}
			if (bag.size() < line.length() / 32) {
				System.out.println("AES/ECB/NoPadding detected [" + line + "]");
			}
		}

	}

	private static String loadTestData() throws FileNotFoundException, IOException {
		BufferedReader br = new BufferedReader(new FileReader(new File("src/main/resources/gistfile1_3132928.txt")));
		StringBuilder sb = new StringBuilder();
		String line = br.readLine();
		sb.append(line);
		while (line != null) {
			line = br.readLine();
			if (line == null) break;
			sb.append("\n");
			sb.append(line);
		}
		br.close();
		return sb.toString();
	}

}
