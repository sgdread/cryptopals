package com.vbrain.crypto;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;

public class Aes128Ecb {

	public static void main_encrypt(String[] args) throws Exception {
		byte[] raw = "0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF".getBytes();
		SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		KeySpec spec = new PBEKeySpec("password".toCharArray(), "salt".getBytes(), 65536, 128);
		SecretKey tmp = factory.generateSecret(spec);
		SecretKey secret = new SecretKeySpec(tmp.getEncoded(), "AES");

		Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding", "SunJCE");
		cipher.init(Cipher.ENCRYPT_MODE, secret);
		byte[] encrypted = cipher.doFinal(raw);
		System.out.println(encrypted.length + "|" + Hex.encodeHexString(encrypted));

	}

	public static void main(String[] args) throws Exception {
		String key = "YELLOW SUBMARINE";
		byte[] encrypted = Base64.decodeBase64(loadTestData());

		Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding", "SunJCE");
		SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), "AES");
		cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(skeySpec.getEncoded(), "AES"));

		byte[] original = cipher.doFinal(encrypted);
		System.out.println(new String(original));
	}

	private static String loadTestData() throws FileNotFoundException, IOException {
		BufferedReader br = new BufferedReader(new FileReader(new File("src/main/resources/gistfile1_3132853.txt")));
		StringBuilder sb = new StringBuilder();
		String line = br.readLine();
		sb.append(line);
		while (line != null) {
			line = br.readLine();
			if (line == null) break;
			sb.append(line);
		}
		br.close();
		return sb.toString();
	}

}
