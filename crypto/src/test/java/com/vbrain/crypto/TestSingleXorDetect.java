package com.vbrain.crypto;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import org.junit.Test;

public class TestSingleXorDetect {

	@Test
	public void testIsSingleXor() throws Exception {
		SingleXorCracker cracker = new SingleXorCracker("src/main/resources/english_sample.txt");
		BufferedReader br = new BufferedReader(new FileReader(new File("src/main/resources/gistfile1_3132713.txt")));
		String line = br.readLine();
		while (line != null) {
			line = br.readLine();
			if (line == null) break;
			SingleXorDetect.isSingleXor(line, cracker);
		}
		br.close();

	}

}
