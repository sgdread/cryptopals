package com.vbrain.crypto;

import static org.junit.Assert.assertEquals;

import org.apache.commons.codec.binary.Hex;
import org.junit.Test;

public class TestXor {

	@Test
		public void testEncode() throws Exception {
			String input = "1c0111001f010100061a024b53535009181c";
			String mask = "686974207468652062756c6c277320657965";
			String expected = "746865206b696420646f6e277420706c6179";
			byte[] xored = Xor.encode(Hex.decodeHex(input.toCharArray()), Hex.decodeHex(mask.toCharArray()));
			String xoredHex = Hex.encodeHexString(xored);
			assertEquals(expected, xoredHex);
		}

	@Test
		public void testEncode2() throws Exception {
			String input = "Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal";
			String mask = "ICE";
			String expectedHex = "0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f";
			byte[] xored = Xor.encode(input.getBytes(), mask.getBytes());
			String xoredHex = Hex.encodeHexString(xored);
			assertEquals(expectedHex, xoredHex);
		}

}
