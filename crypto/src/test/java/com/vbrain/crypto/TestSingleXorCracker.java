package com.vbrain.crypto;

import org.apache.commons.codec.binary.Hex;
import org.junit.Test;

import com.vbrain.crypto.SingleXorCracker.SingleXorCrackResult;

public class TestSingleXorCracker {

	@Test
	public void testCrack() throws Exception {
		String input = "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736";
		SingleXorCracker cracker = new SingleXorCracker("src/main/resources/english_sample.txt");
		SingleXorCrackResult result = cracker.crack(Hex.decodeHex(input.toCharArray()));
		byte[] crackedResult = result.getResult();
		System.out.println("-------------------------------------------------------------------");
		System.out.println("Final result  : [" + new String(crackedResult) + "]");
		System.out.println("XORed with    : [" + result.getXorChar() + "]");
		System.out.println("Result quality: " + result.getResultQuality());
		System.out.println("-------------------------------------------------------------------");
	}
	
	@Test
	public void testCrack2() throws Exception {
		String input = "3504121541090413040941130709041441040e13080b0741130b0741040b41410413080741161041071611";
		SingleXorCracker cracker = new SingleXorCracker("src/main/resources/english_sample.txt");
		SingleXorCrackResult result = cracker.crack(Hex.decodeHex(input.toCharArray()));
		byte[] crackedResult = result.getResult();
		System.out.println("-------------------------------------------------------------------");
		System.out.println("Final result  : [" + new String(crackedResult) + "]");
		System.out.println("XORed with    : [" + result.getXorChar() + "]");
		System.out.println("Result quality: " + result.getResultQuality());
		System.out.println("-------------------------------------------------------------------");
	}

}
