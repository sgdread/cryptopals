package com.vbrain.crypto;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.apache.commons.codec.binary.Base64;
import org.junit.Test;

import com.vbrain.crypto.RepeatingXorCracker.RepeatingXorCrackResult;

public class TestRepeatingXorCracker {

	@Test
	public void testCrack() throws Exception {
		String original = "Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal\n Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal";
		String key = "ICE";
		byte[] xored = Xor.encode(original.getBytes(), key.getBytes());
		RepeatingXorCrackResult result = RepeatingXorCracker.crack(xored, 10, 4);
		assertEquals(original, new String(result.getResult()));
		assertEquals(key, new String(result.getKey()));
	}

	@Test
	public void testCrackFile() throws Exception {
		String input = loadTestData();
		byte[] raw = Base64.decodeBase64(input);
		RepeatingXorCrackResult result = RepeatingXorCracker.crack(raw, 40, 20);
		System.out.println(result);
	}

	private String loadTestData() throws FileNotFoundException, IOException {
		BufferedReader br = new BufferedReader(new FileReader(new File("src/main/resources/gistfile1_3132752.txt")));
		StringBuilder sb = new StringBuilder();
		String line = br.readLine();
		sb.append(line);
		while (line != null) {
			line = br.readLine();
			if (line == null) break;
			sb.append(line);
		}
		br.close();
		return sb.toString();
	}

}
