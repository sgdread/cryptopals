package com.vbrain.crypto;

import static org.junit.Assert.assertEquals;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.junit.Test;

public class TestDecode {

	@Test
	public void test_Hex_Base64_String() throws Exception {
		String input = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d";
		String expected = "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t";
		byte[] hex = Hex.decodeHex(input.toCharArray());
		String result = Base64.encodeBase64String(hex);
		assertEquals(expected, result);
	}

}
