package com.vbrain.crypto;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestHammingDistance {

	@Test
	public void testCalc() throws Exception {
		assertEquals(37, HammingDistance.calc("this is a test".getBytes(), "wokka wokka!!!".getBytes()));
	}

}
