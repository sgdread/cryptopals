package com.vbrain.crypto4;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class Test_26_CTRBitFlipping {

	@Test
	public void test() {
		byte[] adminString = ";admin=true;".getBytes();

		byte[] nouserData = CTRBitFlippingFunctions.encryptUrl("");

		// AAA... length is equals to adminString length
		byte[] urlWithUserData = CTRBitFlippingFunctions.encryptUrl("AAAAAAAAAAAA");

		// Detecting block with encrypted information
		int tamperOffset = -1;
		for (int i = 0; i < nouserData.length; i++) {
			if (nouserData[i] != urlWithUserData[i]) {
				tamperOffset = i;
				break;
			}
		}
		
		// Extracting keystream
		byte[] tamperedBlockKeystream = new byte[adminString.length];
		for (int i = 0; i < tamperedBlockKeystream.length; i++) {
			tamperedBlockKeystream[i] = (byte) (urlWithUserData[tamperOffset + i] ^ 'A');
		}
		
		// Tampering block
		for (int i = 0; i < tamperedBlockKeystream.length; i++) {
			urlWithUserData[tamperOffset + i] = (byte) (tamperedBlockKeystream[i] ^ adminString[i]);
		}
		
		
		assertTrue(CTRBitFlippingFunctions.isAdmin(urlWithUserData));
	}

}
