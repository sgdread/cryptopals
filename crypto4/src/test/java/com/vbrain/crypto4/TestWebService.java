package com.vbrain.crypto4;

import static org.junit.Assert.*;

import javax.ws.rs.core.Response;

import org.junit.Test;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.test.framework.JerseyTest;

public class TestWebService extends JerseyTest {

	public TestWebService() {
		super("com.vbrain.crypto4");
	}

	@Test
	public void testAuthenticate() throws Exception {
		// Generate valid signature
		WebResource res = resource().path("app/sign");
		res = res.queryParam("file", "foo");
		String mac = res.get(String.class);

		// Checking valid singature
		res = resource().path("app/auth/slow");
		res = res.queryParam("file", "foo");
		res = res.queryParam("signature", mac);
		ClientResponse response = res.get(ClientResponse.class);
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		
		// Checking invalid singature
		res = resource().path("app/auth/slow");
		res = res.queryParam("file", "invalid");
		res = res.queryParam("signature", mac);
		response = res.get(ClientResponse.class);
		assertEquals(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());
	}

}
