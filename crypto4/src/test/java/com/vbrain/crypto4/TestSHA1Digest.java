package com.vbrain.crypto4;

import static org.junit.Assert.assertEquals;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.Security;

import org.apache.commons.codec.binary.Hex;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Test;

public class TestSHA1Digest {

	@Test
	public void testDoFinal() throws Exception {
		Security.addProvider(new BouncyCastleProvider());

		byte[] message = "luehgiuhgiuehgehrguheriugherugheruhg".getBytes();

		SHA1Digest customMd = new SHA1Digest();
		customMd.update(message, 0, message.length);
		byte[] digest = new byte[SHA1Digest.getDigestSize()];
		customMd.doFinal(digest, 0);
		String customResult = Hex.encodeHexString(digest);

		MessageDigest javaMd = MessageDigest.getInstance("SHA-1", "SUN");
		javaMd.update(message);
		digest = javaMd.digest();
		String javaResult = Hex.encodeHexString(digest);

		MessageDigest bcMd = MessageDigest.getInstance("SHA-1", "BC");
		bcMd.update(message);
		byte[] digest3 = bcMd.digest();
		String bcResult = Hex.encodeHexString(digest3);

		assertEquals(javaResult, bcResult);
		assertEquals(javaResult, customResult);
	}

	@Test
	public void testSHA1Digest() throws Exception {
		byte[] message = "1324567890".getBytes();
		
		byte[] added = "add".getBytes();
		byte[] padding = SHA1Tools.getPadding(message, 0);
		ByteBuffer bb = ByteBuffer.allocate(message.length + padding.length + added.length);
		bb.put(message).put(padding).put(added);
		byte[] tamperedMessage = bb.array();
		SHA1Digest md = new SHA1Digest();
		md.update(tamperedMessage, 0, tamperedMessage.length);
		byte[] digest = new byte[SHA1Digest.getDigestSize()];
		md.doFinal(digest, 0);
		String expectedHex = Hex.encodeHexString(digest);

		
		md = new SHA1Digest();
		md.update(message, 0, message.length);
		digest = new byte[SHA1Digest.getDigestSize()];
		md.doFinal(digest, 0);
		
		int[] regs = SHA1Tools.getSHARegistersFromMac(digest);
		
		int i = 0;
		md = new SHA1Digest(regs[i++], regs[i++], regs[i++], regs[i++], regs[i++], message.length + padding.length);
		md.update(added, 0, added.length);
		digest = new byte[SHA1Digest.getDigestSize()];
		md.doFinal(digest, 0);
		String actualHex = Hex.encodeHexString(digest);
		
		assertEquals(expectedHex, actualHex);
	}

}
