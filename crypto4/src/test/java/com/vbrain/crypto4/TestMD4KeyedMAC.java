package com.vbrain.crypto4;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.vbrain.crypto2.RandomSecretKeyGenerator;

public class TestMD4KeyedMAC {

	@Test
	public void testAuthenticate() throws Exception {
		byte[] message = "laiuehrgiuaehrgiluha".getBytes();
		byte[] key = RandomSecretKeyGenerator.generate();
		byte[] signedMessage = MD4KeyedMAC.sign(key, message);
		assertTrue(MD4KeyedMAC.authenticate(key, signedMessage));
	}

	@Test
	public void testAuthenticateTampered() throws Exception {
		byte[] message = "laiuehrgiuaehrgiluha".getBytes();
		byte[] key = RandomSecretKeyGenerator.generate();
		byte[] signedMessage = MD4KeyedMAC.sign(key, message);
		signedMessage[signedMessage.length - 1]++;
		assertFalse(MD4KeyedMAC.authenticate(key, signedMessage));
	}

}
