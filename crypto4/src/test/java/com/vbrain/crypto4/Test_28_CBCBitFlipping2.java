package com.vbrain.crypto4;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.apache.commons.codec.binary.Hex;
import org.junit.Test;

import com.vbrain.crypto.Xor;
import com.vbrain.crypto2.CBC;
import com.vbrain.crypto2.PKCS7Padder;

public class Test_28_CBCBitFlipping2 {

	@Test
	public void test() throws Exception {

		String isAdminString = ";admin=true";
		
		// User request
		String AAABlock = createCharBlock('A', isAdminString.length());
		byte[] url = CBCBitFlippingFunctions2.encryptUrl(AAABlock);

		// Attacker tampering the message
		byte[] tamperedUrl = url.clone();
		for (int i = 0; i < CBC.CIPHER_BLOCK_SIZE; i++) {
			tamperedUrl[CBC.CIPHER_BLOCK_SIZE + i] = 0; // block 2 = 0
			tamperedUrl[CBC.CIPHER_BLOCK_SIZE * 2 + i] = tamperedUrl[i]; // block3 == block1
		}

		// Attacker getting plaintext from request exception
		byte[] plaintext;
		try {
			CBCBitFlippingFunctions2.isAdmin(tamperedUrl);
			throw new IllegalStateException("Wrong attack");
		} catch (IllegalArgumentException ex) {
			// Have to use Hex - Strings will eat some UTF code points
			plaintext = Hex.decodeHex(ex.getCause().getMessage().toCharArray());
		}

		// Key extraction
		byte[] p1 = Arrays.copyOfRange(plaintext, 0, CBC.CIPHER_BLOCK_SIZE);
		byte[] p3 = Arrays.copyOfRange(plaintext, CBC.CIPHER_BLOCK_SIZE * 2, CBC.CIPHER_BLOCK_SIZE * 3);
		byte[] key = Xor.encode(p1, p3);

		// Modify message
		String originalPlaintext = new String(PKCS7Padder.unpad(CBC.decrypt(url, key, key), CBC.CIPHER_BLOCK_SIZE));
		String tamperedPlaintext = originalPlaintext.replaceAll(AAABlock, isAdminString);
		url = CBC.encrypt(PKCS7Padder.pad(tamperedPlaintext.getBytes(), CBC.CIPHER_BLOCK_SIZE), key, key);
		
		assertTrue(CBCBitFlippingFunctions2.isAdmin(url));
	}


	private String createCharBlock(char c, int len) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < len; i++) {
			sb.append(c);
		}
		return sb.toString();
	}

}
