package com.vbrain.crypto4;

import static org.junit.Assert.assertEquals;

import java.security.MessageDigest;
import java.security.Security;

import org.apache.commons.codec.binary.Hex;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Test;

public class TestMD4Digest {

	@Test
	public void testMatch() throws Exception {
		Security.addProvider(new BouncyCastleProvider());

		byte[] message = "so;uhegouhgasdasdasdasdsgu".getBytes();
		MD4Digest customMd = new MD4Digest();
		customMd.update(message, 0, message.length);
		byte[] digest = new byte[MD4Digest.getDigestSize()];
		customMd.doFinal(digest, 0);
		String customResult = Hex.encodeHexString(digest);

		MessageDigest bcMd = MessageDigest.getInstance("MD4", "BC");
		bcMd.update(message);
		byte[] digest3 = bcMd.digest();
		String bcResult = Hex.encodeHexString(digest3);

		assertEquals(bcResult, customResult);

	}

}
