package com.vbrain.crypto4;

import static org.junit.Assert.*;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Test;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.test.framework.JerseyTest;

public class Test_31_ArtificialTimingLeak extends JerseyTest {

	public Test_31_ArtificialTimingLeak() {
		super("com.vbrain.crypto4");
	}

	@Test
	public void testAuthenticate() throws Exception {
		String file = "invalid";

		StringBuilder knownMac = new StringBuilder(40);

		// Warmup with invalid char in code
		ClientResponse response = webRequest(file, "x");
		
		long tm = System.currentTimeMillis();
		response = webRequest(file, "x");// x is not valid hex char
		long lastDelta = System.currentTimeMillis() - tm;

		while (response.getStatus() != Status.OK.getStatusCode()) {
			for (int i = 0; i < 16; i++) {
				String nextChar = Integer.toHexString(i);
				tm = System.currentTimeMillis();
				response = webRequest(file, knownMac.toString() + nextChar);
				long delta = System.currentTimeMillis() - tm;
				if (delta - lastDelta > 7) { // <<<<<<<<<<<<<<< Tuning parameter
					knownMac.append(nextChar);
					System.out.println("Recovered mac: [" + knownMac.toString() + "]");
					lastDelta = delta;
					break;
				}
			}
		}

		System.out.println("Recovered mac: [" + knownMac.toString() + "] - final");

		assertEquals(Response.Status.OK.getStatusCode(), webRequest(file, knownMac.toString()).getStatus());
	}

	private ClientResponse webRequest(String file, String signature) {
		WebResource res = resource().path("app/auth/slow");
		res = res.queryParam("file", file);
		res = res.queryParam("signature", signature);
		for (int i = 0; i < 2; i++) { // <<<<<<<<<<<<<<<<<<<<<< Tuning parameter
			res.get(ClientResponse.class);
		}
		return res.get(ClientResponse.class);

	}

}
