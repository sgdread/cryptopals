package com.vbrain.crypto4;

import static org.junit.Assert.assertTrue;

import org.apache.commons.codec.binary.Hex;
import org.junit.Test;

import com.vbrain.crypto2.CBC;

public class Test_27_CBCBitFlipping {

	@Test
	public void test() {
		byte[] adminString = ";admin=true;".getBytes();
		
		int padding = getPadding();
		int paddingAjustment = padding >= adminString.length ? 0 : adminString.length - padding;
		
		
		int blockToTamper = firstBlockToTamper(padding) + 1;
		byte[] url = CBCBitFlippingFunctions2.encryptUrl(createCharBlock('A', padding + paddingAjustment + CBC.CIPHER_BLOCK_SIZE * 2));
		
		for (int i = 0; i < adminString.length; i++) {
			byte xorByte = (byte) ('A' ^ adminString[i]);
			url[blockToTamper * CBC.CIPHER_BLOCK_SIZE + i] = (byte) (url[blockToTamper * CBC.CIPHER_BLOCK_SIZE + i] ^ xorByte);
		}
		
		assertTrue(CBCBitFlippingFunctions2.isAdmin(url));
	}

	private int getPadding() {
		int padding = 0;
		StringBuilder sb = new StringBuilder();
		int referenceLen = CBCBitFlippingFunctions2.encryptUrl("").length;
		int lastLen = 0;
		do {
			sb.append("A");
			lastLen = CBCBitFlippingFunctions2.encryptUrl(sb.toString()).length;
		} while (referenceLen == lastLen);
		padding = sb.length() - 1;

		return padding;
	}

	private int firstBlockToTamper(int padding) {
		String paddedCharBlock = createCharBlock('A', padding + CBC.CIPHER_BLOCK_SIZE);
		String hexCipher1 = Hex.encodeHexString(CBCBitFlippingFunctions2.encryptUrl(paddedCharBlock));
		String hexCipher2 = Hex.encodeHexString(CBCBitFlippingFunctions2.encryptUrl(paddedCharBlock + "A"));
		int hexBlockSize = CBC.CIPHER_BLOCK_SIZE * 2;
		int result = -1;
		for (int block = 0; block < hexCipher1.length() / hexBlockSize; block++) {
			String block1 = hexCipher1.substring(block * hexBlockSize, (block + 1) * hexBlockSize);
			String block2 = hexCipher2.substring(block * hexBlockSize, (block + 1) * hexBlockSize);
			if (!block1.equals(block2)) {
				result = block - 1;
				break;
			}
		}

		return result;
	}

	private String createCharBlock(char c, int len) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < len; i++) {
			sb.append(c);
		}
		return sb.toString();
	}

}
