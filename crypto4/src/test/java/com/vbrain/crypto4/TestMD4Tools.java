package com.vbrain.crypto4;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.apache.commons.codec.binary.Hex;
import org.junit.Test;

import com.vbrain.crypto2.RandomSecretKeyGenerator;

public class TestMD4Tools {

	@Test
	public void testPadding() throws Exception {
		int offset = 10;
		byte[] message = "1234567890".getBytes();

		byte[] padding = MD4Tools.getPadding(message, offset);

		StringBuilder sb = new StringBuilder();
		sb.append("80");
		for (int i = 1 + 8 + message.length + offset; i < 64; i++) {
			sb.append("00");
		}
		sb.append("a000000000000000");
		assertEquals(sb.toString(), Hex.encodeHexString(padding));

	}

	@Test
	public void testTamperMessage() throws Exception {
		byte[] unknownKey = RandomSecretKeyGenerator.generate();
		byte[] signedMessage = MD4KeyedMAC.sign(unknownKey, "message".getBytes());
		
		// Attacker
		byte[] mac = Arrays.copyOfRange(signedMessage, 0, MD4Digest.getDigestSize());
		byte[] message = Arrays.copyOfRange(signedMessage, MD4Digest.getDigestSize(), signedMessage.length);
		byte[] messageToAdd = "add".getBytes();
		byte[] tamperedMessage = MD4Tools.tamperMessage(mac, 16, message, messageToAdd);
		
		// Checking signature
		assertTrue(MD4KeyedMAC.authenticate(unknownKey, tamperedMessage));
	}

	@Test
	public void testGetMD4RegistersFromMac() throws Exception {
		int H1 = 123456;
		int H2 = 234567;
		int H3 = 345678;
		int H4 = 456789;

		byte[] mac = new byte[16];

		
		MD4Digest.unpackWord(H1, mac, 0);
		MD4Digest.unpackWord(H2, mac, 4);
		MD4Digest.unpackWord(H3, mac, 8);
		MD4Digest.unpackWord(H4, mac, 12);

		int[] registers = MD4Tools.getMD4RegistersFromMac(mac);
		int i = 0;
		assertEquals(H1, registers[i++]);
		assertEquals(H2, registers[i++]);
		assertEquals(H3, registers[i++]);
		assertEquals(H4, registers[i++]);
	}
}
