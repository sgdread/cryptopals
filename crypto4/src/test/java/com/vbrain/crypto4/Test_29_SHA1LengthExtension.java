package com.vbrain.crypto4;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Test;

import com.vbrain.crypto2.RandomSecretKeyGenerator;

public class Test_29_SHA1LengthExtension {
	
	@Test
	public void test() throws Exception {
		String origianlMessage = "comment1=cooking%20MCs;userdata=foo;comment2=%20like%20a%20pound%20of%20bacon";
		byte[] unknownKey = RandomSecretKeyGenerator.generate();
		byte[] signedMessage = SHA1KeyedMAC.sign(unknownKey, origianlMessage.getBytes());
		
		// Attacker
		String stringToAdd = ";admin=true";
		byte[] tamperedMessage = tamperMessage(signedMessage,stringToAdd);
		
		// Checking signature
		System.out.println(new String(Arrays.copyOfRange(tamperedMessage, 20, tamperedMessage.length)));
		assertTrue(new String(tamperedMessage).contains(origianlMessage));
		assertTrue(new String(tamperedMessage).contains(stringToAdd));
		assertTrue(SHA1KeyedMAC.authenticate(unknownKey, tamperedMessage));
	}

	private byte[] tamperMessage(byte[] signedMessage, String stringToAdd) {
		byte[] mac = Arrays.copyOfRange(signedMessage, 0, SHA1Digest.getDigestSize());
		byte[] message = Arrays.copyOfRange(signedMessage, SHA1Digest.getDigestSize(), signedMessage.length);
		byte[] messageToAdd = stringToAdd.getBytes();
		byte[] tamperedMessage = SHA1Tools.tamperMessage(mac, 16, message, messageToAdd);
		return tamperedMessage;
	}


}
