package com.vbrain.crypto4;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestCTRBitFlippingFunctions {

	@Test
	public void testGetUrl() throws Exception {
		String input = "abc";
		String url = CTRBitFlippingFunctions.getUrl(input);
		String expected = "comment1=cooking%20MCs;userdata=abc;comment2=%20like%20a%20pound%20of%20bacon";
		assertEquals(expected, url);
	}

	@Test
	public void testGetUrl_QuotingOut() throws Exception {
		String input = "abc;admin=true";
		String url = CTRBitFlippingFunctions.getUrl(input);
		String expected = "comment1=cooking%20MCs;userdata=abcadmintrue;comment2=%20like%20a%20pound%20of%20bacon";
		assertEquals(expected, url);
	}

}
