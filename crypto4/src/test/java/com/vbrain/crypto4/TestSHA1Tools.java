package com.vbrain.crypto4;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.apache.commons.codec.binary.Hex;
import org.junit.Test;

import com.vbrain.crypto2.RandomSecretKeyGenerator;

public class TestSHA1Tools {

	@Test
	public void testPadding() throws Exception {
		int offset = 10;
		byte[] message = "1234567890".getBytes();

		byte[] padding = SHA1Tools.getPadding(message, offset);

		StringBuilder sb = new StringBuilder();
		sb.append("80");
		for (int i = 1 + 8 + message.length + offset; i < 64; i++) {
			sb.append("00");
		}
		sb.append("00000000000000a0");
		assertEquals(sb.toString(), Hex.encodeHexString(padding));

	}

	@Test
	public void testTamperMessage() throws Exception {
		byte[] unknownKey = RandomSecretKeyGenerator.generate();
		byte[] signedMessage = SHA1KeyedMAC.sign(unknownKey, "message".getBytes());
		
		// Attacker
		byte[] mac = Arrays.copyOfRange(signedMessage, 0, SHA1Digest.getDigestSize());
		byte[] message = Arrays.copyOfRange(signedMessage, SHA1Digest.getDigestSize(), signedMessage.length);
		byte[] messageToAdd = "add".getBytes();
		byte[] tamperedMessage = SHA1Tools.tamperMessage(mac, 16, message, messageToAdd);
		
		// Checking signature
		assertTrue(SHA1KeyedMAC.authenticate(unknownKey, tamperedMessage));
	}

	@Test
	public void testGetSHARegistersFromMac() throws Exception {
		int H1 = 123456;
		int H2 = 234567;
		int H3 = 345678;
		int H4 = 456789;
		int H5 = 567890;

		byte[] mac = new byte[20];

		SHA1Digest.intToBigEndian(H1, mac, 0);
		SHA1Digest.intToBigEndian(H2, mac, 4);
		SHA1Digest.intToBigEndian(H3, mac, 8);
		SHA1Digest.intToBigEndian(H4, mac, 12);
		SHA1Digest.intToBigEndian(H5, mac, 16);

		int[] registers = SHA1Tools.getSHARegistersFromMac(mac);
		int i = 0;
		assertEquals(H1, registers[i++]);
		assertEquals(H2, registers[i++]);
		assertEquals(H3, registers[i++]);
		assertEquals(H4, registers[i++]);
		assertEquals(H5, registers[i++]);
	}
}
