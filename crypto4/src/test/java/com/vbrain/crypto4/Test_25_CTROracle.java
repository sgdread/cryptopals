package com.vbrain.crypto4;

import org.junit.Test;

import com.vbrain.crypto.Xor;

public class Test_25_CTROracle {

	@Test
	public void testRun() throws Exception {
		byte[] origCiphertext = CTROracle.init();
		// Using 0x00 as mask
		byte[] xorMask = new byte[origCiphertext.length];
		// CTR XOR operation will produce keystream as a response
		byte[] keystreamMask = CTROracle.edit(origCiphertext, 0, xorMask);
		// ciphertext XOR keystream = plaintext
		byte[] plaintext = Xor.encode(origCiphertext, keystreamMask);

		System.out.println(new String(plaintext));
	}

}
