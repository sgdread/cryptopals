package com.vbrain.crypto4;

import static org.junit.Assert.assertEquals;

import java.security.Security;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Test;

public class TestHMACSHA1 {

	@Test
	public void testHmac() throws Exception {
		byte[] key = "abc".getBytes();
		byte[] message = "iluehgiudhgiuahrga".getBytes();

		Security.addProvider(new BouncyCastleProvider());

		SecretKeySpec secretKey = new SecretKeySpec(key, "HmacSHA1");
		Mac bcMac = Mac.getInstance("HMac-SHA1", "BC");
		bcMac.init(secretKey);
		byte[] mac = bcMac.doFinal(message);
		String bcResult = Hex.encodeHexString(mac);

		mac = HMACSHA1.hmac(key, message);
		String myResult = Hex.encodeHexString(mac);

		assertEquals(bcResult, myResult);

	}
	
}
