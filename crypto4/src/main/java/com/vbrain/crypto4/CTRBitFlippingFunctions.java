package com.vbrain.crypto4;

import java.util.Random;

import com.vbrain.crypto2.RandomSecretKeyGenerator;
import com.vbrain.crypto3.CTR;

public class CTRBitFlippingFunctions {

	private static byte[] unknownPredefinedKey = RandomSecretKeyGenerator.generate();
	private static long unknownNonce = new Random().nextLong();

	public static byte[] encryptUrl(String userdata) {
		String url = getUrl(userdata);
		try {
			byte[] ciphertext = CTR.encrypt(url.getBytes(), unknownNonce, unknownPredefinedKey);
			return ciphertext;
		} catch (Exception ex) {
			throw new IllegalArgumentException(ex);
		}
	}

	public static boolean isAdmin(byte[] ciphertext) {
		try {
			byte[] plaintext = CTR.decrypt(ciphertext, unknownNonce, unknownPredefinedKey);
			String url = new String(plaintext);
			boolean admin = url.contains(";admin=true;");
			return admin;
		} catch (Exception ex) {
			throw new IllegalArgumentException(ex);
		}
	}

	public static String getUrl(String userdata) {
		String prefix = "comment1=cooking%20MCs;userdata=";
		String suffix = ";comment2=%20like%20a%20pound%20of%20bacon";
		userdata = userdata.replaceAll("[;=]", "");
		String result = prefix + userdata + suffix;
		return result;
	}

}
