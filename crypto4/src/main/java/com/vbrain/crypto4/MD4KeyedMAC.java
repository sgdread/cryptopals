package com.vbrain.crypto4;

import java.nio.ByteBuffer;

public class MD4KeyedMAC {

	public static byte[] sign(byte[] key, byte[] message) {
		MD4Digest md = new MD4Digest();
		md.update(key, 0, key.length);
		md.update(message, 0, message.length);
		byte[] digest = new byte[MD4Digest.getDigestSize()];
		md.doFinal(digest, 0);

		ByteBuffer bb = ByteBuffer.allocate(digest.length + message.length);
		bb.put(digest);
		bb.put(message);

		return bb.array();
	}

	public static boolean authenticate(byte[] key, byte[] signedMessage) {
		MD4Digest md = new MD4Digest();
		md.update(key, 0, key.length);
		md.update(signedMessage, MD4Digest.getDigestSize(), signedMessage.length - MD4Digest.getDigestSize());
		byte[] digest = new byte[MD4Digest.getDigestSize()];
		md.doFinal(digest, 0);

		boolean result = true;
		for (int i = 0; i < digest.length; i++) {
			if (signedMessage[i] != digest[i]) {
				result = false;
				break;
			}
		}

		return result;
	}

}
