package com.vbrain.crypto4;

import java.nio.ByteBuffer;

public class SHA1KeyedMAC {

	public static byte[] sign(byte[] key, byte[] message) {
		SHA1Digest md = new SHA1Digest();
		md.update(key, 0, key.length);
		md.update(message, 0, message.length);
		byte[] digest = new byte[SHA1Digest.getDigestSize()];
		md.doFinal(digest, 0);

		ByteBuffer bb = ByteBuffer.allocate(digest.length + message.length);
		bb.put(digest);
		bb.put(message);

		return bb.array();
	}

	public static boolean authenticate(byte[] key, byte[] signedMessage) {
		SHA1Digest md = new SHA1Digest();
		md.update(key, 0, key.length);
		md.update(signedMessage, SHA1Digest.getDigestSize(), signedMessage.length - SHA1Digest.getDigestSize());
		byte[] digest = new byte[SHA1Digest.getDigestSize()];
		md.doFinal(digest, 0);

		boolean result = true;
		for (int i = 0; i < digest.length; i++) {
			if (signedMessage[i] != digest[i]) {
				result = false;
				break;
			}
		}

		return result;
	}

}
