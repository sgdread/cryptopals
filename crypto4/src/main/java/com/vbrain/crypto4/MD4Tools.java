package com.vbrain.crypto4;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;

public class MD4Tools {

	public static byte[] tamperMessage(byte[] mac, int keyLen, byte[] origianlMessage, byte[] messageToAdd) {
		byte[] padding = getPadding(origianlMessage, keyLen);
		ByteBuffer bb = ByteBuffer.allocate(mac.length + origianlMessage.length + padding.length + messageToAdd.length);
		bb.put(getTamperedMac(mac, keyLen + origianlMessage.length + padding.length, messageToAdd));
		bb.put(origianlMessage);
		bb.put(padding);
		bb.put(messageToAdd);
		return bb.array();
	}

	public static byte[] getTamperedMac(byte[] mac, int offset, byte[] messageToAdd) {
		int[] registers = getMD4RegistersFromMac(mac);
		int i = 0;
		int H1 = registers[i++];
		int H2 = registers[i++];
		int H3 = registers[i++];
		int H4 = registers[i++];

		MD4Digest md = new MD4Digest(H1, H2, H3, H4, offset);
		md.update(messageToAdd, 0, messageToAdd.length);
		byte[] tamperedMac = new byte[mac.length];
		md.doFinal(tamperedMac, 0);

		return tamperedMac;
	}

	public static int[] getMD4RegistersFromMac(byte[] mac) {
		ByteBuffer bb = ByteBuffer.allocate(16).order(ByteOrder.LITTLE_ENDIAN);
		IntBuffer ib = bb.asIntBuffer();
		bb.put(mac);
		int[] registers = new int[4];
		ib.get(registers);
		return registers;
	}

	public static byte[] getPadding(byte[] message, int offset) {
		int paddingLen = 64 - ((message.length + offset + 1 + 8) % 64); 
		ByteBuffer bb = ByteBuffer.allocate(1 + paddingLen + 8);
		bb.put((byte) 0x80);
		for (int i = 0; i < paddingLen; i++) {
			bb.put((byte) 0x00);
		}
		long bitLen = (message.length + offset) << 3;
		bb.order(ByteOrder.LITTLE_ENDIAN);
		bb.putLong(bitLen);

		return bb.array();
	}

}
