package com.vbrain.crypto4;

import java.util.Random;

import org.apache.commons.codec.binary.Hex;

import com.vbrain.crypto2.CBC;
import com.vbrain.crypto2.PKCS7Padder;
import com.vbrain.crypto2.RandomSecretKeyGenerator;

public class CBCBitFlippingFunctions2 {

	private static byte[] unknownPredefinedKey = RandomSecretKeyGenerator.generate();
	// Using key as IV
	private static byte[] unknownIV = unknownPredefinedKey;
	static {
		new Random().nextBytes(unknownIV);
	}

	public static byte[] encryptUrl(String userdata) {
		String url = getUrl(userdata);
		byte[] padded = PKCS7Padder.pad(url.getBytes(), CBC.CIPHER_BLOCK_SIZE);
		try {
			byte[] ciphertext = CBC.encrypt(padded, unknownPredefinedKey, unknownIV);
			return ciphertext;
		} catch (Exception ex) {
			throw new IllegalArgumentException(ex);
		}
	}

	public static boolean isAdmin(byte[] ciphertext) {
		try {
			byte[] paddedPlaintext = CBC.decrypt(ciphertext, unknownPredefinedKey, unknownIV);
			byte[] plaintext = PKCS7Padder.unpad(paddedPlaintext, CBC.CIPHER_BLOCK_SIZE);
			// Verify for ASCII compliance
			for (int i = 0; i < plaintext.length; i++) {
				if (plaintext[i] < 0) {
					throw new IllegalArgumentException(Hex.encodeHexString(plaintext));
				}
			}
			String url = new String(plaintext);
			boolean admin = url.contains(";admin=true;");
			return admin;
		} catch (Exception ex) {
			throw new IllegalArgumentException(ex);
		}
	}

	public static String getUrl(String userdata) {
		String prefix = "comment1=cooking%20MCs;userdata=";
		String suffix = ";comment2=%20like%20a%20pound%20of%20bacon";
		userdata = userdata.replaceAll("[;=]", "");
		String result = prefix + userdata + suffix;
		return result;
	}

}
