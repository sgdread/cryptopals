package com.vbrain.crypto4;

import java.nio.ByteBuffer;
import java.util.Arrays;

public class HMACSHA1 {

	private static final int BLOCK_SIZE = SHA1Digest.getByteLength();

	public static byte[] hmac(byte[] key, byte[] message) {

		if (key.length > BLOCK_SIZE) {
			key = hash(key);
		} else if (key.length < BLOCK_SIZE) {
			key = Arrays.copyOf(key, BLOCK_SIZE);
		}

		byte[] oKeyPad = new byte[BLOCK_SIZE];
		byte[] iKeyPad = new byte[BLOCK_SIZE];
		for (int i = 0; i < key.length; i++) {
			oKeyPad[i] = (byte) (key[i] ^ 0x5c);
			iKeyPad[i] = (byte) (key[i] ^ 0x36);
		}

		byte[] innerHash = hash(ByteBuffer.allocate(iKeyPad.length + message.length).put(iKeyPad).put(message).array());
		byte[] mac = hash(ByteBuffer.allocate(oKeyPad.length + innerHash.length).put(oKeyPad).put(innerHash).array());
		return mac;
	}

	private static byte[] hash(byte[] data) {
		SHA1Digest md = new SHA1Digest();
		byte[] digest = new byte[SHA1Digest.getDigestSize()];
		md.update(data, 0, data.length);
		md.doFinal(digest, 0);
		return digest;
	}

}
