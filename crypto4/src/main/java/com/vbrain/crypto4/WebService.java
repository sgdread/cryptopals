package com.vbrain.crypto4;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.binary.Hex;

import com.vbrain.crypto2.RandomSecretKeyGenerator;

@Path("/app")
public class WebService {

	private static final byte[] unknownKey = RandomSecretKeyGenerator.generate();

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/sign")
	public Response sign(@QueryParam("file") String file) {
		String macHex = Hex.encodeHexString(HMACSHA1.hmac(unknownKey, file.getBytes()));
		return Response.status(Status.OK).entity(macHex).build();
	}

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/auth/slow")
	public Response auth(@QueryParam("file") String file, @QueryParam("signature") String signature) {
		String macHex = Hex.encodeHexString(HMACSHA1.hmac(unknownKey, file.getBytes()));
		Status status = insecureCompare(signature, macHex, 5);
		Response response = Response.status(status).build();
		return response;
	}

	private Status insecureCompare(String signature, String macHex, int delayInMillis) {
		Status status = Status.OK;
		try {
			for (int i = 0; i < macHex.length(); i++) {
				if (macHex.charAt(i) != signature.charAt(i)) {
					status = Status.INTERNAL_SERVER_ERROR;
					break;
				}
				try {
					Thread.sleep(delayInMillis);
				} catch (InterruptedException ex) {
					ex.printStackTrace();
				}
			}
		} catch (Exception ex) {
			status = Status.INTERNAL_SERVER_ERROR;
		}
		return status;
	}

}
