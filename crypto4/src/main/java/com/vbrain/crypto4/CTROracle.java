package com.vbrain.crypto4;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Random;

import org.apache.commons.codec.binary.Base64;

import com.vbrain.crypto2.ECB;
import com.vbrain.crypto2.FileLoader;
import com.vbrain.crypto2.PKCS7Padder;
import com.vbrain.crypto2.RandomSecretKeyGenerator;
import com.vbrain.crypto3.CTR;

public class CTROracle {

	private static byte[] unknowKey = RandomSecretKeyGenerator.generate();
	private static long unknownNonce = new Random().nextLong();

	public static byte[] init() throws Exception {
		String raw = FileLoader.loadDataFromFile("src/main/resources/25_data.txt");
		byte[] plaintext = ECB.decrypt(Base64.decodeBase64(raw.getBytes()), "YELLOW SUBMARINE".getBytes());
		plaintext = PKCS7Padder.unpad(plaintext, 16);
		byte[] ciphertext = CTR.encrypt(plaintext, unknownNonce, unknowKey);
		return ciphertext;
	}

	public static byte[] edit(byte[] ciphertext, int offset, byte[] newText) throws Exception {
		byte[] plaintext = CTR.decrypt(ciphertext, unknownNonce, unknowKey);
		
		int lenIncrease = offset + newText.length - plaintext.length;
		if (lenIncrease < 0) {
			lenIncrease = 0;
		}

		ByteBuffer bb = ByteBuffer.allocate(plaintext.length + lenIncrease);
		bb.put(Arrays.copyOfRange(plaintext, 0, offset));
		bb.put(newText);
		if (lenIncrease == 0) {
			bb.put(Arrays.copyOfRange(plaintext, offset + newText.length, plaintext.length));
		}

		plaintext = bb.array();

		ciphertext = CTR.encrypt(plaintext, unknownNonce, unknowKey);

		return ciphertext;
	}
}
