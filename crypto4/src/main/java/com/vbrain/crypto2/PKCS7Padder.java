package com.vbrain.crypto2;

import java.util.Arrays;

public class PKCS7Padder {

	public static byte[] pad(byte[] data, int len) {
		byte[] padded;
		if (data.length % len == 0) {
			padded = Arrays.copyOf(data, data.length + len);
			byte padValue = (byte) len;
			for (int i = data.length; i < padded.length; i++) {
				padded[i] = padValue;
			}
		} else {
			int paddedLen = data.length < len ? len : ((data.length / len) + 1) * len;
			padded = Arrays.copyOf(data, paddedLen);
			byte padding = (byte) ((paddedLen - data.length) & 0xFF);
			for (int i = data.length; i < paddedLen; i++) {
				padded[i] = padding;
			}
		}
		return padded;
	}

	public static byte[] unpad(byte[] data, int len) {
		if (data.length % len != 0) {
			throw new IllegalArgumentException("Invalid padding: message length is not factor of " + len);
		}
		byte lastByte = data[data.length - 1];
		if(lastByte > len) {
			throw new IllegalArgumentException("Invalid padding: wrong padding byte [" + lastByte + "] at " + (data.length - 1));
		}
		
		for (int i = data.length - lastByte; i < data.length ; i++) {
			if(data[i] != lastByte) {
				throw new IllegalArgumentException("Invalid padding: wrong padding byte [" + lastByte + "] at " + (i));
			}
		}
		
		return Arrays.copyOfRange(data, 0, data.length - lastByte);
	}
}
