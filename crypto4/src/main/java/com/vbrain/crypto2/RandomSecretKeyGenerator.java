package com.vbrain.crypto2;

import java.util.Random;

public class RandomSecretKeyGenerator {
	
	public static byte[] generate() {
		byte[] key = new byte[16];
		new Random().nextBytes(key);
		return key ;
	}
	
}
