package tools;

import java.math.BigInteger;

public class BI {
	
	

	public static BigInteger divCeil(BigInteger x, BigInteger y) {
		BigInteger[] divideAndRemainder = x.divideAndRemainder(y);
		return divideAndRemainder[1].equals(BigInteger.ZERO) ? divideAndRemainder[0] : divideAndRemainder[0].add(BigInteger.ONE);
	}

	public static BigInteger divFloor(BigInteger x, BigInteger y) {
		return x.divide(y);
	}

	public static BigInteger max(BigInteger x, BigInteger y) {
		return x.compareTo(y) >= 0 ? x : y;
	}

	public static BigInteger min(BigInteger x, BigInteger y) {
		return x.compareTo(y) < 0 ? x : y;
	}

}
