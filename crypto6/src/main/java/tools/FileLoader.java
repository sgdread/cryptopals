package tools;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import com.google.common.base.Function;

public class FileLoader {
	
	public static String loadDataFromFile(String filename) {
		return loadDataFromFile(filename, new Function<String, String>() {
			public String apply(String input) {
				return input + "\n";
			}
		});
	}
	
	public static String loadDataFromFile(String filename, Function<String, String> transform) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(filename));
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();
			while (line != null) {
				line = transform.apply(line);
				sb.append(line);
				line = br.readLine();
				if (line == null) break;
			}
			br.close();
			return sb.toString();
		} catch (IOException ex) {
			throw new IllegalStateException(ex);
		}
	}

}
