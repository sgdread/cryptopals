package com.vbrain.crypto6.dsa;

import static java.math.BigInteger.ONE;

import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;
import java.security.Security;
import java.security.interfaces.DSAParams;
import java.security.interfaces.DSAPrivateKey;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.BigIntegers;
import org.javatuples.Pair;

public class DSA {

	private static final int KEY_LEN = 1024;
	private BigInteger p;
	private BigInteger q;
	private BigInteger g;
	private BigInteger x;
	private BigInteger y;
	private BigInteger k;

	static {
		Security.addProvider(new BouncyCastleProvider());
	}

	public DSA() {
		init();
	}

	public void init(BigInteger p, BigInteger q, BigInteger g) {
		try {
			this.p = p;
			this.q = q;
			this.g = g;

			k = BigIntegers.createRandomInRange(ONE, q, new SecureRandom());
			x = BigIntegers.createRandomInRange(ONE, q, new SecureRandom());
			y = g.modPow(x, p);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new IllegalStateException(ex);
		}
	}

	public void init() {
		try {
			// Just reuse Bouncy Castle key parameters generation
			KeyPairGenerator keyGen = KeyPairGenerator.getInstance("DSA", "BC");
			keyGen.initialize(KEY_LEN);
			KeyPair keypair = keyGen.genKeyPair();
			DSAPrivateKey privateKey = (DSAPrivateKey) keypair.getPrivate();

			DSAParams dsaParams = privateKey.getParams();
			p = dsaParams.getP();
			q = dsaParams.getQ();
			g = dsaParams.getG();

			init(p, q, g);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new IllegalStateException(ex);
		}
	}

	/**
	 * @param message
	 * @return Pair&lt;r, s&gt;
	 */
	public Pair<BigInteger, BigInteger> sign(byte[] message) {
		try {
			// 160-bit hash required
			BigInteger shaM = DSATools.sha(message);

			BigInteger r = g.modPow(k, p).mod(q);

			BigInteger kInv = k.modInverse(q);

			BigInteger s = kInv.multiply(shaM.add(x.multiply(r))).mod(q);
			return new Pair<BigInteger, BigInteger>(r, s);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new IllegalStateException(ex);
		}
	}

	/**
	 * @param message
	 * @param signature
	 *            Pair&lt;r, s&gt;
	 * @return
	 */
	public boolean verify(byte[] message, Pair<BigInteger, BigInteger> signature) {
		try {
			BigInteger r = signature.getValue0();
			BigInteger s = signature.getValue1();
			BigInteger shaM = DSATools.sha(message);

			BigInteger w = s.modInverse(q);
			BigInteger u1 = shaM.multiply(w).mod(q);
			BigInteger u2 = r.multiply(w).mod(q);
			BigInteger v = g.modPow(u1, p).multiply(y.modPow(u2, p)).mod(p).mod(q);

			return v.equals(r);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new IllegalStateException(ex);
		}
	}

	public BigInteger leakX() {
		return x;
	}

	public BigInteger leakK() {
		return k;
	}

	public BigInteger getP() {
		return p;
	}

	public BigInteger getQ() {
		return q;
	}

	public BigInteger getG() {
		return g;
	}

	public BigInteger getY() {
		return y;
	}

}
