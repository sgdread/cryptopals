package com.vbrain.crypto6.dsa;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

public class DSATools {

	public static BigInteger calculateX(BigInteger r, BigInteger s, BigInteger q, BigInteger k, BigInteger H) {
		BigInteger rInv = r.modInverse(q);
		BigInteger x = s.multiply(k).subtract(H).multiply(rInv).mod(q);
		return x;
	}

	public static BigInteger sha(byte[] message) throws NoSuchAlgorithmException, NoSuchProviderException {
		MessageDigest md = MessageDigest.getInstance("SHA1", "BC");
		byte[] digest = md.digest(message);
		BigInteger shaM = new BigInteger(digest);
		return shaM.abs();
	}

}
