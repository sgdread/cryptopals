package com.vbrain.crypto6;

import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.Signature;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.RSAKeyGenParameterSpec;
import java.util.Arrays;

import javax.crypto.Cipher;

import org.apache.commons.codec.binary.Hex;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class Oracle42 { // Gives The Answer

	private static final int KEY_SIZE = 1024;
	private PublicKey pubKey;
	private PrivateKey privKey;
	private Signature signature;
	private Cipher noPadCipher;

	static {
		Security.addProvider(new BouncyCastleProvider());
	}

	public Oracle42() {
		init();
	}

	public void init() {
		try {
			noPadCipher = Cipher.getInstance("RSA/NONE/NoPadding", "BC");

			KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA", "BC");

			SecureRandom random = new SecureRandom();
			AlgorithmParameterSpec keySpec = new RSAKeyGenParameterSpec(KEY_SIZE, new BigInteger("3"));
			generator.initialize(keySpec, random);

			KeyPair pair = generator.generateKeyPair();

			pubKey = pair.getPublic();
			privKey = pair.getPrivate();
			signature = Signature.getInstance("SHA1withRSA", "BC");
		} catch (Exception ex) {
			throw new IllegalStateException(ex);
		}
	}

	public byte[] sign(byte[] message) {
		try {
			signature.initSign(privKey, new SecureRandom());
			signature.update(message);
			byte[] sigBytes = signature.sign();
			return sigBytes;
		} catch (Exception ex) {
			throw new IllegalStateException(ex);
		}

	}

	public boolean verify(byte[] message, byte[] sigBytes) {
		try {
			// Bouncy Castle works correct, so I have to implement my own buggy verify
			// signature.initVerify(pubKey);
			// signature.update(message);
			// boolean valid = signature.verify(sigBytes);

			noPadCipher.init(Cipher.ENCRYPT_MODE, pubKey);
			byte[] signature = noPadCipher.doFinal(sigBytes);

			try {
				String unpaddedHex = Hex.encodeHexString(unpad(signature));
				
				MessageDigest md = MessageDigest.getInstance("SHA1", "BC");
				byte[] hash = md.digest(message);
				
				String hashHex = Hex.encodeHexString(hash);
				String sha1ASN1Hex = "3021300906052b0e03021a05000414";
	
				return unpaddedHex.startsWith(sha1ASN1Hex + hashHex);
			} catch (IllegalArgumentException ex) {
				return false;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new IllegalStateException(ex);
		}
	}

	private byte[] unpad(byte[] signature) {
		if (signature[0] != 0x00) {
			throw new IllegalArgumentException("Bad Padding");
		}
		if (signature[1] != 0x01) {
			throw new IllegalArgumentException("Bad Padding");
		}

		int offset = -1;
		for (int i = 3; i < signature.length; i++) {
			if (signature[i] == 0x00) {
				offset = ++i;
				break;
			} else if (signature[i] != 0xFF) {
				continue;
			} else {
				throw new IllegalArgumentException("Bad Padding");
			}
		}
		if (offset < 0) {
			throw new IllegalArgumentException("Bad Padding");
		}
		return Arrays.copyOfRange(signature, offset, signature.length);
	}
}
