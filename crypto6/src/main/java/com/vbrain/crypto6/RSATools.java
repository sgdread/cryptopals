package com.vbrain.crypto6;

import java.math.BigInteger;

import org.bouncycastle.crypto.DataLengthException;

public class RSATools {

	public static BigInteger convertBytesToBigInteger(byte[] input, BigInteger modulus) {
		return convertBytesToBigInteger(input, modulus, false);
	}
	
	public static byte[] convertBigIntegerToBytes(BigInteger input, BigInteger modulus) {
		return convertBigIntegerToBytes(input, modulus, false);
	}
	
	public static BigInteger convertBytesToBigInteger(byte[] input, BigInteger modulus, boolean forEncryption) {
		return convertInput(input, 0, input.length, modulus, forEncryption);
	}

	public static byte[] convertBigIntegerToBytes(BigInteger input, BigInteger modulus, boolean forEncryption) {
		return convertOutput(input, modulus, forEncryption);
	}

	/**
	 * Return the maximum size for an input block to this engine. For RSA this
	 * is always one byte less than the key size on encryption, and the same
	 * length as the key size on decryption.
	 * 
	 * @param forEncryption
	 * 
	 * @return maximum size for an input block.
	 */
	public static int getInputBlockSize(BigInteger modulus, boolean forEncryption) {
		int bitSize = modulus.bitLength();

		if (forEncryption) {
			return (bitSize + 7) / 8 - 1;
		} else {
			return (bitSize + 7) / 8;
		}
	}

	/**
	 * Return the maximum size for an output block to this engine. For RSA this
	 * is always one byte less than the key size on decryption, and the same
	 * length as the key size on encryption.
	 * 
	 * @return maximum size for an output block.
	 */
	public static int getOutputBlockSize(BigInteger modulus, boolean forEncryption) {
		int bitSize = modulus.bitLength();

		if (forEncryption) {
			return (bitSize + 7) / 8;
		} else {
			return (bitSize + 7) / 8 - 1;
		}
	}

	public static BigInteger convertInput(byte[] in, int inOff, int inLen, BigInteger modulus, boolean forEncryption) {
		if (inLen > (getInputBlockSize(modulus, forEncryption) + 1)) {
			throw new DataLengthException("input too large for RSA cipher.");
		} else if (inLen == (getInputBlockSize(modulus, forEncryption) + 1) && !forEncryption) {
			throw new DataLengthException("input too large for RSA cipher.");
		}

		byte[] block;

		if (inOff != 0 || inLen != in.length) {
			block = new byte[inLen];

			System.arraycopy(in, inOff, block, 0, inLen);
		} else {
			block = in;
		}

		BigInteger res = new BigInteger(1, block);
		if (res.compareTo(modulus) >= 0) {
			throw new DataLengthException("input too large for RSA cipher.");
		}

		return res;
	}

	public static byte[] convertOutput(BigInteger result, BigInteger modulus, boolean forEncryption) {
		byte[] output = result.toByteArray();

		if (forEncryption) {
			if (output[0] == 0 && output.length > getOutputBlockSize(modulus, forEncryption))
			// have ended up with an extra zero byte, copy down.
			{
				byte[] tmp = new byte[output.length - 1];

				System.arraycopy(output, 1, tmp, 0, tmp.length);

				return tmp;
			}

			if (output.length < getOutputBlockSize(modulus, forEncryption))
			// have ended up with less bytes than normal, lengthen
			{
				byte[] tmp = new byte[getOutputBlockSize(modulus, forEncryption)];

				System.arraycopy(output, 0, tmp, tmp.length - output.length, output.length);

				return tmp;
			}
		} else {
			if (output[0] == 0) // have ended up with an extra zero byte, copy
								// down.
			{
				byte[] tmp = new byte[output.length - 1];

				System.arraycopy(output, 1, tmp, 0, tmp.length);

				return tmp;
			}
		}

		return output;
	}

}
