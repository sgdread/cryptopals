package com.vbrain.crypto6;

import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;

import javax.crypto.Cipher;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.provider.JCERSAPublicKey;

public class Oracle46 {

	private static final int KEY_SIZE = 1024;

	private PublicKey pubKey;
	private PrivateKey privKey;

	private Cipher cipher;
	static {
		Security.addProvider(new BouncyCastleProvider());
	}

	public Oracle46() {
		init();
	}

	public void init() {
		try {
			KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA", "BC");

			SecureRandom random = new SecureRandom();
			generator.initialize(KEY_SIZE, random);

			KeyPair pair = generator.generateKeyPair();

			pubKey = pair.getPublic();
			privKey = pair.getPrivate();

			cipher = Cipher.getInstance("RSA/NONE/NoPadding");
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new IllegalStateException(ex);
		}
	}

	public boolean isEven(byte[] ciphertext) {
		JCERSAPublicKey jceRsaPubKey = (JCERSAPublicKey) pubKey;
		byte[] plaintext = decrypt(ciphertext);
		BigInteger bi = RSATools.convertBytesToBigInteger(plaintext, jceRsaPubKey.getModulus());
		boolean even = (bi.intValue() & 1) == 0;
		return even;
	}

	public byte[] encrypt(byte[] plaintext) {
		try {
			cipher.init(Cipher.ENCRYPT_MODE, pubKey);
			byte[] ciphertext = cipher.doFinal(plaintext);
			return ciphertext;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new IllegalStateException(ex);
		}
	}

	public byte[] decrypt(byte[] ciphertext) {
		try {
			cipher.init(Cipher.DECRYPT_MODE, privKey);
			byte[] plaintext = cipher.doFinal(ciphertext);
			return plaintext;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new IllegalStateException(ex);
		}
	}

	public PublicKey getPubKey() {
		return pubKey;
	}

}
