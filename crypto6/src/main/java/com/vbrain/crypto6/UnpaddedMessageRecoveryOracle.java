package com.vbrain.crypto6;

import java.math.BigInteger;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;
import java.security.Security;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.RSAKeyGenParameterSpec;
import java.util.Set;

import javax.crypto.Cipher;

import org.apache.commons.codec.binary.Hex;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.provider.JCERSAPublicKey;

import com.google.common.collect.Sets;

public class UnpaddedMessageRecoveryOracle {

	static {
		Security.addProvider(new BouncyCastleProvider());
	}

	private Key pubKey;
	private Key privKey;
	private Cipher rsaCipher;

	private Set<String> processedRequests;

	public UnpaddedMessageRecoveryOracle() {
		init();
	}

	public void init() {
		processedRequests = Sets.newHashSet();
		try {
			// Create the public and private keys
			KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA", "BC");

			SecureRandom random = new SecureRandom();
			AlgorithmParameterSpec keySpec = new RSAKeyGenParameterSpec(1024, new BigInteger("3"));
			generator.initialize(keySpec, random);

			KeyPair pair = generator.generateKeyPair();
			pubKey = pair.getPublic();

			privKey = pair.getPrivate();

			rsaCipher = Cipher.getInstance("RSA/NONE/NoPadding", "BC");
		} catch (Exception ex) {
			throw new IllegalStateException(ex);
		}
	}

	public String process(String hexBlob) {
		if (!processedRequests.contains(hexBlob)) {
			processedRequests.add(hexBlob);
			return decrypt(hexBlob);
		}
		throw new IllegalArgumentException("Duplicate request");
	}

	protected String decrypt(String hexBlob) {
		try {
			byte[] ciphertext = Hex.decodeHex(hexBlob.toCharArray());
			rsaCipher.init(Cipher.DECRYPT_MODE, privKey);
			byte[] plaintext = rsaCipher.doFinal(ciphertext);
			return Hex.encodeHexString(plaintext);
		} catch (Exception ex) {
			throw new IllegalStateException(ex);
		}
	}

	protected String encrypt(String plaintextHex) {
		try {
			byte[] plaintext = Hex.decodeHex(plaintextHex.toCharArray());
			rsaCipher.init(Cipher.ENCRYPT_MODE, pubKey);
			rsaCipher.update(plaintext);
			byte[] ciphertext = rsaCipher.doFinal();
			return Hex.encodeHexString(ciphertext);
		} catch (Exception ex) {
			throw new IllegalStateException(ex);
		}
	}

	public JCERSAPublicKey getPubKey() {
		return (JCERSAPublicKey) pubKey;
	}

}
