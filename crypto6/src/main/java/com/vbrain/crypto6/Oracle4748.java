package com.vbrain.crypto6;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;

import org.apache.commons.codec.binary.Hex;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class Oracle4748 {

	public static final int KEY_SIZE = 768;

	private PublicKey pubKey;
	private PrivateKey privKey;

	private Cipher cipher;

	private Cipher noPadCipher;
	static {
		Security.addProvider(new BouncyCastleProvider());
	}

	public Oracle4748() {
		init();
	}

	public void init() {
		try {
			KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA", "BC");
			generator.initialize(KEY_SIZE, new SecureRandom());

			KeyPair pair = generator.generateKeyPair();

			pubKey = pair.getPublic();
			privKey = pair.getPrivate();

			cipher = Cipher.getInstance("RSA/NONE/PKCS1Padding", "BC");
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new IllegalStateException(ex);
		}
	}

	public boolean isValid(byte[] ciphertext) {
		byte[] plaintext = decryptNoPad(ciphertext);

		String hex = Hex.encodeHexString(plaintext);
		hex = addHeadingZeroes(hex);
		return hex.startsWith("0002");
	}

	public static String addHeadingZeroes(String hex) {
		while (hex.length() * 4 < KEY_SIZE) {
			hex = "0" + hex;
		}
		return hex;
	}

	public byte[] encrypt(byte[] plaintext) {
		try {
			cipher.init(Cipher.ENCRYPT_MODE, pubKey);
			byte[] ciphertext = cipher.doFinal(plaintext);
			return ciphertext;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new IllegalStateException(ex);
		}
	}

	public byte[] encryptNoPad(byte[] plaintext) {
		try {
			Cipher cipher = Cipher.getInstance("RSA/NONE/NoPadding", "BC");
			cipher.init(Cipher.ENCRYPT_MODE, pubKey);
			byte[] ciphertext = cipher.doFinal(plaintext);
			return ciphertext;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new IllegalStateException(ex);
		}
	}

	public byte[] decrypt(byte[] ciphertext) throws BadPaddingException {
		try {
			cipher.init(Cipher.DECRYPT_MODE, privKey);
			byte[] plaintext = cipher.doFinal(ciphertext);
			return plaintext;
		} catch (BadPaddingException ex) {
			throw ex;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new IllegalStateException(ex);
		}
	}

	public byte[] decryptNoPad(byte[] ciphertext) {
		try {
			noPadCipher = Cipher.getInstance("RSA/NONE/NoPadding", "BC");
			noPadCipher.init(Cipher.DECRYPT_MODE, privKey);
			byte[] plaintext = noPadCipher.doFinal(ciphertext);
			return plaintext;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new IllegalStateException(ex);
		}
	}

	public PublicKey getPubKey() {
		return pubKey;
	}

	public PrivateKey getPrivKey() {
		return privKey;
	}

}
