package com.vbrain.crypto6;

import java.math.BigInteger;
import java.security.Security;
import java.util.Set;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.javatuples.Pair;

import com.google.common.collect.Sets;
import com.vbrain.crypto5.RSA;

public class UnpaddedMessageRecoveryOracle1 {

	static {
		Security.addProvider(new BouncyCastleProvider());
	}

	private Pair<BigInteger, BigInteger> pubKey;

	private Set<String> processedRequests;
	private RSA rsa;

	public UnpaddedMessageRecoveryOracle1() {
		init();
	}

	public void init() {
		processedRequests = Sets.newHashSet();
		rsa = new RSA();
		pubKey = rsa.getPubKey();
	}

	public String process(String hexBlob) {
		if (!processedRequests.contains(hexBlob)) {
			processedRequests.add(hexBlob);
			return decrypt(hexBlob);
		}
		throw new IllegalArgumentException("Duplicate request");
	}
	
	public BigInteger process(BigInteger input) {
		if (!processedRequests.contains(input.toString(16))) {
			processedRequests.add(input.toString(16));
			return decrypt(input);
		}
		throw new IllegalArgumentException("Duplicate request");
	}

	protected String decrypt(String hexBlob) {
		return rsa.decrypt(hexBlob);
	}

	protected String encrypt(String plaintextHex) {
		return rsa.encrypt(plaintextHex);
	}

	protected BigInteger decrypt(BigInteger ciphertext) {
		return rsa.decrypt(ciphertext);
	}

	protected BigInteger encrypt(BigInteger plaintext) {
		return rsa.encrypt(plaintext);
	}

	public Pair<BigInteger, BigInteger> getPubKey() {
		return pubKey;
	}

}
