package com.vbrain.crypto5;

import java.math.BigInteger;
import java.security.SecureRandom;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.javatuples.Pair;

public class RSA {

	private Pair<BigInteger, BigInteger> pubKey;
	private Pair<BigInteger, BigInteger> privKey;
	private BigInteger e;
	private int keySize;

	public RSA() {
		this.keySize = 512;
		init();
	}

	public RSA(int keySize) {
		this.keySize = keySize;
		init();
	}

	private void init() {
		e = BigInteger.valueOf(3);
		BigInteger d = null;
		BigInteger n = null;

		BigInteger lo = BigInteger.valueOf(2).pow(keySize - 8);
		BigInteger hi = BigInteger.valueOf(2).pow(keySize).subtract(BigInteger.ONE);
		
		while (d == null) {
			try {
				BigInteger p = BigInteger.probablePrime(keySize/2, new SecureRandom());
				BigInteger q = BigInteger.probablePrime(keySize/2, new SecureRandom());
				n = p.multiply(q);
				
				if (n.compareTo(lo) < 0 || n.compareTo(hi) > 0) {
					continue;
				}
				// totient
				BigInteger et = (p.subtract(BigInteger.ONE).multiply(q.subtract(BigInteger.ONE)));
				d = e.modInverse(et);
			} catch (ArithmeticException ex) {
				// probable primes generated not-invertible e and et
			}
		}

		pubKey = Pair.with(e, n);
		privKey = Pair.with(d, n);
	}

	public String encrypt(String data) {
		BigInteger m = new BigInteger(Hex.encodeHexString(data.getBytes()), 16);
		BigInteger c = encrypt(m);
		return Hex.encodeHexString(c.toByteArray());
	}

	public String decrypt(String ciphertext) {
		try {
			BigInteger c = new BigInteger(Hex.decodeHex(ciphertext.toCharArray()));
			BigInteger m = decrypt(c);
			return m.toString(16);

		} catch (DecoderException ex) {
			throw new IllegalArgumentException(ex);
		}
	}

	public BigInteger encrypt(BigInteger m) {
		BigInteger c = m.modPow(pubKey.getValue0(), pubKey.getValue1());
		return c;
	}

	public BigInteger decrypt(BigInteger c) {
		BigInteger m = c.modPow(privKey.getValue0(), privKey.getValue1());
		return m;
	}

	public Pair<BigInteger, BigInteger> getPubKey() {
		return pubKey;
	}

	public Pair<BigInteger, BigInteger> getPrivKey() {
		return privKey;
	}

}
