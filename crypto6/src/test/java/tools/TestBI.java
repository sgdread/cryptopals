package tools;

import static org.junit.Assert.assertEquals;

import java.math.BigInteger;

import org.junit.Test;



public class TestBI {

	@Test
	public void testDivCeil() throws Exception {
		assertEquals(4, BI.divCeil(BigInteger.valueOf(12), BigInteger.valueOf(3)).intValue());
		assertEquals(5, BI.divCeil(BigInteger.valueOf(13), BigInteger.valueOf(3)).intValue());
	}

	
	@Test
	public void testDivFloor() throws Exception {
		assertEquals(4, BI.divFloor(BigInteger.valueOf(12), BigInteger.valueOf(3)).intValue());
		assertEquals(4, BI.divFloor(BigInteger.valueOf(13), BigInteger.valueOf(3)).intValue());
	}
	
	@Test
	public void testMax() throws Exception {
		assertEquals(12, BI.max(BigInteger.valueOf(12), BigInteger.valueOf(3)).intValue());
		assertEquals(12, BI.max(BigInteger.valueOf(3), BigInteger.valueOf(12)).intValue());
		
	}
	
	@Test
	public void testMin() throws Exception {
		assertEquals(3, BI.min(BigInteger.valueOf(12), BigInteger.valueOf(3)).intValue());
		assertEquals(3, BI.min(BigInteger.valueOf(3), BigInteger.valueOf(12)).intValue());
		
	}
}
