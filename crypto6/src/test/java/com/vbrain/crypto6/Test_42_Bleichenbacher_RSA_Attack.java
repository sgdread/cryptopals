package com.vbrain.crypto6;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.security.Security;

import org.apache.commons.codec.binary.Hex;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nevec.rjm.BigDecimalMath;

public class Test_42_Bleichenbacher_RSA_Attack {

	private static final BigInteger N = BigInteger.probablePrime(1024, new SecureRandom());
	private static final int KEY_SIZE = 1024;
	private Oracle42 orcl;

	@BeforeClass
	public static void beforeClass() {
		Security.addProvider(new BouncyCastleProvider());
	}

	@Before
	public void setup() throws Exception {
		orcl = new Oracle42();
	}

	@Test
	public void testSignVerify() throws Exception {
		String plaintext = "hello world!";

		byte[] signature = orcl.sign(plaintext.getBytes());

		assertTrue(orcl.verify(plaintext.getBytes(), signature));
		assertFalse(orcl.verify("wrongText".getBytes(), signature));
	}

	@Test
	public void testAttack() throws Exception {
		String plaintext = "hi mom";
		byte[] message = plaintext.getBytes();

		MessageDigest md = MessageDigest.getInstance("SHA1", "BC");
		md.update(message);
		byte[] hash = md.digest();

		String hashHex = Hex.encodeHexString(hash);
		String sha1ASN1Hex = "3021300906052b0e03021a05000414"; // SHA1 ASN.1
		String paddingPrefixHex = "0001ffff00";

		String forgedPrefixHex = paddingPrefixHex + sha1ASN1Hex + hashHex;
		byte[] forgedPrefix = Hex.decodeHex(forgedPrefixHex.toCharArray());

		int len = KEY_SIZE / 8;
		ByteBuffer buff = ByteBuffer.allocate(len).put(forgedPrefix);
		for (int i = 0; i < len - forgedPrefix.length; i++) {
			buff.put((byte) 0x00);
		}
		byte[] arr = buff.array();
		BigInteger raw1 = RSATools.convertBytesToBigInteger(arr, N, false);
		BigInteger cbrt = BigDecimalMath.cbrt(new BigDecimal(raw1)).toBigInteger();

		byte[] forgedSignature = RSATools.convertBigIntegerToBytes(cbrt.add(BigInteger.ONE), N, false);

		boolean valid = orcl.verify(message, forgedSignature);

		assertTrue(valid);
	}

}
