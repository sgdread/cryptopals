package com.vbrain.crypto6;

import static java.math.BigInteger.ZERO;
import static org.junit.Assert.assertEquals;

import java.math.BigInteger;
import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.provider.JCERSAPublicKey;
import org.bouncycastle.util.encoders.Base64;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class Test_46_RSA_BitOracle {

	private static final BigInteger TWO = BigInteger.valueOf(2);
	private Oracle46 orcl;
	private BigInteger n;
	private BigInteger e;
	private byte[] ciphertext;
	private String unknownPlaintext;

	@BeforeClass
	public static void beforeClass() {
		Security.addProvider(new BouncyCastleProvider());
	}

	@Before
	public void setup() throws Exception {
		orcl = new Oracle46();
		JCERSAPublicKey pubKey = (JCERSAPublicKey) orcl.getPubKey();
		n = pubKey.getModulus();
		e = pubKey.getPublicExponent();

		String plainB64 = "VGhhdCdzIHdoeSBJIGZvdW5kIHlvdSBkb24ndCBwbGF5IGFyb3VuZCB3aXRoIHRoZSBGdW5reSBDb2xkIE1lZGluYQ==";
		byte[] plaintext = Base64.decode(plainB64);
		unknownPlaintext = new String(plaintext); // For verification only
		ciphertext = orcl.encrypt(plaintext);
	}

	@Test
	public void testAttack() throws Exception {
		BigInteger ciphertextBI = RSATools.convertBytesToBigInteger(ciphertext, n);
		BigInteger mult = TWO.modPow(e, n);

		BigInteger low = ZERO;
		BigInteger up = n;
		for (int i = 1; i < log2(n); i++) {
			ciphertextBI = ciphertextBI.multiply(mult).mod(n);
			BigInteger h = up.add(low).divide(TWO);
			if (orcl.isEven(RSATools.convertBigIntegerToBytes(ciphertextBI, n))) { // left side
				up = h;
			} else { // right side
				low = h;
			}
		}

		// Last char can be wrong, but the solution field is VERY small
		String decodedPlaintextU = new String(RSATools.convertBigIntegerToBytes(up, n));

		assertEquals(unknownPlaintext.substring(0, unknownPlaintext.length() - 1), decodedPlaintextU.substring(0, decodedPlaintextU.length() - 1));

	}

	private static int log2(BigInteger value) {
		int i = 0;
		while (value.compareTo(TWO) >= 0) {
			value = value.divide(TWO);
			i++;
		}
		return i;
	}

}
