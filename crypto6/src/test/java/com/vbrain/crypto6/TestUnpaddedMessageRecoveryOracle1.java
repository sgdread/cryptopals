package com.vbrain.crypto6;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.math.BigInteger;

import org.apache.commons.codec.binary.Hex;
import org.junit.Test;

public class TestUnpaddedMessageRecoveryOracle1 {

	@Test
	public void testEncryptDecrypt() throws Exception {
		UnpaddedMessageRecoveryOracle1 orcl = new UnpaddedMessageRecoveryOracle1();

		String plaintextHex = Hex.encodeHexString("testData".getBytes());
		String ciphertextHex = orcl.encrypt(plaintextHex);
		String decryptedPlaintextHex = new String(Hex.decodeHex(orcl.decrypt(ciphertextHex).toCharArray()));

		assertEquals(plaintextHex, decryptedPlaintextHex);
		assertNotEquals(plaintextHex, ciphertextHex);
	}

	@Test
	public void testEncryptDecrypt2() throws Exception {
		UnpaddedMessageRecoveryOracle1 orcl = new UnpaddedMessageRecoveryOracle1();

		BigInteger plaintext = new BigInteger("12345678901234567890");
		BigInteger ciphertext = orcl.encrypt(plaintext);
		BigInteger decryptedPlaintext = orcl.decrypt(ciphertext);

		assertEquals(plaintext, decryptedPlaintext);
		assertNotEquals(decryptedPlaintext, ciphertext);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testProcess() throws Exception {
		UnpaddedMessageRecoveryOracle1 orcl = new UnpaddedMessageRecoveryOracle1();

		String result = null;
		try {
			result = orcl.process("1234");
		} catch (IllegalArgumentException ex) {
			fail("request expected to be processed");
		}
		assertNotNull(result);
		result = orcl.process("1234"); // << throw
	}

}
