package com.vbrain.crypto6;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.apache.commons.codec.binary.Hex;
import org.junit.Test;

public class TestUnpaddedMessageRecoveryOracle {

	@Test
	public void testEncryptDecrypt() throws Exception {
		UnpaddedMessageRecoveryOracle orcl = new UnpaddedMessageRecoveryOracle();

		String plaintextHex = Hex.encodeHexString("testData".getBytes());
		String ciphertextHex = orcl.encrypt(plaintextHex);
		String decryptedPlaintextHex = orcl.decrypt(ciphertextHex);

		assertEquals(plaintextHex, decryptedPlaintextHex);
		assertNotEquals(plaintextHex, ciphertextHex);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testProcess() throws Exception {
		UnpaddedMessageRecoveryOracle orcl = new UnpaddedMessageRecoveryOracle();

		String result = null;
		try {
			result = orcl.process("1234");
		} catch (IllegalArgumentException ex) {
			fail("request expected to be processed");
		}
		assertNotNull(result);
		result = orcl.process("1234"); // << throw
	}

}
