package com.vbrain.crypto6;

import static org.junit.Assert.assertEquals;

import java.math.BigInteger;
import java.security.SecureRandom;

import org.javatuples.Pair;
import org.junit.Before;
import org.junit.Test;

public class Test_41_UnpaddedMessageRecoveryOracle1 {

	private UnpaddedMessageRecoveryOracle1 orcl;

	// Use for verification only
	private static final BigInteger SECRET_PLAINTEXT = new BigInteger("12345");

	private BigInteger N; // public modulus
	private BigInteger E; // public exponent

	private BigInteger capturedCiphertext;

	@Before
	public void setup() {
		orcl = new UnpaddedMessageRecoveryOracle1();

		Pair<BigInteger, BigInteger> pubKey = orcl.getPubKey();
		N = pubKey.getValue1();
		E = pubKey.getValue0();
		
		System.out.println("n=" + N);
		System.out.println("e=" + E);

		capturedCiphertext = orcl.encrypt(SECRET_PLAINTEXT);
	}

	@Test
	public void testProcess() throws Exception {
		BigInteger C = capturedCiphertext;
		BigInteger S = new BigInteger(1024, new SecureRandom()).mod(N);
		BigInteger Cprime = S.modPow(E, N).multiply(C).mod(N);
		BigInteger Pprime = orcl.process(Cprime);
		BigInteger P = Pprime.multiply(S.modInverse(N)).mod(N);
		assertEquals(SECRET_PLAINTEXT, P);
	}

}
