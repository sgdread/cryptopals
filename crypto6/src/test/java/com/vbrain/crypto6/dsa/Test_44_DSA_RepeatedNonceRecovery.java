package com.vbrain.crypto6.dsa;

import static org.junit.Assert.assertEquals;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.Security;

import org.apache.commons.codec.binary.Hex;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Test;

public class Test_44_DSA_RepeatedNonceRecovery {

	static {
		Security.addProvider(new BouncyCastleProvider());
	}

	@Test
	public void test() throws Exception {
		// 1)
		// msg: Listen for me, you better listen for me now.
		// s: 1267396447369736888040262262183731677867615804316
		// r: 1105520928110492191417703162650245113664610474875
		// m: a4db3de27e2db3e5ef085ced2bced91b82e0df19
		// 2)
		// msg: Pure black people mon is all I mon know.
		// s: 1021643638653719618255840562522049391608552714967
		// r: 1105520928110492191417703162650245113664610474875
		// m: d22804c4899b522b23eda34d2137cd8cc22b9ce8

		BigInteger q = new BigInteger("f4f47f05794b256174bba6e9b396a7707e563c5b", 16);

		BigInteger m1 = new BigInteger("a4db3de27e2db3e5ef085ced2bced91b82e0df19", 16);
		BigInteger m2 = new BigInteger("d22804c4899b522b23eda34d2137cd8cc22b9ce8", 16);

		BigInteger s1 = new BigInteger("1267396447369736888040262262183731677867615804316");
		BigInteger s2 = new BigInteger("1021643638653719618255840562522049391608552714967");

		// Not working with my implementation - BigDecimal conversion in 
		// Ruby and Java are different
		
		// String msg1 = "Listen for me, you better listen for me now.";
		// BigInteger H1 = DSATools.sha(msg1.getBytes());
		//
		// String msg2 = "Pure black people mon is all I mon know.";
		// BigInteger H2 = DSATools.sha(msg2.getBytes());

		// --- k
		BigInteger s1s2Inv = s1.subtract(s2).modInverse(q);
		BigInteger k = m1.subtract(m2).multiply(s1s2Inv).mod(q);

		// Getting x
		BigInteger r1 = new BigInteger("1105520928110492191417703162650245113664610474875");

		BigInteger x1 = DSATools.calculateX(r1, s1, q, k, m1);

		// Getting x
		BigInteger r2 = new BigInteger("1105520928110492191417703162650245113664610474875");

		BigInteger x2 = DSATools.calculateX(r2, s2, q, k, m2);

		assertEquals(x1, x2); // Cross-checking

		String xHex = x1.toString(16);

		String xSha1Hex = Hex.encodeHexString(MessageDigest.getInstance("SHA1").digest(xHex.getBytes()));
		
		assertEquals("ca8f6f7c66fa362d40760d135b763eb8527d3d52", xSha1Hex);
		
		System.out.println("x = " + x1.toString(16));
		System.out.println("SHA1(xHex) = " + xSha1Hex);
	}

}
