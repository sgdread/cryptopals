package com.vbrain.crypto6.dsa;

import static org.junit.Assert.assertTrue;

import java.math.BigInteger;

import org.javatuples.Pair;
import org.junit.Test;

public class Test_45_DSA_ParameterTampering {

	@Test
	public void testVerifyGequalsP() throws Exception {
		DSA dsa = new DSA();

		BigInteger p = dsa.getP();
		BigInteger q = dsa.getQ();
		BigInteger g = p.add(BigInteger.ONE); // <<<< BAD!

		dsa.init(p, q, g);

		// Magic signature
		BigInteger r = BigInteger.ONE;
		BigInteger s = BigInteger.ONE;

		Pair<BigInteger, BigInteger> signature = new Pair<BigInteger, BigInteger>(r, s);

		assertTrue(dsa.verify("Hello, world".getBytes(), signature));
		assertTrue(dsa.verify("Goodbye, world".getBytes(), signature));
		assertTrue(dsa.verify("ghuirghh4g93hguiah".getBytes(), signature));
	}

}
