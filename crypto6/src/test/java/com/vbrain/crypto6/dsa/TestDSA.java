package com.vbrain.crypto6.dsa;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigInteger;

import org.javatuples.Pair;
import org.junit.Test;

public class TestDSA {

	@Test
	public void testLeakedK() throws Exception {
		String msg = "abc";
		
		DSA dsa = new DSA();
		Pair<BigInteger, BigInteger> signature = dsa.sign(msg.getBytes());
		
		BigInteger p = dsa.getP(); // Known
		BigInteger q = dsa.getQ(); // Known
		BigInteger g = dsa.getG(); // Known

		BigInteger k = dsa.leakK(); // Leaked

		BigInteger r = signature.getValue0();
		BigInteger s = signature.getValue1();
		BigInteger H = DSATools.sha(msg.getBytes());
		
		BigInteger x = DSATools.calculateX(r, s, q, k, H); 

		// -------------------------------------- Sign offline
		String forgedMessage = "mal";
		BigInteger rMal = g.modPow(k, p).mod(q);

		BigInteger kInv = k.modInverse(q);

		BigInteger HMal = DSATools.sha(forgedMessage.getBytes());
		BigInteger sMal = kInv.multiply(HMal .add(x.multiply(r))).mod(q);
		
		boolean valid = dsa.verify(forgedMessage.getBytes(), new Pair<BigInteger, BigInteger>(rMal, sMal));
		
		assertTrue(valid);
	}

	@Test
	public void testSignVerify() throws Exception {
		String msg = "abc";
		DSA dsa = new DSA();
		Pair<BigInteger, BigInteger> signature = dsa.sign(msg.getBytes());
		assertTrue(dsa.verify(msg.getBytes(), signature));
	}

	@Test
	public void testRawCalcs() throws Exception {
		// http://www.itl.nist.gov/fipspubs/fip186.htm
		BigInteger p = new BigInteger(
				"d411a4a0e393f6aab0f08b14d18458665b3e4dbdce2544543fe365cf71c8622412db6e7dd02bbe13d88c58d7263e90236af17ac8a9fe5f249cc81f427fc543f7", 16);
		BigInteger q = new BigInteger("b20db0b101df0c6624fc1392ba55f77d577481e5", 16);
		BigInteger g = new BigInteger(
				"b3085510021f999049a9e7cd3872ce9958186b5007e7adaf25248b58a3dc4f71781d21f2df89b71747bd54b323bbecc443ec1d3e020dadabbf7822578255c104", 16);

		BigInteger x = new BigInteger("6b2cd935d0192d54e2c942b574c80102c8f8ef67", 16);
		BigInteger k = new BigInteger("79577ddcaafddc038b865b19f8eb1ada8a2838c6", 16);

		BigInteger kInv = k.modInverse(q);
		System.out.println("kInv = " + kInv.toString(16));

		BigInteger y = g.modPow(x, p);
		System.out.println("y = " + y.toString(16));

		BigInteger shaM = new BigInteger("0164b1118a914cd2a5e74c4f7ff082c4d97f1edf880", 16);

		BigInteger r = g.modPow(k, p).mod(q);
		System.out.println("r = " + r.toString(16));

		BigInteger s = kInv.multiply(shaM.add(x.multiply(r))).mod(q);
		System.out.println("s = " + s.toString(16));

		BigInteger w = s.modInverse(q);
		System.out.println("w = " + w.toString(16));

		BigInteger u1 = shaM.multiply(w).mod(q);
		System.out.println("u1 = " + u1.toString(16));

		BigInteger u2 = r.multiply(w).mod(q);
		System.out.println("u2 = " + u2.toString(16));

		BigInteger v = g.modPow(u1, p).multiply(y.modPow(u2, p)).mod(p).mod(q);
		System.out.println("v = " + v.toString(16));

		assertEquals(v, r);
	}
}
