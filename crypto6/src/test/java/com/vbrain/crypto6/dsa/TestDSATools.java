package com.vbrain.crypto6.dsa;

import static org.junit.Assert.assertEquals;

import java.math.BigInteger;

import org.javatuples.Pair;
import org.junit.Test;

public class TestDSATools {

	@Test
	public void testCalculateX() throws Exception {
		String msg = "abc";
		DSA dsa = new DSA();
		Pair<BigInteger, BigInteger> signature = dsa.sign(msg.getBytes());

		BigInteger r = signature.getValue0();
		BigInteger s = signature.getValue1();
		BigInteger q = dsa.getQ();
		BigInteger k = dsa.leakK();
		BigInteger H = DSATools.sha(msg.getBytes());

		BigInteger x = DSATools.calculateX(r, s, q, k, H);
		assertEquals(dsa.leakX(), x);
	}

}
