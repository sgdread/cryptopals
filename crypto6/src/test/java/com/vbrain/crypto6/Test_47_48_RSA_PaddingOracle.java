package com.vbrain.crypto6;

import static java.math.BigInteger.ONE;
import static java.math.BigInteger.ZERO;
import static org.junit.Assert.assertEquals;

import java.math.BigInteger;
import java.security.Security;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.codec.binary.Hex;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.provider.JCERSAPublicKey;
import org.javatuples.Pair;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.common.collect.Sets;

public class Test_47_48_RSA_PaddingOracle {

	private static final BigInteger THR = BigInteger.valueOf(3);
	private static final BigInteger TWO = BigInteger.valueOf(2);
	private Oracle4748 orcl;
	private byte[] ciphertext;
	private String unknownPlaintext;
	private BigInteger n;
	private BigInteger e;
	private BigInteger B;
	private BigInteger B2;
	private BigInteger B3;

	@BeforeClass
	public static void beforeClass() {
		Security.addProvider(new BouncyCastleProvider());
	}

	@Before
	public void setup() throws Exception {
		orcl = new Oracle4748();

		JCERSAPublicKey pubKey = (JCERSAPublicKey) orcl.getPubKey();
		n = pubKey.getModulus();
		e = pubKey.getPublicExponent();

		int k = Oracle4748.KEY_SIZE;
		B = TWO.pow(k - 16);
		B2 = B.multiply(TWO);
		B3 = B.multiply(THR);

		unknownPlaintext = "hhh"; // For verification only
		byte[] plaintext = unknownPlaintext.getBytes();
		ciphertext = orcl.encrypt(plaintext);
	}

	@Test
	public void testAttack() throws Exception {
		System.out.println("Original: " + Hex.encodeHexString(orcl.decryptNoPad(ciphertext)));
		BigInteger solution = search(RSATools.convertBytesToBigInteger(ciphertext, n));
		String original = Hex.encodeHexString(orcl.decryptNoPad(ciphertext));
		// Leading zeroes are lost disappear in integer math
		String solutionHex = "0" + solution.toString(16); 
		assertEquals(original, solutionHex);
	}

	private boolean callOracle(BigInteger m) {
		return orcl.isValid(RSATools.convertBigIntegerToBytes(m, n));
	}

	public BigInteger search(BigInteger c0) { // 2a
		BigInteger solution = ZERO;
		int i = 1;
		BigInteger si = null;
		BigInteger si1 = null; // s(i-1)
		BigInteger result = null;

		BigInteger a = B2;
		BigInteger b = B3.subtract(ONE);
		Pair<BigInteger, BigInteger> startPair = new Pair<BigInteger, BigInteger>(a, b);
		Set<Pair<BigInteger, BigInteger>> M = Sets.newHashSet();
		M.add(startPair);

		while (result == null) {

			if (i == 1) { // -------------------------------------------------- 2a
				si = ceil(n, B3);
				System.out.println("Starting s" + i + " search from " + si);
				si = findS(c0, si);
				System.out.println("   s_" + i + "=" + si);
			} else if (M.size() > 1) { // ------------------------------------- 2b
				System.out.println("Multiple intervals");
				si = findS(c0, si1.add(ONE));
				System.out.println("   s_" + i + "=" + si);
			} else if (M.size() == 1) { // ------------------------------------ 2c
				System.out.println("One interval left");
				Pair<BigInteger, BigInteger> interval = M.iterator().next();
				a = interval.getValue0();
				b = interval.getValue1();

				BigInteger ri = ceil(b.multiply(si1).subtract(B2).multiply(TWO), n);

				boolean found = false;
				while (!found) {
					BigInteger siLo = floor(B2.add(ri.multiply(n)), b);
					BigInteger siHi = ceil(B3.add(ri.multiply(n)), a);
					for (si = siLo; si.compareTo(siHi) < 0; si = si.add(ONE)) {
						BigInteger mi = c0.multiply(si.modPow(e, n)).mod(n);
						if (callOracle(mi)) { // Correct padding
							found = true;
							break;
						}
					}
					if (!found) {
						ri = ri.add(ONE);
					}
				}
				System.out.println("   s_" + i + "=" + si);
			}

			// ---------------------------------------------------------------- 3

			
			Set<Pair<BigInteger, BigInteger>> newM = Sets.newHashSet();
			for (Iterator<Pair<BigInteger, BigInteger>> j = M.iterator(); j.hasNext();) {
				Pair<BigInteger, BigInteger> interval = j.next();
				a = interval.getValue0();
				b = interval.getValue1();
				newM.addAll(narrow(a, b, si));
			}
			M = newM;

			// ---------------------------------------------------------------- 4
			Pair<BigInteger, BigInteger> interval = M.iterator().next();
			if (M.size() == 1 && interval.getValue0().equals(interval.getValue1())) {
				System.out.println("Original: " + Hex.encodeHexString(orcl.decryptNoPad(ciphertext)));
				System.out.println("Solution: " + interval.getValue0().toString(16));
				solution = interval.getValue0();
				break;
			} else {
				i++;
				si1 = si;
			}
		}
		return solution;
	}

	public Set<Pair<BigInteger, BigInteger>> narrow(BigInteger a, BigInteger b, BigInteger s) {
		Set<Pair<BigInteger, BigInteger>> result = Sets.newHashSet();

		BigInteger rLo = floor(a.multiply(s).subtract(B3).add(ONE), n).subtract(ONE);
		BigInteger rHi = ceil(b.multiply(s).subtract(B2), n).add(ONE);

		int intervals = 0;
		for (BigInteger r = rLo; r.compareTo(rHi) <= 0; r = r.add(ONE)) {
			BigInteger aa = ceil(B2.add(r.multiply(n)), s);
			BigInteger bb = floor(B3.subtract(ONE).add(r.multiply(n)), s);

			BigInteger newA = max(a, aa);
			BigInteger newB = min(b, bb);
			if (newA.compareTo(newB) <= 0) {
				result.add(new Pair<BigInteger, BigInteger>(newA, newB));
				System.out.println("   r = " + r);
				System.out.println("     a: " + newA.toString(16));
				System.out.println("     b: " + newB.toString(16));
				intervals++;
			}
		}
		System.out.println(" - r explored: " + rHi.subtract(rLo).add(ONE) + "; intervals: " + intervals);

		return result;
	}

	private BigInteger findS(BigInteger c0, BigInteger si) {
		while (true) {
			BigInteger m1 = c0.multiply(si.modPow(e, n)).mod(n);
			if (callOracle(m1)) { // Correct padding
				break;
			}
			si = si.add(ONE);
		}
		return si;
	}

	public static BigInteger max(BigInteger x, BigInteger y) {
		return x.compareTo(y) > 0 ? x : y;
	}

	public static BigInteger min(BigInteger x, BigInteger y) {
		return x.compareTo(y) < 0 ? x : y;
	}

	public static BigInteger ceil(BigInteger x, BigInteger y) {
		return x.divide(y).add(ONE);
	}

	public static BigInteger floor(BigInteger x, BigInteger y) {
		return x.divide(y);
	}

}
