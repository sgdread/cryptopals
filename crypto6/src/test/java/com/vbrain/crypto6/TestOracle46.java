package com.vbrain.crypto6;

import static org.junit.Assert.assertEquals;

import java.math.BigInteger;

import org.bouncycastle.jce.provider.JCERSAPublicKey;
import org.junit.Test;

public class TestOracle46 {

	@Test
	public void testCheckMath() throws Exception {
		byte[] plaintext = "abc".getBytes();

		Oracle46 orcl = new Oracle46();
		JCERSAPublicKey pubKey = (JCERSAPublicKey) orcl.getPubKey();
		BigInteger n = pubKey.getModulus();
		BigInteger e = pubKey.getPublicExponent();

		byte[] ciphertext = orcl.encrypt(plaintext);

		// ---------------
		BigInteger multiplicator = BigInteger.valueOf(2).modPow(e, n);

		BigInteger ciphBI = RSATools.convertBytesToBigInteger(ciphertext, n);
		BigInteger doubledCiphBI = ciphBI.multiply(multiplicator).mod(n);

		BigInteger plainBI = RSATools.convertBytesToBigInteger(plaintext, n);
		BigInteger doubledPlainBI = plainBI.multiply(BigInteger.valueOf(2)).mod(n);

		byte[] decrypt1 = orcl.decrypt(RSATools.convertBigIntegerToBytes(doubledCiphBI, n));
		BigInteger mult11 = RSATools.convertBytesToBigInteger(decrypt1, n);

		assertEquals(doubledPlainBI, mult11);
	}

	@Test
	public void testEncryptDecrypt() throws Exception {
		Oracle46 orcl = new Oracle46();
		byte[] ciphertext = orcl.encrypt("abc".getBytes());
		byte[] plaintext = orcl.decrypt(ciphertext);
		assertEquals("abc", new String(plaintext));
	}

}
