package com.vbrain.crypto6;

import static org.junit.Assert.assertEquals;

import java.math.BigInteger;
import java.security.SecureRandom;

import org.apache.commons.codec.binary.Hex;
import org.bouncycastle.jce.provider.JCERSAPublicKey;
import org.junit.Before;
import org.junit.Test;

public class Test_41_UnpaddedMessageRecoveryOracle {

	private UnpaddedMessageRecoveryOracle orcl;

	// Use for verification only
	private static final String SECRET_PLAINTEXT = "TestMessage";

	private BigInteger N; // public modulus
	private BigInteger E; // public exponent

	private String capturedCiphertext;

	@Before
	public void setup() {
		orcl = new UnpaddedMessageRecoveryOracle();

		JCERSAPublicKey jceRsaPubKey = orcl.getPubKey();
		N = jceRsaPubKey.getModulus();
		E = jceRsaPubKey.getPublicExponent();
		
		System.out.println("n=" + N);
		System.out.println("e=" + E);

		String plaintextHex = Hex.encodeHexString(SECRET_PLAINTEXT.getBytes());
		capturedCiphertext = orcl.encrypt(plaintextHex);
	}

	@Test
	public void testProcess() throws Exception {
		BigInteger C = RSATools.convertBytesToBigInteger(Hex.decodeHex(capturedCiphertext.toCharArray()), N, false);
		
		
		BigInteger S = new BigInteger(1024, new SecureRandom()).mod(N);
		BigInteger Cprime = S.modPow(E, N).multiply(C).mod(N);
		
		String request = Hex.encodeHexString(RSATools.convertBigIntegerToBytes(Cprime, N, false));
		String response = orcl.process(request);
		
		BigInteger Pprime = RSATools.convertBytesToBigInteger(Hex.decodeHex(response.toCharArray()), N, false);
		BigInteger P = Pprime.multiply(S.modInverse(N)).mod(N);
		
		assertEquals(SECRET_PLAINTEXT, new String(RSATools.convertBigIntegerToBytes(P, N, false)));
	}

}
