package com.vbrain.crypto3;

import java.nio.ByteBuffer;

public class MT19937StreamCipher {
	
	public static byte[] encrypt(byte[] input, int seed) {
		seed = seed & 0xFFFF; // Use only 16 bits
		MT19937 rng = new MT19937(seed);
		ByteBuffer bb = ByteBuffer.allocate(input.length);
		for (int i = 0; i < input.length; i++) {
			byte cipheredValue = (byte) (input[i] ^ rng.nextByte());
			bb.put(cipheredValue);
		}
		return bb.array();
	}

	public static byte[] decrypt(byte[] input, int seed) {
		return encrypt(input, seed);
	}

}
