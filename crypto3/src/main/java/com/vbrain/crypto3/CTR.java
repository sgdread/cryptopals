package com.vbrain.crypto3;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import com.vbrain.crypto2.ECB;

public class CTR {

	public static final int CIPHER_BLOCK_SIZE = 16;

	public static byte[] encrypt(byte[] data, long nonce, byte[] key) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException,
			NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		int block = -1;
		int offset = 0;
		byte[] keyStream = null;
		ByteBuffer buffer = ByteBuffer.allocate(data.length);
		for (int i = 0; i < data.length; i++) {
			if (offset == 0) keyStream = ECB.encrypt(getKeystream(nonce, ++block), key);
			buffer.put((byte) (data[i] ^ keyStream[offset++]));
			if (offset == CIPHER_BLOCK_SIZE) offset = 0;
		}
		return buffer.array();
	}

	public static byte[] decrypt(byte[] data, long nonce, byte[] key) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException,
	NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		return encrypt(data, nonce, key);
	}

	private static byte[] getKeystream(long nonce, long block) {
		return ByteBuffer.allocate(16).order(ByteOrder.LITTLE_ENDIAN).putLong(nonce).putLong(block).array();
	}

}
