package com.vbrain.crypto3;

import java.util.Random;

import org.apache.commons.codec.binary.Base64;

import com.vbrain.crypto2.CBC;
import com.vbrain.crypto2.FileLoader;
import com.vbrain.crypto2.PKCS7Padder;
import com.vbrain.crypto2.RandomSecretKeyGenerator;

public class CBCPaddingOracle {

	private static byte[] randomKey = RandomSecretKeyGenerator.generate();
	private static byte[] secretString;

	static {
		String filedata = FileLoader.loadDataFromFile("src/main/resources/17_random_strings.txt");
		String[] lines = filedata.split("\n");
		String secrettext = lines[new Random().nextInt(lines.length)];
		secretString = Base64.decodeBase64(secrettext);

	}

	public static EncryptedMessage encrypt() {
		byte[] padded = PKCS7Padder.pad(secretString, CBC.CIPHER_BLOCK_SIZE);

		byte[] iv = new byte[CBC.CIPHER_BLOCK_SIZE];
		new Random().nextBytes(iv);

		byte[] ciphertext;
		try {
			ciphertext = CBC.encrypt(padded, randomKey, iv);
		} catch (Exception ex) {
			throw new IllegalStateException(ex);
		}

		return new EncryptedMessage(iv, ciphertext);
	}

	public static boolean processEncryptedMessage(EncryptedMessage message) {
		byte[] padded;
		try {
			padded = CBC.decrypt(message.ciphertext, randomKey, message.iv);
		} catch (Exception ex) {
			throw new IllegalStateException(ex);
		}

		try {
			PKCS7Padder.unpad(padded, CBC.CIPHER_BLOCK_SIZE);
		} catch (IllegalArgumentException ex) {
			return false;
		}

		return true;
	}

	public static class EncryptedMessage {
		public byte[] iv; // public - don't care about encapsulation in puzzles
		public byte[] ciphertext; // public - don't care about encapsulation in puzzles

		public EncryptedMessage(byte[] iv, byte[] ciphertext) {
			this.iv = iv;
			this.ciphertext = ciphertext;
		}

	}

}
