package com.vbrain.crypto3;

public class MT19937 {

	private int[] mt = new int[624];
	int index = 0;

	public MT19937(long seed) {
		mt[0] = (int) seed;
		for (int i = 1; i < mt.length; i++) {
			mt[i] = (0x6c078965 * (mt[i - 1] ^ (mt[i - 1] >>> 30)) + i) & 0xFFFFFFFF;
		}
	}
	
	public MT19937(int[] mt) {
		if(mt.length != 624) {
			throw new IllegalArgumentException("mt len must be 624");
		}
		this.mt = mt.clone();
	}

	public int nextInt() {
		if (index == 0) {
			generateNumbers();
		}

		int y = mt[index];
		y ^= y >>> 11;
		y ^= (y << 7) & 0x9d2c5680;
		y ^= (y << 15) & 0xefc60000;
		y ^= y >>> 18;
		index = (index + 1) % 624;
		return y;
	}
	
	public byte nextByte() {
		return (byte) nextInt();
	}

	private void generateNumbers() {
		for (int i = 0; i < mt.length; i++) {
			int y = (mt[i] & 0x80000000) + (mt[(i + 1) % 624] & 0x7FFFFFFF);
			mt[i] = mt[(i + 397) % 624] ^ (y >>> 1);
			if (y % 2 != 0) {
				mt[i] = mt[i] ^ 0x9908b0df;
			}
		}

	}

}
