package com.vbrain.crypto;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.apache.commons.codec.binary.Hex;

import com.google.common.collect.Lists;

public class SingleXorCracker {

	private String referenceCharset;
	private List<CharRank> referenceRank;

	public SingleXorCracker(String sampleFilename) {
		try {
			referenceCharset = getReferenceCharset(sampleFilename);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	public SingleXorCrackResult crack(byte[] input, int refCharShift, int charShift) {
		SingleXorCrackResult result = new SingleXorCrackResult();

		int[] frequency = new int[256];
		for (int i = 0; i < input.length; i++) {
			frequency[input[i] + Byte.MAX_VALUE + 1]++;
		}
		List<CharRank> rank = getCharacterRanks(frequency, false);

		int xorChar = referenceCharset.charAt(refCharShift) ^ rank.get(charShift).c;
		
		byte[] fixedXor = Xor.encode(input, new byte[] { (byte) xorChar });

		frequency = new int[256];
		for (int i = 0; i < fixedXor.length; i++) {
			frequency[fixedXor[i] + Byte.MAX_VALUE + 1]++;
		}
		rank = getCharacterRanks(frequency, false);

		int evalResultQuality = 0;
		for (CharRank score : rank) {
			if (referenceCharset.contains(new String(new char[] { score.c }))) {
				evalResultQuality++;
			}
			if (!referenceCharset.contains(new String(new char[] { score.c }))) {
				evalResultQuality --;
			}
		}
		if (evalResultQuality > result.getResultQuality()) {
			result.setResultQuality(evalResultQuality);
			result.setResult(fixedXor);
			result.setXorChar((char) xorChar);
			result.setRank(rank);
			result.setOverallCharFrequencyDeltaQuality(getOverallCharFrequencyDelta(result));
		}

		return result;
	}

	private String getReferenceCharset(String sampleFilename) throws IOException {
		int[] frequency = new int[256];
		BufferedReader br = new BufferedReader(new FileReader(new File(sampleFilename)));
		String line = br.readLine();
		while (line != null) {
			line = br.readLine();
			if (line == null) break;
			for (byte b : line.getBytes()) {
				frequency[b + Byte.MAX_VALUE + 1]++;
			}
		}
		br.close();

		List<CharRank> stats = getCharacterRanks(frequency, true);
		this.referenceRank = stats;
		StringBuilder sb = new StringBuilder();
		for (CharRank charRank : stats) {
			sb.append(charRank.c);
		}
		return sb.toString();
	}

	private List<CharRank> getCharacterRanks(int[] scores, boolean filterNonAscii) {
		int totalCount = 0;
		for (int i = 0; i < scores.length; i++) {
			totalCount += scores[i];
		}

		List<CharRank> rank = Lists.newArrayList();
		for (int i = 0; i < scores.length; i++) {
			int c = i - Byte.MAX_VALUE - 1;
			if (filterNonAscii) {
				boolean isCapitalChar = c >= 'A' && c <= 'Z';
				boolean isLowChar = c >= 'a' && c <= 'z';
				boolean isSpace = c == ' ';
				if (!(isCapitalChar || isLowChar || isSpace)) continue;
			}
			if (scores[i] == 0) continue;
			double usage = (double) scores[i] * 100 / totalCount;
			rank.add(new CharRank((char) c, usage));
		}
		Collections.sort(rank);
		return rank;
	}

	private double getOverallCharFrequencyDelta(SingleXorCrackResult result) {
		double overall = 0;
		for (int i = 0; i < 8; i++) {
			double score = result.getRank().get(i).score;
			double refScore = getReferenceStats().get(i).score;
			double delta = (score - refScore) / refScore;
			overall += delta;
		}
		return Math.abs(overall);
	}

	public List<CharRank> getReferenceStats() {
		return referenceRank;
	}

	public class SingleXorCrackResult {
		private List<CharRank> rank;
		private int resultQuality = 1;
		private byte[] result;
		private char xorChar;
		private double overallCharFrequencyDeltaQuality;

		public List<CharRank> getRank() {
			return rank;
		}

		public void setRank(List<CharRank> rank) {
			this.rank = rank;
		}

		public int getResultQuality() {
			return resultQuality;
		}

		public void setResultQuality(int resultQuality) {
			this.resultQuality = resultQuality;
		}

		public byte[] getResult() {
			return result;
		}

		public void setResult(byte[] result) {
			this.result = result;
		}

		public char getXorChar() {
			return xorChar;
		}

		public void setXorChar(char xorChar) {
			this.xorChar = xorChar;
		}

		public double getOverallCharFrequencyDeltaQuality() {
			return overallCharFrequencyDeltaQuality;
		}

		public void setOverallCharFrequencyDeltaQuality(double overallCharFrequencyDeltaQuality) {
			this.overallCharFrequencyDeltaQuality = overallCharFrequencyDeltaQuality;
		}

		@Override
		public String toString() {
			return "CrackResult [" + new String(result) + "]" + ", xorChar=[" + xorChar + "]" + "\nresultQuality=" + resultQuality
					+ ", overallCharFrequencyDeltaQuality=" + overallCharFrequencyDeltaQuality;
		}

	}

	public class CharRank implements Comparable<CharRank> {
		public char c;
		public double score;

		public CharRank(char c, double score) {
			this.c = c;
			this.score = score;
		}

		public int compareTo(CharRank o) {
			return Double.compare(o.score, this.score);
		}

		@Override
		public String toString() {
			return c + "[" + Hex.encodeHexString(new String(new char[] { c }).getBytes()) + "|" + (int) c + "]" + ": " + score;
		}

	}

}
