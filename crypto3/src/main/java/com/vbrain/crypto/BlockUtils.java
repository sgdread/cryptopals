package com.vbrain.crypto;

public class BlockUtils {
	
	public static byte[] joinBlocks(byte[][] blocks, int len) {
		byte[] result = new byte[len];
		int blockLen = blocks[0].length;
		int index = 0;
		end: for (int i = 0; i < blockLen; i++) {
			for (int block = 0; block < blocks.length; block++) {
				if(blocks[block] == null) {
					blocks[block] = new byte[blockLen];
				}
				result[index++] = blocks[block][i];
				if (index >= len) break end;
			}
		}
		return result;
	}

	public static byte[][] splitIntoBlocks(byte[] data, int len) {
		int blockLen = data.length / len;
		if (data.length % len != 0) blockLen++;
		byte[][] result = new byte[len][];

		for (int block = 0; block < len; block++) {
			result[block] = new byte[blockLen];
			int blkIndex = 0;
			for (int i = block; i < data.length; i += len) {
				result[block][blkIndex++] = data[i];
			}
		}
		return result;
	}
	
}
