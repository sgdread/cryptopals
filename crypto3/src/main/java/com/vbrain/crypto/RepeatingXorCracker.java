package com.vbrain.crypto;

import com.vbrain.crypto.SingleXorCracker.SingleXorCrackResult;

public class RepeatingXorCracker {

	public static RepeatingXorCrackResult crackUsingFixedLen(byte[] data, int len, String dictionaryFilename) {
		byte[][] blocks = BlockUtils.splitIntoBlocks(data, len);
		byte[] key = new byte[len];
		SingleXorCracker singleXorCracker = new SingleXorCracker(dictionaryFilename);
		for (int i = 0; i < blocks.length; i++) {
			SingleXorCrackResult bestResult = null;
			for (int refShift = 0; refShift < 53; refShift++) {
				for (int shift = 0; shift < 11; shift++) {
					SingleXorCrackResult result = singleXorCracker.crack(blocks[i], refShift, shift);
					if (bestResult == null || result.getResultQuality() > bestResult.getResultQuality()) {
						bestResult = result;
					}
				}
			}
			blocks[i] = bestResult.getResult();
			key[i] = (byte) bestResult.getXorChar();

		}
		byte[] result = BlockUtils.joinBlocks(blocks, data.length);
		return new RepeatingXorCrackResult(result, key);
	}

	public static class RepeatingXorCrackResult {
		private byte[] result;
		private byte[] key;

		public RepeatingXorCrackResult(byte[] result, byte[] key) {
			this.result = result;
			this.key = key;
		}

		public byte[] getResult() {
			return result;
		}

		public byte[] getKey() {
			return key;
		}

		@Override
		public String toString() {
			return "RepeatingXorCrackResult [" + new String(result) + "], key=[" + new String(key) + "]";
		}

	}

}
