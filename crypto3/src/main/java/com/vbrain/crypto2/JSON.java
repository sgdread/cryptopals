package com.vbrain.crypto2;

public class JSON {

	public static String toJSON(String input) {
		StringBuilder sb = new StringBuilder();
		sb.append("{");

		String[] splits = input.split("&");
		boolean isFirst = true;
		for (String split : splits) {
			if (isFirst) {
				isFirst = false;
			} else {
				sb.append(",");
			}
			String[] arg = split.split("=");
			// Since it's a puzzle, no sanity checks and fool-proof code
			sb.append("\n    ").append(arg[0]).append(": '").append(arg[1]).append("'");

		}

		sb.append("\n}");
		return sb.toString();
	}

	public static String fromJSON(String json) {
		String[] lines = json.split("\n");
		StringBuilder sb = new StringBuilder();
		boolean isFirst = true;
		int i = 0;
		for (String line : lines) {
			if (line.contains(": '")) {
				if (isFirst) {
					isFirst = false;
				} else {
					sb.append("&");
				}
				line = line.trim();
				line = line.replaceAll(": '", "=");
				sb.append(line.substring(0, line.length() - (i == (lines.length - 2) ? 1 : 2)));
			}
			i++;
		}
		return sb.toString();
	}

}
