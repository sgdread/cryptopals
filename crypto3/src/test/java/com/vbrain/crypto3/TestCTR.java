package com.vbrain.crypto3;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.junit.Test;

import com.google.common.collect.Lists;
import com.vbrain.crypto.RepeatingXorCracker;
import com.vbrain.crypto.RepeatingXorCracker.RepeatingXorCrackResult;
import com.vbrain.crypto2.FileLoader;
import com.vbrain.crypto2.RandomSecretKeyGenerator;

public class TestCTR {

	@Test
	public void testXor19() throws Exception {
		List<byte[]> encryptedData = getEncryptedData(19);
		decodeXor(encryptedData);
	}
	
	@Test
	public void testXor20() throws Exception {
		List<byte[]> encryptedData = getEncryptedData(20);
		decodeXor(encryptedData);
	}

	private void decodeXor(List<byte[]> encryptedData) {
		int minLen = Integer.MAX_VALUE;
		for (byte[] bs : encryptedData) {
			if (bs.length < minLen) {
				minLen = bs.length;
			}
		}
		
		int keyLen = minLen;
		ByteBuffer byteBuff = ByteBuffer.allocate(encryptedData.size() * keyLen);
		int lines = 0;
		for (byte[] bs : encryptedData) {
			byteBuff.put(bs, 0, keyLen);
			lines++;
		}
		RepeatingXorCrackResult result = RepeatingXorCracker.crackUsingFixedLen(byteBuff.array(), keyLen, "src/main/resources/english_sample.txt");
		for (int i = 0; i < lines; i++) {
			byte[] res = Arrays.copyOfRange(result.getResult(), i * minLen, (i+1) * minLen);
			System.out.println(new String(res));
		}
		System.out.println("****** Key [" + Hex.encodeHexString(result.getKey()) + "]");
	}

	public List<byte[]> getEncryptedData(int num) throws Exception {
		List<byte[]> result = Lists.newArrayList();
		
		byte[] key = RandomSecretKeyGenerator.generate();
		String data = FileLoader.loadDataFromFile("src/main/resources/" + num +"_data.txt");
		String[] lines = data.split("\n");
		
		for (String line : lines) {
			byte[] plaintext = Base64.decodeBase64(line);
			byte[] ciphertext = CTR.encrypt(plaintext, 0, key);
			result.add(ciphertext);
		}
		return result;
	}

}
