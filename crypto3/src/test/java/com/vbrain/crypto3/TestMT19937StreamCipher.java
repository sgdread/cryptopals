package com.vbrain.crypto3;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.Arrays;
import java.util.Random;

import org.apache.commons.codec.binary.Hex;
import org.junit.Test;

public class TestMT19937StreamCipher {

	@Test
	public void testEncrypt() throws Exception {
		String input = "eurhgurhg9u8409jf084ry74hfuisdf";
		int seed = new Random().nextInt();

		byte[] ciphertext = MT19937StreamCipher.encrypt(input.getBytes(), seed);
		byte[] plaintext = MT19937StreamCipher.decrypt(ciphertext, seed);

		assertNotEquals(input, new String(ciphertext));
		assertEquals(input, new String(plaintext));
	}

	@Test
	public void testCrack24() throws Exception {
		String knownPlaintext = "AAAAAAAAAAAAAA";

		byte[] unknownPlaintextWithKnownText = generateTestData(knownPlaintext);
		byte[] ciphertext = MT19937StreamCipher.encrypt(unknownPlaintextWithKnownText, new Random().nextInt() & 0xFFFF);

		// Assumptions:
		// 1) we know knowntext location (if not, matching will be slightly more complicated but still manageable)
		// 2) we know that seed is 16-bit - i.e. "bruteforceable"

		int randomOffsetLen = ciphertext.length - knownPlaintext.length();
		String cipheredAAAHex = Hex.encodeHexString(Arrays.copyOfRange(ciphertext, randomOffsetLen, ciphertext.length));

		byte[] probeData = generatePrefixedData(knownPlaintext, ciphertext.length);
		int seed = -1;
		for (int seedCandidate = 0; seedCandidate < 0x10000; seedCandidate++) {
			byte[] probedCiphertext = MT19937StreamCipher.encrypt(probeData, seedCandidate);
			String cipheredProbedAAAHex = Hex.encodeHexString(Arrays.copyOfRange(probedCiphertext, randomOffsetLen, probedCiphertext.length));
			if (cipheredProbedAAAHex.equals(cipheredAAAHex)) {
				seed = seedCandidate;
				break;
			}
		}

		// Check if decryption was correct
		assertEquals(Hex.encodeHexString(unknownPlaintextWithKnownText),  Hex.encodeHexString(MT19937StreamCipher.decrypt(ciphertext, seed)));
	}
	
	private byte[] generatePrefixedData(String fixedInput, int totalLen) {
		byte[] result = new byte[totalLen];
		byte[] fixedBytes = fixedInput.getBytes();
		int prefixLen = totalLen - fixedBytes.length;
		for (int i = 0; i < fixedBytes.length; i++) {
			result[prefixLen + i] = fixedBytes[i];
		}
		return result;
	}
	
	private byte[] generateTestData(String fixedInput) {
		byte[] fixedBytes = fixedInput.getBytes();
		int prefixLen = 5 + new Random().nextInt(20);
		byte[] result = new byte[prefixLen + fixedBytes.length];
		new Random().nextBytes(result);

		for (int i = 0; i < fixedBytes.length; i++) {
			result[prefixLen + i] = fixedBytes[i];
		}
		return result;
	}

}
