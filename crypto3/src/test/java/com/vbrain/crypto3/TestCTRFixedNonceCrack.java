package com.vbrain.crypto3;

import static org.junit.Assert.assertEquals;

import org.apache.commons.codec.binary.Base64;
import org.junit.Test;

import com.vbrain.crypto3.CTR;

public class TestCTRFixedNonceCrack {

	@Test
	public void testEncrypt() throws Exception {
		String input = "L77na/nrFsKvynd6HzOoG7GHTLXsTVu9qvY/2syLXzhPweyyMTJULu/6/kXX0KSvoOLSFQ==";
		byte[] data = Base64.decodeBase64(input);
		byte[] key = "YELLOW SUBMARINE".getBytes();
		System.out.println("[" + new String(CTR.decrypt(data, 0, key)) + "]");
	}

	@Test
	public void testEncryptDecrypt() throws Exception {
		String input = "test123456testEncryptDecrypt";
		byte[] key = "YELLOW SUBMARINE".getBytes();
		long nonce = 0;

		byte[] ciphertext = CTR.encrypt(input.getBytes(), nonce, key);
		byte[] plain = CTR.decrypt(ciphertext, nonce, key);

		assertEquals(input, new String(plain));
	}

}
