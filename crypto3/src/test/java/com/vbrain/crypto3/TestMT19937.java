package com.vbrain.crypto3;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestMT19937 {

	@Test
	public void testNextInt() throws Exception {
		MT19937 mt19937 = new MT19937(0);

		// Using implementation linked in Wiki as a reference implementation to
		// check correctness. Original is located at:
		// http://www.cs.gmu.edu/~sean/research/mersenne/MersenneTwister.java
		MersenneTwisterRI mt = new MersenneTwisterRI(0);

		for (int i = 0; i < 100; i++) {
			assertEquals(mt.nextInt(), mt19937.nextInt());
		}
	}

	@Test
	public void testNextInt2() throws Exception {
		MT19937 mt19937 = new MT19937(27654);

		// Using implementation linked in Wiki as a reference implementation to
		// check correctness.
		MersenneTwisterRI mt = new MersenneTwisterRI(27654);

		for (int i = 0; i < 100; i++) {
			assertEquals(mt.nextInt(), mt19937.nextInt());
		}
	}
	
	@Test
	public void testNextInt_LongSeed() throws Exception {
		MT19937 mt19937 = new MT19937(Long.MAX_VALUE);
		
		// Using implementation linked in Wiki as a reference implementation to
		// check correctness.
		MersenneTwisterRI mt = new MersenneTwisterRI(Long.MAX_VALUE);
		
		for (int i = 0; i < 100; i++) {
			assertEquals(mt.nextInt(), mt19937.nextInt());
		}
	}

}
