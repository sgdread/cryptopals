package com.vbrain.crypto3;

import java.util.Arrays;

import org.junit.Test;

import com.vbrain.crypto2.CBC;
import com.vbrain.crypto3.CBCPaddingOracle.EncryptedMessage;

public class TestCBCPaddingOracle {

	@Test
	public void testRun() throws Exception {
		EncryptedMessage msg = CBCPaddingOracle.encrypt();
		byte[] origCiphertext = msg.ciphertext.clone(); // Defensive copy
		int blocks = origCiphertext.length / CBC.CIPHER_BLOCK_SIZE;

		byte[] result = new byte[origCiphertext.length];

		for (int block = blocks - 2; block >= -1; block--) {
			byte[] origBlockWorkCiphertext;
			boolean usingIv = false;
			if (block < 0) {
				origBlockWorkCiphertext = new byte[CBC.CIPHER_BLOCK_SIZE * 2];
				for (int i = 0; i < msg.iv.length; i++) {
					// copy IV into block 0
					origBlockWorkCiphertext[i] = msg.iv[i];
					// copy block 0 into block 1
					origBlockWorkCiphertext[CBC.CIPHER_BLOCK_SIZE + i] = origCiphertext[i];
				}
				usingIv = true;
			} else {
				// Toss decrypted blocks from tail
				origBlockWorkCiphertext = Arrays.copyOf(origCiphertext, (block + 2) * CBC.CIPHER_BLOCK_SIZE);
			}

			decryptBlock(msg, result, block < 0 ? 0 : block, origBlockWorkCiphertext, usingIv);
		}
		System.out.println(new String(result));

	}

	private void decryptBlock(EncryptedMessage msg, byte[] result, int block, byte[] origBlockWorkCiphertext, boolean usingIv) {
		byte[] decodedBlock = new byte[CBC.CIPHER_BLOCK_SIZE];

		int discoveredChars = 0;

		for (int offset = CBC.CIPHER_BLOCK_SIZE - 1; offset >= 0; offset--) {
			// create xorMask

			int targetByte = discoveredChars + 1;
			byte[] xorMask = new byte[CBC.CIPHER_BLOCK_SIZE];
			Arrays.fill(xorMask, (byte) 0xFF);
			for (int i = CBC.CIPHER_BLOCK_SIZE - 1; i >= CBC.CIPHER_BLOCK_SIZE - discoveredChars; i--) {
				xorMask[i] = (byte) (targetByte ^ decodedBlock[i]);
			}

			// xor known characters
			msg.ciphertext = origBlockWorkCiphertext.clone();
			for (int i = 0; i < xorMask.length; i++) {
				msg.ciphertext[block * CBC.CIPHER_BLOCK_SIZE + i] = (byte) (msg.ciphertext[block * CBC.CIPHER_BLOCK_SIZE + i] ^ xorMask[i]);
			}

			// find next unknown character
			int tamperPos = CBC.CIPHER_BLOCK_SIZE - discoveredChars - 1;
			for (byte xorChar = Byte.MIN_VALUE; xorChar <= Byte.MAX_VALUE; xorChar++) {
				msg.ciphertext[block * CBC.CIPHER_BLOCK_SIZE + tamperPos] = (byte) (origBlockWorkCiphertext[block * CBC.CIPHER_BLOCK_SIZE + tamperPos] ^ xorChar);
				boolean paddingIsOk = CBCPaddingOracle.processEncryptedMessage(msg);
				if (paddingIsOk) {
					decodedBlock[tamperPos] = (byte) (xorChar ^ targetByte);
					discoveredChars++;
					break;
				}
			}

			// Copy decoded block into results
			for (int i = 0; i < decodedBlock.length; i++) {
				result[(block + (usingIv ? 0 : 1)) * CBC.CIPHER_BLOCK_SIZE + i] = decodedBlock[i];
			}
		}
	}

	public static int getPadding(EncryptedMessage msg, int blocks) {
		int padding = -1;
		byte[] origCiphertext = msg.ciphertext;
		for (int i = 0; i < CBC.CIPHER_BLOCK_SIZE; i++) {
			msg.ciphertext = origCiphertext.clone();
			int flippedPos = (blocks - 2) * CBC.CIPHER_BLOCK_SIZE + i;
			msg.ciphertext[flippedPos] = (byte) (msg.ciphertext[flippedPos] ^ 0x01);
			boolean paddingIsOk = CBCPaddingOracle.processEncryptedMessage(msg);
			if (!paddingIsOk) {
				padding = CBC.CIPHER_BLOCK_SIZE - i;
				break;
			}
		}
		msg.ciphertext = origCiphertext;
		return padding;
	}

}
