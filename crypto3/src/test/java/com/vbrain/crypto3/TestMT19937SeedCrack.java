package com.vbrain.crypto3;

import static org.junit.Assert.assertEquals;

import java.util.Random;

import org.junit.Test;

public class TestMT19937SeedCrack {

	@Test
	public void test22GuessSeed() throws Exception {
		long unixTimeStamp = System.currentTimeMillis();
		MT19937 mt = new MT19937(unixTimeStamp);
		int generatedValue = mt.nextInt();
		
		// Simulated unknownDelay 
		long unixTimeStampAfterRandomDelay = unixTimeStamp + (new Random().nextInt(2000) + 80);

		// Since we know that seed is based on time, let's brute force it for last 3000 seconds and find seed
		long guessedSeed = 0;
		for (long seedCandidate = unixTimeStampAfterRandomDelay; seedCandidate > (unixTimeStampAfterRandomDelay - 3000); seedCandidate--) {
			int value = new MT19937(seedCandidate).nextInt();
			if (value == generatedValue) {
				guessedSeed = seedCandidate;
				break;
			}
		}
		
		// Check if our guess correct
		assertEquals(unixTimeStamp, guessedSeed);
	}

	@Test
	public void test23CloneRNG() throws Exception {
		MT19937 originalRng = new MT19937(new Random().nextLong());
		
		// Generating 624 outputs 
		int[] randomOutput = new int[624];
		for (int i = 0; i < randomOutput.length; i++) {
			randomOutput[i] = originalRng.nextInt();
		}
		
		// untempering
		int[] untemperValues = new int[624];
		for (int i = 623; i >= 0; i--) {
			untemperValues[i] = untemper(randomOutput[i]);
		}
		MT19937 clonedRng = new MT19937(untemperValues);

		// checking sequences matching
		for (int i = 0; i < 1000; i++) {
			assertEquals(originalRng.nextInt(), clonedRng.nextInt());
		}
	}

	public int untemper(int generatorOutput) {
		int value = generatorOutput;
		// Reversing tempering operations
		value = reverseBitshiftRightXor(value, 18);
		value = reverseBitshiftLeftXor(value, 15, 0xefc60000);
		value = reverseBitshiftLeftXor(value, 7, 0x9d2c5680);
		value = reverseBitshiftRightXor(value, 11);
		return value;
	}
	
	
	

	public int reverseBitshiftRightXor(int value, int shift) {
		int i = 0;
		int result = 0;
		while (i * shift < 32) {
			int partMask = (-1 << (32 - shift)) >>> (shift * i);
			int part = value & partMask;
			value ^= part >>> shift;
			result |= part;
			i++;
		}
		return result;
	}

	public int reverseBitshiftLeftXor(int value, int shift, int mask) {
		int i = 0;
		int result = 0;
		while (i * shift < 32) {
			int partMask = (-1 >>> (32 - shift)) << (shift * i);
			int part = value & partMask;
			value ^= (part << shift) & mask;
			result |= part;
			i++;
		}
		return result;
	}
}
