package tools;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ToImage {
	public static void toImage(String filename, byte[][] pixels) {
		int width = pixels[0].length;
		int height = pixels.length;
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

		WritableRaster raster = image.getRaster();
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				int[] colorArray = new int[] {pixels[j][i],pixels[j][i],pixels[j][i]};
				raster.setPixel(i, j, colorArray);
			}
		}

		try {
			ImageIO.write(image, "gif", new File("./" + filename));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
