package com.vbrain.crypto;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.apache.commons.codec.binary.Hex;
import org.junit.Test;

public class TestECBCutNPaste {

	@Test
	public void testEncryptDecrypt() throws Exception {
		String username = "foo1234567890123@bar.com";

		// Aligning for tampered block generation
		String alignedUsername = username;
		ECB.ECBParams ecbParams = ECB.detectECB(username);
		if (ecbParams.firstAndLastBlockPartiallySplit) {
			AlignedUsernameRequest alignedRequest = alignBlocks(username, ecbParams);
			alignedUsername = alignedRequest.requestUsername;
			ecbParams = alignedRequest.ecbParams;
		}

		// create tampered block content (with admin)
		String adminString = new String(PKCS7Padder.pad("admin".getBytes(), ecbParams.blockSize));
		byte[] ciphertext = EncryptionOracle.profileForOracle(alignedUsername + adminString);
		int firstCleanBlock = ecbParams.firstCleanBlock;
		byte[] adminParamBlock = Arrays.copyOfRange(ciphertext, (firstCleanBlock + 2) * ecbParams.blockSize, (ecbParams.firstCleanBlock + 3)
				* ecbParams.blockSize);

		// get block to replace content (with user)
		String userString = new String(PKCS7Padder.pad("user".getBytes(), ecbParams.blockSize));
		ciphertext = EncryptionOracle.profileForOracle(alignedUsername + userString);
		byte[] userParamBlock = Arrays.copyOfRange(ciphertext, (firstCleanBlock + 2) * ecbParams.blockSize, (ecbParams.firstCleanBlock + 3)
				* ecbParams.blockSize);

		// Align tail
		alignedUsername = username;
		ciphertext = EncryptionOracle.profileForOracle(alignedUsername);
		String endOfCiphertextHex = Hex.encodeHexString(ciphertext).substring((ciphertext.length - ecbParams.blockSize) * 2);
		String userParamBlockHex = Hex.encodeHexString(userParamBlock);
		while (!userParamBlockHex.equals(endOfCiphertextHex)) {
			alignedUsername = "A" + alignedUsername;
			ciphertext = EncryptionOracle.profileForOracle(alignedUsername);
			endOfCiphertextHex = Hex.encodeHexString(ciphertext).substring((ciphertext.length - ecbParams.blockSize) * 2);
		}

		ciphertext = EncryptionOracle.profileForOracle(alignedUsername);
		
		// replace block with user with admin block
		int replacementOffset = ciphertext.length - ecbParams.blockSize;
		for (int i = 0; i < adminParamBlock.length; i++) {
			ciphertext[replacementOffset + i] = adminParamBlock[i];
		}
		
		String profile = EncryptionOracle.decryptEncrypterProfileFor(ciphertext);
		System.out.println(profile);

		// --------------- Validation
		checkResult(profile);
	}

	private AlignedUsernameRequest alignBlocks(String username, ECB.ECBParams ecbParams) {
		StringBuilder sb = new StringBuilder();
		sb.append(username);
		int blockSize = ecbParams.blockSize;
		int firstCleanBlock = -1;
		boolean firstAndLastBlockPartiallySplit = true;

		out: for (int blockLen = 1; blockLen < 50; blockLen++) {
			sb.append("A");
			byte[] ciphertext = EncryptionOracle.profileForOracle(sb.toString());
			if (ciphertext.length % blockSize != 0) continue;

			for (int block = 0; block < ciphertext.length / blockSize - 1; block++) {
				String block1 = Hex.encodeHexString(Arrays.copyOfRange(ciphertext, blockSize * block, blockSize * (block + 1)));
				String block2 = Hex.encodeHexString(Arrays.copyOfRange(ciphertext, blockSize * (block + 1), blockSize * (block + 2)));
				if (block1.equals(block2)) {
					firstAndLastBlockPartiallySplit = false;
					firstCleanBlock = block;
					break out;
				}
			}
		}
		ECB.ECBParams aligned = new ECB.ECBParams(blockSize, firstAndLastBlockPartiallySplit, firstCleanBlock);
		AlignedUsernameRequest alignedRequest = new AlignedUsernameRequest(aligned, sb.toString());
		return alignedRequest;
	}

	private void checkResult(String profile) {
		boolean adminIsFound = false;
		String[] lines = profile.split("\n");
		for (String line : lines) {
			if (line.trim().startsWith("role: 'admin")) {
				adminIsFound = true;
			}
		}
		assertTrue("Admin is not found: " + profile, adminIsFound);
	}

	private class AlignedUsernameRequest {
		private ECB.ECBParams ecbParams;
		private String requestUsername;

		public AlignedUsernameRequest(ECB.ECBParams ecbParams, String requestUsername) {
			this.ecbParams = ecbParams;
			this.requestUsername = requestUsername;
		}

		@Override
		public String toString() {
			return "AlignedUsernameRequest [ecbParams=" + ecbParams + ", requestUsername=" + requestUsername + "]";
		}

	}

}
