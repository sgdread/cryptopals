package com.vbrain.crypto;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestJSON {

	@Test
	public void testToJSON() throws Exception {
		String json = JSON.toJSON("foo=bar&baz=qux&zap=zazzle");
		// @formatter:off
		String expected = ""
				+ "{"
				+ "\n    foo: 'bar',"
				+ "\n    baz: 'qux',"
				+ "\n    zap: 'zazzle'"
				+ "\n}";
		// @formatter:on
		assertEquals(expected, json);
	}

	@Test
	public void testFromJSON() throws Exception {
		// @formatter:off
		String json = ""
				+ "{"
				+ "\n    foo: 'bar',"
				+ "\n    baz: 'qux',"
				+ "\n    zap: 'zazzle'"
				+ "\n}";
		// @formatter:on
		String expected = "foo=bar&baz=qux&zap=zazzle";
		String url = JSON.fromJSON(json);
		assertEquals(expected, url);
	}

}
