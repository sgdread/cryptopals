package com.vbrain.crypto;

import static org.junit.Assert.assertEquals;

import org.apache.commons.codec.binary.Base64;
import org.junit.Test;

public class TestCBC {

	@Test
	public void testEncryptDecrypt() throws Exception {
		String message = "1234567890ABCDEFEFGH";
		String key = "1234567890ABCDEF";
		byte[] iv = new byte[CBC.CIPHER_BLOCK_SIZE];
		
		byte[] paddedData = PKCS7Padder.pad(message.getBytes(), CBC.CIPHER_BLOCK_SIZE);
		
		byte[] ciphertext = CBC.encrypt(paddedData, key.getBytes(), iv);
		byte[] plaintext = CBC.decrypt(ciphertext, key.getBytes(), iv);
		
		assertEquals(new String(paddedData), new String(plaintext));
	}
	@Test
	public void testDecryptFile() throws Exception {
		String key="YELLOW SUBMARINE";
		byte[] iv = new byte[CBC.CIPHER_BLOCK_SIZE];
		
		String fileData = FileLoader.loadDataFromFile("src/test/resources/gistfile1_3132976.txt");
		byte[] ciphertext = Base64.decodeBase64(fileData);
		
		byte[] plaintext = CBC.decrypt(ciphertext, key.getBytes(), iv);
		System.out.println(new String(plaintext));
	}

}
