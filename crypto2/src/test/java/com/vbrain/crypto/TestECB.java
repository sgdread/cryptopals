package com.vbrain.crypto;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestECB {

	@Test
	public void testEncryptDecrypt() throws Exception {
		String key = "1234567890ABCDEF";
		String data = "0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF";
		byte[] ciphertext = ECB.encrypt(data.getBytes(), key.getBytes());
		byte[] decipheredtext = ECB.decrypt(ciphertext, key.getBytes());
		assertEquals(data, new String(decipheredtext));
	}
	
}
