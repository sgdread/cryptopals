package com.vbrain.crypto;

import java.util.Arrays;

import org.apache.commons.codec.binary.Hex;
import org.junit.Ignore;
import org.junit.Test;

import com.vbrain.crypto.ECB.ECBParams;

public class TestByteAtATimeECBDecryption {

	@Test
	public void testDecryptWithRandomPrefix() throws Exception {
		ECBParams ecbParams = ECB.detectECB("");
		StringBuilder sb = new StringBuilder();
		int blockSize = ecbParams.blockSize;

		// Aligning for tampered block generation
		String alignedPrefix = "";
		if (ecbParams.firstAndLastBlockPartiallySplit) {
			AlignedUsernameRequest alignedRequest = alignBlocks("", ecbParams);
			alignedPrefix = alignedRequest.requestPrefix;
			alignedPrefix = alignedPrefix.substring(0, alignedPrefix.length() % blockSize);
			ecbParams = alignedRequest.ecbParams;
		}
		
		int messageLen = EncryptionOracle.unknownFixedKeyWithRandomPrefixEncryptionOracle(alignedPrefix).length;
		for (int i = blockSize * (ecbParams.firstCleanBlock); i < messageLen ; i++) {
			int block = i / blockSize;
			int blockOffset = i % blockSize;
			String queryPrefix = alignedPrefix + createPrefix(blockSize - blockOffset - 1, "");
			String wantedHexedCiphertext = getHexedECBEncryptedBlockFromOracle(queryPrefix, blockSize, block, true);
			
			char encryptedChar = '?';
			
			queryPrefix = alignedPrefix  + createPrefix(blockSize - blockOffset - 1, "") + sb.toString();
			for (char x = 0x00; x < 0x7f; x++) {
				String hexedBlock = getHexedECBEncryptedBlockFromOracle(queryPrefix + x, blockSize, block, true);
				if (hexedBlock.equals(wantedHexedCiphertext)) {
					encryptedChar = x;
					break;
				}
			}
			sb.append(encryptedChar);
			
		}
		
		System.out.println(sb.toString());
		
	}
	

	private String getHexedECBEncryptedBlockFromOracle(String data, int blockSize, int blockNumber, boolean withUnknownPrefix) {
		byte[] ciphertext = withUnknownPrefix ?  EncryptionOracle.unknownFixedKeyWithRandomPrefixEncryptionOracle(data):  EncryptionOracle.unknownFixedKeyEncryptionOracle(data);
		byte[] block = Arrays.copyOfRange(ciphertext, blockNumber * blockSize, (blockNumber + 1) * blockSize);
		String hexEncodedBlock = Hex.encodeHexString(block);
		return hexEncodedBlock;
	}

	/**
	 * @param sb
	 * @return ECB cypher block len or -1 if not ECB
	 */
	private int detectECB() {
		int blockSize = -1;
		StringBuilder sb = new StringBuilder();
		for (int blockLen = 1; blockLen < 50; blockLen++) {
			sb.append("AAA");
			byte[] ciphertext = EncryptionOracle.unknownFixedKeyEncryptionOracle(sb.toString());

			if (ciphertext.length % blockSize != 0) continue;

			String block1 = Hex.encodeHexString(Arrays.copyOfRange(ciphertext, 0, blockLen));
			String block2 = Hex.encodeHexString(Arrays.copyOfRange(ciphertext, blockLen, blockLen * 2));
			String block3 = Hex.encodeHexString(Arrays.copyOfRange(ciphertext, blockLen * 2, blockLen * 3));

			if (block1.equals(block2) && block1.equals(block3)) {
				blockSize = blockLen;
				break;
			}
		}
		return blockSize;
	}
	
	@Test
	@Ignore
	public void testDecrypt() throws Exception {
		int blockSize = detectECB();
		StringBuilder sb = new StringBuilder();

		int messageLen = EncryptionOracle.unknownFixedKeyEncryptionOracle("").length;
		for (int i = 0; i < messageLen ; i++) {
			int block = i / blockSize;
			int blockOffset = i % blockSize;
			String queryPrefix = createPrefix(blockSize - blockOffset - 1, "");
			String wantedHexedCiphertext = getHexedECBEncryptedBlockFromOracle(queryPrefix, blockSize, block, false);

			char encryptedChar = '?';

			queryPrefix = createPrefix(blockSize * (block + 1) - 1, sb.toString());
			for (char x = 0x00; x < 0x7f; x++) {
				String hexedBlock = getHexedECBEncryptedBlockFromOracle(queryPrefix + x, blockSize, block, false);
				if (hexedBlock.equals(wantedHexedCiphertext)) {
					encryptedChar = x;
					break;
				}
			}
			sb.append(encryptedChar);

		}
		System.out.println(sb.toString());

	}

	private static String createPrefix(int prefixLen, String suffix) {
		StringBuilder sb = new StringBuilder();
		String result;
		if (suffix.length() < prefixLen) {
			for (int i = 0; i < prefixLen - suffix.length(); i++) {
				sb.append('A');
			}
			sb.append(suffix);
			result = sb.toString();
		} else {
			result = suffix.substring(suffix.length() - prefixLen);
		}
		return result;
	}

	private AlignedUsernameRequest alignBlocks(String prefix, ECB.ECBParams ecbParams) {
		StringBuilder sb = new StringBuilder();
		sb.append(prefix);
		int blockSize = ecbParams.blockSize;
		int firstCleanBlock = -1;
		boolean firstAndLastBlockPartiallySplit = true;

		out: for (int blockLen = 1; blockLen < 50; blockLen++) {
			sb.append("A");
			byte[] ciphertext = EncryptionOracle.unknownFixedKeyWithRandomPrefixEncryptionOracle(sb.toString());
			if (ciphertext.length % blockSize != 0) continue;

			for (int block = 0; block < ciphertext.length / blockSize - 1; block++) {
				String block1 = Hex.encodeHexString(Arrays.copyOfRange(ciphertext, blockSize * block, blockSize * (block + 1)));
				String block2 = Hex.encodeHexString(Arrays.copyOfRange(ciphertext, blockSize * (block + 1), blockSize * (block + 2)));
				if (block1.equals(block2)) {
					firstAndLastBlockPartiallySplit = false;
					firstCleanBlock = block;
					break out;
				}
			}
		}
		ECB.ECBParams aligned = new ECB.ECBParams(blockSize, firstAndLastBlockPartiallySplit, firstCleanBlock);
		AlignedUsernameRequest alignedRequest = new AlignedUsernameRequest(aligned, sb.toString());
		return alignedRequest;
	}

	private class AlignedUsernameRequest {
		private ECB.ECBParams ecbParams;
		private String requestPrefix;

		public AlignedUsernameRequest(ECB.ECBParams ecbParams, String requestUsername) {
			this.ecbParams = ecbParams;
			this.requestPrefix = requestUsername;
		}

		@Override
		public String toString() {
			return "AlignedUsernameRequest [ecbParams=" + ecbParams + ", requestPrefix=" + requestPrefix + "]";
		}

	}

}
