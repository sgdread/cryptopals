package com.vbrain.crypto;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestPKCS7Padder {

	@Test
	public void testPad() throws Exception {
		String input = "YELLOW SUBMARINE";
		char padChar = 0x04;
		String expected = input + padChar + padChar + padChar + padChar;
		byte[] padded = PKCS7Padder.pad(input.getBytes(), 20);
		assertEquals(expected, new String(padded));
	}

	@Test
	public void testPad2() throws Exception {
		String input = "YELL";
		char padChar = 0x01;
		String expected = input + padChar;
		byte[] padded = PKCS7Padder.pad(input.getBytes(), 5);
		assertEquals(expected, new String(padded));
	}

	@Test
	public void testPad3() throws Exception {
		String input = "YELL";
		char padChar = 0x02;
		String expected = input + padChar + padChar;
		byte[] padded = PKCS7Padder.pad(input.getBytes(), 3);
		assertEquals(expected, new String(padded));
	}

	@Test
	public void testPad4() throws Exception {
		String input = "123123";
		char padChar = 0x03;
		String expected = input + padChar + padChar + padChar;
		byte[] padded = PKCS7Padder.pad(input.getBytes(), 3);
		assertEquals(expected, new String(padded));
	}

	@Test
	public void testUnpad_Correct_HalfFilledLastBlock() throws Exception {
		String unpadded = "YELLOW SUBMARINE";
		char padChar = 0x04;
		String padded = unpadded + padChar + padChar + padChar + padChar;
		assertEquals(unpadded, new String(PKCS7Padder.unpad(padded.getBytes(), 20)));
	}

	@Test
	public void testUnpad_Correct_FullLastBlock() throws Exception {
		String unpadded = "123123";
		char padChar = 0x03;
		String padded = unpadded + padChar + padChar + padChar;
		assertEquals(unpadded, new String(PKCS7Padder.unpad(padded.getBytes(), 3)));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testUnpad_InvalidPadding_WrongAlignment() throws Exception {
		String padded = "YELLOW SUBMARINE";
		assertEquals(padded, PKCS7Padder.unpad(padded.getBytes(), 17));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testUnpad_InvalidPadding_LastCharGreaterThanBlockSize() throws Exception {
		String unpadded = "123123";
		char padChar = 0x03;
		String padded = unpadded + padChar + padChar + (padChar + 20);
		assertEquals(unpadded, PKCS7Padder.unpad(padded.getBytes(), 3));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testUnpad_InvalidPadding_CorrectLastButWrongPrevious() throws Exception {
		String unpadded = "YELLOW SUBMARINE";
		char padChar = 0x01;
		String padded = unpadded + (padChar++) + (padChar++) + (padChar++) + (padChar++);
		assertEquals(unpadded, PKCS7Padder.unpad(padded.getBytes(), 20));
	}

}
