package com.vbrain.crypto;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.apache.commons.codec.binary.Hex;
import org.junit.Test;

public class TestDetectBlockCypher {

	@Test
	public void testEncryptDecrypt() throws Exception {
		for (int i = 0; i < 1000; i++) {

			String zeroesBlock = "00000000000000000000000000000000";

			// Adding 1st zero block to compensate random prefix block length
			// 2nd and 3rd blocks are detection blocks
			// 4th and 5th will compensate for last random block length and PCKS
			// padding.
			// As a result we guaranteed to have at least two same blocks in ECB
			// mode while in CBC it won't be the case (XOR will mess it).
			//
			String tamperedMessageHex = zeroesBlock + zeroesBlock + zeroesBlock + zeroesBlock + zeroesBlock;
			byte[] data = Hex.decodeHex(tamperedMessageHex.toCharArray());

			byte[] ciphertext = EncryptionOracle.randomEncryptionOracle(data);

			boolean expectedGuess = EncryptionOracle.isCBC;

			assertEquals("Failed to detect cypher on " + i, expectedGuess, isCBC(ciphertext));
		}
	}

	private boolean isCBC(byte[] ciphertext) {
		boolean isCBC = true;
		byte[] tamperedBlock1 = Arrays.copyOfRange(ciphertext, ciphertext.length - CBC.CIPHER_BLOCK_SIZE * 4, ciphertext.length - CBC.CIPHER_BLOCK_SIZE * 3);
		byte[] tamperedBlock2 = Arrays.copyOfRange(ciphertext, ciphertext.length - CBC.CIPHER_BLOCK_SIZE * 3, ciphertext.length - CBC.CIPHER_BLOCK_SIZE * 2);

		String tamperedBlock1Hex = Hex.encodeHexString(tamperedBlock1);
		String tamperedBlock2Hex = Hex.encodeHexString(tamperedBlock2);

		if (tamperedBlock1Hex.equals(tamperedBlock2Hex)) {
			isCBC = false;
		}
		return isCBC;
	}

}
