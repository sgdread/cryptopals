package com.vbrain.crypto;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;

public class ECB {

	public static byte[] encrypt(byte[] data, byte[] key) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		SecretKey secret = new SecretKeySpec(key, "AES");
		Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding", "SunJCE");
		cipher.init(Cipher.ENCRYPT_MODE, secret);
		byte[] encrypted = cipher.doFinal(data);
		return encrypted;
	}
	
	public static byte[] decrypt(byte[] data, byte[] key) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding", "SunJCE");
		SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
		cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(skeySpec.getEncoded(), "AES"));
		byte[] original = cipher.doFinal(data);
		return original;
	}
	
	public static ECBParams detectECB(String prefix) {
		int blockSize = -1;
		int firstCleanBlock = -1;
		boolean firstAndLastBlockPartiallySplit = false;
		StringBuilder sb = new StringBuilder();
		sb.append(prefix);

		out: for (int blockLen = 2; blockLen < 50; blockLen++) {
			sb.insert(0, "AAA"); // Blocks 1&3 can be partially broken into
									// halves
			byte[] ciphertext = EncryptionOracle.profileForOracle(sb.toString());
			if (ciphertext.length % blockSize != 0) continue;

			for (int block = 0; block < ciphertext.length / blockLen - 1; block++) {
				String block1 = Hex.encodeHexString(Arrays.copyOfRange(ciphertext, blockLen * block, blockLen * (block + 1)));
				String block2 = Hex.encodeHexString(Arrays.copyOfRange(ciphertext, blockLen * (block + 1), blockLen * (block + 2)));
				if (block1.equals(block2)) {
					blockSize = blockLen;
					// we have 3 AAA... blocks. We need to know if 1st and 4th
					// are broken in half.
					String block3 = Hex.encodeHexString(Arrays.copyOfRange(ciphertext, blockLen * (block + 2), blockLen * (block + 3)));
					firstAndLastBlockPartiallySplit = !block1.equals(block3);
					firstCleanBlock = block;
					break out;
				}
			}
		}
		return new ECBParams(blockSize, firstAndLastBlockPartiallySplit, firstCleanBlock);
	}

	public static class ECBParams {
		public int blockSize;
		public boolean firstAndLastBlockPartiallySplit;
		public int firstCleanBlock;

		public ECBParams(int blockSize, boolean firstAndLastBlockPartiallySplit, int firstCleanBlock) {
			this.blockSize = blockSize;
			this.firstAndLastBlockPartiallySplit = firstAndLastBlockPartiallySplit;
			this.firstCleanBlock = firstCleanBlock;
		}

		@Override
		public String toString() {
			return "ECBParams [blockSize=" + blockSize + ", firstAndLastBlockPartiallySplit=" + firstAndLastBlockPartiallySplit + ", firstCleanBlock="
					+ firstCleanBlock + "]";
		}

	}

	
}
