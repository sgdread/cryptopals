package com.vbrain.crypto;
public class Xor {

	public static byte[] encode(byte[] input, byte[] mask) {
		byte[] result = new byte[input.length];
		int maskLen = mask.length;
		for (int i = 0; i < result.length; i++) {
			result[i] = (byte) (input[i] ^ mask[i % maskLen]);
		}
		return result;
	}

}
