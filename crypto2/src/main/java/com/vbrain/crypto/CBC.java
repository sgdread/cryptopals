package com.vbrain.crypto;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class CBC {

	public static final int CIPHER_BLOCK_SIZE = 16;

	public static byte[] encrypt(byte[] data, byte[] key, byte[] iv) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException,
			NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		byte[][] blocks = split(data);

		byte[] result = new byte[data.length];
		for (int i = 0; i < blocks.length; i++) {
			byte[] xored = Xor.encode(blocks[i], iv);
			byte[] ciphertext = ECB.encrypt(xored, key);
			iv = ciphertext;
			int resultOffset = i * CIPHER_BLOCK_SIZE;
			for (int j = 0; j < ciphertext.length; j++) {
				result[resultOffset + j] = ciphertext[j];
			}
		}
		return result;
	}

	public static byte[] decrypt(byte[] data, byte[] key, byte[] iv) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException,
			NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		byte[][] blocks = split(data);
		byte[] result = new byte[data.length];
		for (int i = 0; i < blocks.length; i++) {
			byte[] ciphertext = blocks[i];
			byte[] xored = ECB.decrypt(ciphertext, key);
			byte[] plaintext = Xor.encode(xored, iv);
			iv = ciphertext;
			int resultOffset = i * CIPHER_BLOCK_SIZE;
			for (int j = 0; j < plaintext.length; j++) {
				result[resultOffset + j] = plaintext[j];
			}
		}
		return result;
	}

	private static byte[][] split(byte[] paddedData) {
		int blocksNum = paddedData.length / CIPHER_BLOCK_SIZE;
		byte[][] blocks = new byte[blocksNum][];
		for (int block = 0; block < blocksNum; block++) {
			blocks[block] = Arrays.copyOfRange(paddedData, block * CIPHER_BLOCK_SIZE, (block + 1) * CIPHER_BLOCK_SIZE);
		}
		return blocks;
	}

}
