package com.vbrain.crypto;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class FileLoader {
	
	public static String loadDataFromFile(String filename) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(filename));
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();
			while (line != null) {
				sb.append(line);
				line = br.readLine();
				if (line == null) break;
			}
			br.close();
			return sb.toString();
		} catch (IOException ex) {
			throw new IllegalStateException(ex);
		}
	}

}
