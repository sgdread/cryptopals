package com.vbrain.crypto;

import java.util.Random;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;

public class EncryptionOracle {

	private static byte[] predefinedKey = RandomSecretKeyGenerator.generate();
	private static String unknownTextHex = Hex.encodeHexString(Base64.decodeBase64(FileLoader.loadDataFromFile("src/test/resources/unknownText.txt")));

	private static byte[] unknownRandomPrefix = new byte[20 + new Random().nextInt(20)];
	static {
		new Random().nextBytes(unknownRandomPrefix);
	}

	public static boolean isCBC = false;

	public static byte[] profileForOracle(String username) {
		username = username.replaceAll("[&=]", "");
		StringBuilder sb = new StringBuilder();
		sb.append("email=").append(username).append("&uid=10&role=user");
		String data = sb.toString();
		byte[] paddedData = null;
		paddedData = PKCS7Padder.pad(data.getBytes(), CBC.CIPHER_BLOCK_SIZE);

		byte[] key = predefinedKey;

		byte[] ciphertext = null;
		try {
			ciphertext = ECB.encrypt(paddedData, key);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return ciphertext;
	}

	public static String decryptEncrypterProfileFor(byte[] data) {
		try {
			String plaintext = new String(ECB.decrypt(data, predefinedKey));
			String profile = JSON.toJSON(plaintext);
			return profile;
		} catch (Exception ex) {
			throw new IllegalStateException(ex);
		}
	}

	public static byte[] randomEncryptionOracle(byte[] data) {
		byte[] prefixedSuffixedData = randomizePrefixesSuffixes(data);
		byte[] paddedData = PKCS7Padder.pad(prefixedSuffixedData, CBC.CIPHER_BLOCK_SIZE);
		byte[] key = RandomSecretKeyGenerator.generate();

		byte[] ciphertext = null;
		isCBC = new Random().nextBoolean();
		if (isCBC) { // CBC
			byte[] iv = new byte[CBC.CIPHER_BLOCK_SIZE];
			new Random().nextBytes(iv); // Random IV

			try {
				ciphertext = CBC.encrypt(paddedData, key, iv);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else { // ECB
			try {
				ciphertext = ECB.encrypt(paddedData, key);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return ciphertext;
	}

	public static byte[] unknownFixedKeyWithRandomPrefixEncryptionOracle(String userInput) {
		String textToEncryptHex = Hex.encodeHexString(unknownRandomPrefix) + Hex.encodeHexString(userInput.getBytes()) + unknownTextHex;
		byte[] paddedData = null;
		try {
			byte[] data = Hex.decodeHex((textToEncryptHex).toCharArray());
			paddedData = PKCS7Padder.pad(data, CBC.CIPHER_BLOCK_SIZE);
		} catch (DecoderException ex) {
			throw new IllegalStateException(ex);
		}

		byte[] key = predefinedKey;

		byte[] ciphertext = null;
		try {
			ciphertext = ECB.encrypt(paddedData, key);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return ciphertext;
	}

	public static String decryptBlock(String hex) {
		try {
			return new String(ECB.decrypt(Hex.decodeHex(hex.toCharArray()), predefinedKey));
		} catch (Exception ex) {
			throw new IllegalStateException(ex);
		}
	}
	
	public static byte[] unknownFixedKeyEncryptionOracle(String userInput) {
		String textToEncryptHex = Hex.encodeHexString(userInput.getBytes()) + unknownTextHex;
		byte[] paddedData = null;
		try {
			paddedData = PKCS7Padder.pad(Hex.decodeHex((textToEncryptHex).toCharArray()), CBC.CIPHER_BLOCK_SIZE);
		} catch (DecoderException ex) {
			throw new IllegalStateException(ex);
		}

		byte[] key = predefinedKey;

		byte[] ciphertext = null;
		try {
			ciphertext = ECB.encrypt(paddedData, key);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return ciphertext;
	}

	private static byte[] randomizePrefixesSuffixes(byte[] data) {
		byte[] randomPrefix = new byte[5 + new Random().nextInt(1000) % 5];
		byte[] randomSuffix = new byte[5 + new Random().nextInt(1000) % 5];
		new Random().nextBytes(randomPrefix);
		new Random().nextBytes(randomSuffix);

		byte[] prefixedSuffixedData = new byte[randomPrefix.length + data.length + randomSuffix.length];
		for (int i = 0; i < randomPrefix.length; i++) {
			prefixedSuffixedData[i] = randomPrefix[i];
		}
		for (int i = 0; i < data.length; i++) {
			prefixedSuffixedData[randomPrefix.length + i] = data[i];
		}
		for (int i = 0; i < randomSuffix.length; i++) {
			prefixedSuffixedData[randomPrefix.length + data.length + i] = randomSuffix[i];
		}
		return prefixedSuffixedData;
	}

}
