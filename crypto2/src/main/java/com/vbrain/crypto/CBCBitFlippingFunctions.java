package com.vbrain.crypto;

import java.util.Random;

public class CBCBitFlippingFunctions {

	private static byte[] unknownPredefinedKey = RandomSecretKeyGenerator.generate();
	private static byte[] unknownIV = new byte[CBC.CIPHER_BLOCK_SIZE];
	static {
		new Random().nextBytes(unknownIV);
	}

	public static byte[] encryptUrl(String userdata) {
		String url = getUrl(userdata);
		byte[] padded = PKCS7Padder.pad(url.getBytes(), CBC.CIPHER_BLOCK_SIZE);
		try {
			byte[] ciphertext = CBC.encrypt(padded, unknownPredefinedKey, unknownIV);
			return ciphertext;
		} catch (Exception ex) {
			throw new IllegalArgumentException(ex);
		}
	}

	public static boolean isAdmin(byte[] ciphertext) {
		try {
			byte[] paddedPlaintext = CBC.decrypt(ciphertext, unknownPredefinedKey, unknownIV);
			byte[] plaintext = PKCS7Padder.unpad(paddedPlaintext, CBC.CIPHER_BLOCK_SIZE);
			String url = new String(plaintext);
			boolean admin = url.contains(";admin=true;");
			return admin;
		} catch (Exception ex) {
			throw new IllegalArgumentException(ex);
		}
	}

	public static String getUrl(String userdata) {
		String prefix = "comment1=cooking%20MCs;userdata=";
		String suffix = ";comment2=%20like%20a%20pound%20of%20bacon";
		userdata = userdata.replaceAll("[;=]", "");
		String result = prefix + userdata + suffix;
		return result;
	}

}
